package businessLogic;

import java.sql.Date;

public class Empleado {
	
	/* Atributos */
	private String dni;
	private String apellidos;
	private String nombre;
	private Date fechaNacimiento;
	private String poblacion;
	private String provincia;
	private String cp;
	private String telefono;
	private String nombreUsuario;
	private String contrasena;
	
	/* Constructores */
	public Empleado() {
		super();
	}
	
	public Empleado(String dni, String apellidos, String nombre,
			Date fechaNacimiento, String poblacion, String provincia,
			String cp, String telefono, String nombreUsuario, String contrasena) {
		super();
		this.dni = dni;
		this.apellidos = apellidos;
		this.nombre = nombre;
		this.fechaNacimiento = fechaNacimiento;
		this.poblacion = poblacion;
		this.provincia = provincia;
		this.cp = cp;
		this.telefono = telefono;
		this.nombreUsuario = nombreUsuario;
		this.contrasena = contrasena;
	}


	/* M�todos */
	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getPoblacion() {
		return poblacion;
	}

	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

}
