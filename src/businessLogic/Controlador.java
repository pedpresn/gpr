                                                                                                                                                                                                                                                                                                                                                                                                                                                 package businessLogic;

import java.sql.Timestamp;
import java.util.List;

import businessLogic.*;
import dataAccess.DAL;
import excepciones.DAOExcepcion;

public class Controlador {
	/* Atributos */
	public static final int OPERARIO = 0;
	public static final int MAESTRO = 1;
	public static final int JEFE = 2;
	public static final String APPLICATION_NAME = "BatHospital";
	public static final String[] PRIORIDADES = { "BAJA", "MEDIA", "ALTA" };

	private static Controlador con;
	private static DAL dal;
	private static List<AvisoIncidencia> avisos;
	private static List<ParteIncidencia> partes;
	private static Empleado empActual;
	private static OrdenTrabajo ordenActual;

	/* Constructores */
	private Controlador() throws DAOExcepcion {
		dal = DAL.getSingletonDAL();
	}

	/* M�todos */
	public static Controlador getSingletonControlador() throws DAOExcepcion {
		if (con == null)
			con = new Controlador();
		return con;
	}

	public int comprobarUsuarioYContrasena(String nombreUsuario,
			String contrasena) throws DAOExcepcion {
		int tipoUsuario = -1;
		empActual =
				dal.getEmpleadoByNombreUsuarioAndContrasena(nombreUsuario,
						contrasena);
		if (empActual instanceof businessLogic.Operario)
			tipoUsuario = this.OPERARIO;
		else if (empActual instanceof businessLogic.Maestro)
			tipoUsuario = this.MAESTRO;
		else if (empActual instanceof businessLogic.Jefe)
			tipoUsuario = this.JEFE;
		return tipoUsuario;
	}

	public void crearAvisoIncidencia(String descripcion, int maquinaId)
			throws DAOExcepcion {
		dal.crearAvisoIncidencia(descripcion, new Timestamp(System
				.currentTimeMillis()), maquinaId);
	}

	public void crearParteIncidencia(int codigo, int prioridad, int estado,
			double costeDiario, int area) throws DAOExcepcion {
		ParteIncidencia pi =
				new ParteIncidencia(codigo, prioridad, estado, costeDiario,
						area);
		dal.crearParteIncidencia(pi);
	}

	public void crearOrdenTrabajo(int parteId, String dni) throws DAOExcepcion {
		OrdenTrabajo orden = new OrdenTrabajo(parteId, dni, null, -1);
		dal.crearOrdenTrabajo(orden);
	}

	// Matrix row structure: id (codMaquina), area (codArea), modelo,
	// descripcion
	public Object[][] getAllMaquinas() throws DAOExcepcion {
		int pos = 0;
		List<Maquina> maquinas = dal.getAllMaquinas();

		Object[][] result = new Object[maquinas.size()][4];
		for (Maquina m : maquinas) {
			result[pos][0] = m.getCodMaquina();
			// TODO: show correct area name.
			result[pos][1] = m.getCodArea();
			result[pos][2] = m.getModelo();
			result[pos][3] = m.getDescripcion();
			++pos;
		}
		return result;
	}

	public Object[][] getAllOrdenesConcluidas() throws DAOExcepcion {
		return dal.getAllOrdenesTerminadasByArea(((Maestro) this.empActual).getArea());
	}
	
	// Matrix row structure: id (codMaquina), area (nombre), modelo,
	// descripcion
	public Object[][] getAllMaquinasWithAreas() throws DAOExcepcion {
		Object[] maquinasAndAreas = dal.getAllMaquinasWithAreas();
		List<Maquina> maquinas = (List<Maquina>) maquinasAndAreas[0];
		List<Area> areas = (List<Area>) maquinasAndAreas[1];

		Object[][] result = new Object[maquinas.size()][4];
		for (int pos = 0; pos < maquinas.size(); ++pos) {
			result[pos][0] = maquinas.get(pos).getCodMaquina();
			// TODO: show correct area name.
			result[pos][1] = areas.get(pos).getNombre();
			result[pos][2] = maquinas.get(pos).getModelo();
			result[pos][3] = maquinas.get(pos).getDescripcion();
		}
		return result;
	}

	// Matrix row structure: codigo, fecha(dd-mm-yyyy), descripcion,
	// maquina (true,false)
	public Object[][] getAllAvisosIncidencia() throws DAOExcepcion {
		int pos = 0;
		avisos = dal.getAllAvisosIncidencia();
		Object[][] result = new Object[avisos.size()][4];
		for (AvisoIncidencia a : avisos) {
			result[pos][0] = Integer.toString(a.getCodigo());
			result[pos][1] =
					a.getFechaPeticion().toLocaleString().substring(0, 11);
			result[pos][2] = a.getDescripcion();
			result[pos][3] = (a.getMaquina() < 0 ? false : true);
			++pos;
		}
		return result;
	}

	// Vector structure: codigo (aviso), fecha (aviso), descripcion (aviso),
	// maquina (aviso)(true,false), area (maquina codArea), modelo (maquina),
	// descripcion (maquina)
	public Object[] getAvisoIncidenciaById(int id) throws DAOExcepcion {
		AvisoIncidencia a = null;
		Maquina m = null;
		Object[] result = null;

		for (int i = 0; i < avisos.size(); ++i) {
			if (avisos.get(i).getCodigo() == id) {
				a = avisos.get(i);
				break;
			}
		}

		if (a.getMaquina() != -1)
			m = dal.getMaquinaById(a.getMaquina());

		result = new Object[7];
		result[0] = a.getCodigo();
		result[1] = a.getFechaPeticion().toLocaleString();
		result[2] = a.getDescripcion();
		result[3] = (a.getMaquina() != -1);
		result[4] = (m == null) ? null : m.getCodArea();
		result[5] = (m == null) ? null : m.getModelo();
		result[6] = (m == null) ? null : m.getDescripcion();
		return result;
	}

	public Object[] getIncidenciaInformationByCodigo(int codigo)
			throws DAOExcepcion {
		int i;
		AvisoIncidencia ai = null;
		Maquina m = null;

		for (i = 0; i < avisos.size(); ++i) {
			if (avisos.get(i).getCodigo() == codigo) {
				ai = avisos.get(i);
				break;
			}
		}

		Object[] result = new Object[8];
		result[0] = ai.getCodigo();
		result[1] = ai.getFechaPeticion().toLocaleString();
		result[2] = ai.getDescripcion();
		result[3] = (ai.getMaquina() != -1);
		if (ai.getMaquina() != -1) {
			m = dal.getMaquinaById(ai.getMaquina());
			result[4] = dal.getAreaByCodArea(m.getCodArea()).getNombre();
			result[5] = m.getModelo();
			result[6] = m.getDescripcion();
		} else {
			result[4] = result[5] = result[6] = null;
		}
		result[7] = partes.get(i).getPrioridad();

		return result;
	}

	public Object[][] getAllOperariosOfCurrentArea() throws DAOExcepcion {
		return getAllOperariosByArea(((Maestro) this.empActual).getArea());
	}

	public Object[][] getAllAreas() throws DAOExcepcion {
		List<Area> areas = dal.getAllAreas();
		Object[][] result = new Object[areas.size()][2];
		int pos = 0;
		for (Area a : areas) {
			result[pos][0] = a.getCodArea();
			result[pos][1] = a.getNombre();
			++pos;
		}
		return result;
	}

	// Matrix row structure: [dni] [nombre y apellidos]
	public Object[][] getAllOperariosByArea(int codArea) throws DAOExcepcion {
		List<Empleado> operarios = dal.getAllOperariosByArea(codArea);
		Object[][] result = new Object[operarios.size()][2];
		int pos = 0;
		for (Empleado e : operarios) {
			result[pos][0] = e.getDni();
			result[pos][1] = e.getNombre() + ' ' + e.getApellidos();
			++pos;
		}
		return result;
	}

	// Matrix row structure: codigo, fecha, descripcion, prioridad.
	public Object[][] getAllIncidenciasOfThisArea() throws DAOExcepcion {
		Object[] partesAndAvisos =
				dal.getAllIncidenciasByCodArea(((Maestro) empActual).getArea());
		partes = (List<ParteIncidencia>) partesAndAvisos[0];
		avisos = (List<AvisoIncidencia>) partesAndAvisos[1];
		Object[][] result = new Object[partes.size()][4];

		for (int pos = 0; pos < partes.size(); ++pos) {
			result[pos][0] = partes.get(pos).getCodigo();
			result[pos][1] =
					avisos.get(pos).getFechaPeticion().toLocaleString();
			result[pos][2] = avisos.get(pos).getDescripcion();
			result[pos][3] = partes.get(pos).getPrioridad();
		}
		return result;
	}

	// columnas: (codigo, fecha de petici�n, descripci�n, prioridad)
	public Object[][] getAllOrdenesTrabajoOfThisOperario() throws DAOExcepcion {
		return getAllOrdenesTrabajoByDni(empActual.getDni());
	}

	// columnas: (codigo, fecha de petici�n, descripci�n, prioridad)
	public Object[][] getAllOrdenesTrabajoByDni(String dni) throws DAOExcepcion {
		Object[] partesAndAvisos =
				dal.getAllIncidenciasAndAvisosOfOrdenTrabajoByDni(dni);
		partes = (List<ParteIncidencia>) partesAndAvisos[0];
		avisos = (List<AvisoIncidencia>) partesAndAvisos[1];
		Object[][] result = new Object[partes.size()][4];

		for (int pos = 0; pos < partes.size(); ++pos) {
			result[pos][0] = partes.get(pos).getCodigo();
			result[pos][1] =
					avisos.get(pos).getFechaPeticion().toLocaleString();
			result[pos][2] = avisos.get(pos).getDescripcion();
			result[pos][3] = partes.get(pos).getPrioridad();
		}
		return result;
	}

	// (codigo, fecha, descripcion, maquina[bool], (area, modelo, descr),
	// prioridad )
	public Object[] getOrdenTrabajoInformationByCodigo(int codigo)
			throws DAOExcepcion {
		int i;
		AvisoIncidencia ai = null;
		Maquina m = null;

		for (i = 0; i < avisos.size(); ++i) {
			if (avisos.get(i).getCodigo() == codigo) {
				ai = avisos.get(i);
				break;
			}
		}

		ordenActual =
				new OrdenTrabajo(ai.getCodigo(), empActual.getDni(), null, -1);

		Object[] result = new Object[8];
		result[0] = ai.getCodigo();
		result[1] = ai.getFechaPeticion().toLocaleString();
		result[2] = ai.getDescripcion();
		result[3] = (ai.getMaquina() != -1);
		if (ai.getMaquina() != -1) {
			m = dal.getMaquinaById(ai.getMaquina());
			result[4] = dal.getAreaByCodArea(m.getCodArea()).getNombre();
			result[5] = m.getModelo();
			result[6] = m.getDescripcion();
		} else {
			result[4] = result[5] = result[6] = null;
		}
		result[7] = partes.get(i).getPrioridad();

		return result;
	}

	public void finalizarOrdenTrabajo(int ordenCodigo) throws DAOExcepcion {
		ordenActual
				.setFechaReparacion(new Timestamp(System.currentTimeMillis()));
		dal.finalizarOrdenTrabajo(ordenActual);
	}
	
	public void eliminarAvisoIncidenciaById(int codigo) throws DAOExcepcion {
		dal.eliminarAvisoIncidenciaById(codigo);
	}

	public void eliminarParteIncidenciaById(int codigo) throws DAOExcepcion {
		dal.eliminarParteIncidenciaById(codigo);
	}
	
	public void eliminarOrdenTrabajoById(int codigo) throws DAOExcepcion {
		dal.eliminarOrdenTrabajoById(codigo);
	}
	
	public void logout() {
		empActual = null;
		partes = null;
		avisos = null;
		ordenActual = null;
	}
}
