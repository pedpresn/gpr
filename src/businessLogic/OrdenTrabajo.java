package businessLogic;

import java.sql.Timestamp;

public class OrdenTrabajo {
	public static final int PENDIENTE  = 0;
	public static final int EN_CURSO   = 1;
	public static final int TERMINADA  = 2;
	public static final int REGISTRADA = 3;
	
	/* Atributos */
	private int codigo;
	private String dni;
	private Timestamp fechaReparacion;
	private int pedido;
	
	public OrdenTrabajo() {
		super();
		this.codigo = -1;
		this.dni = null;
		this.fechaReparacion = null;
		this.pedido = -1;
	}
	
	public OrdenTrabajo(int codigo, String dni, Timestamp fechaReparacion,
			int pedido) {
		super();
		this.codigo = codigo;
		this.dni = dni;
		this.fechaReparacion = fechaReparacion;
		this.pedido = pedido;
	}
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public Timestamp getFechaReparacion() {
		return fechaReparacion;
	}
	public void setFechaReparacion(Timestamp fechaReparacion) {
		this.fechaReparacion = fechaReparacion;
	}
	public int getPedido() {
		return pedido;
	}
	public void setPedido(int pedido) {
		this.pedido = pedido;
	}
	
}