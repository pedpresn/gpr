package businessLogic;

import java.sql.Timestamp;

public class Maquina {
	/* Atributos */
	private int codMaquina;
	private String marca;
	private String modelo;
	private int codArea;
	private String descripcion;
	private Timestamp fechaUltimaRev;

	/* Constructores */
	public Maquina(int codMaquina, String marca, String modelo, int codArea,
			String descripcion, Timestamp fechaUltimaRev) {
		super();
		this.codMaquina = codMaquina;
		this.marca = marca;
		this.modelo = modelo;
		this.codArea = codArea;
		this.descripcion = descripcion;
		this.fechaUltimaRev = fechaUltimaRev;
	}

	/* M�todos */
	public int getCodMaquina() {
		return codMaquina;
	}

	public void setCodMaquina(int codMaquina) {
		this.codMaquina = codMaquina;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getCodArea() {
		return codArea;
	}

	public void setCodArea(int codArea) {
		this.codArea = codArea;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFechaUltimaRev() {
		return fechaUltimaRev;
	}

	public void setFechaUltimaRev(Timestamp fechaUltimaRev) {
		this.fechaUltimaRev = fechaUltimaRev;
	}
}
