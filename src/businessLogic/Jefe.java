package businessLogic;

import java.sql.Date;

public class Jefe extends Empleado {
	/* Constructor */
	public Jefe(String dni, String apellidos, String nombre,
			Date fechaNacimiento, String poblacion, String provincia,
			String cp, String telefono, String nombreUsuario, String contrasena){
		super(dni, apellidos, nombre, fechaNacimiento, poblacion,
			  provincia, cp, telefono, nombreUsuario, contrasena);
	}
}
