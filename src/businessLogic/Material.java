package businessLogic;

public class Material {
	private int codMaterial;
	private String proveedor;
	private String descripcion;
	private double precio;
	private int stock;
	private int stockMin;
	private int stockMax;
	
	public Material() {
		super();
		this.codMaterial = -1;
		this.proveedor = null;
		this.descripcion = null;
		this.precio = -1;
		this.stock = -1;
		this.stockMin = -1;
		this.stockMax = -1;
	}
	
	public Material(int codMaterial, String proveedor, String descripcion,
			double precio, int stock, int stockMin, int stockMax) {
		super();
		this.codMaterial = codMaterial;
		this.proveedor = proveedor;
		this.descripcion = descripcion;
		this.precio = precio;
		this.stock = stock;
		this.stockMin = stockMin;
		this.stockMax = stockMax;
	}

	public int getCodMaterial() {
		return codMaterial;
	}

	public void setCodMaterial(int codMaterial) {
		this.codMaterial = codMaterial;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getStockMin() {
		return stockMin;
	}

	public void setStockMin(int stockMin) {
		this.stockMin = stockMin;
	}

	public int getStockMax() {
		return stockMax;
	}

	public void setStockMax(int stockMax) {
		this.stockMax = stockMax;
	}
	
}
