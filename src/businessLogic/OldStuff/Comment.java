package businessLogic.OldStuff;

import java.util.*;

class Comment {
	/* Attributes */
	private String text;
	private Date date;

	private RegisteredUser registeredUser;

	/* Methods */
	public String getText() {
		return text;
	}

	public Date getDate() {
		return date;
	}

	public void setText(String newText) {
		text = newText;
	}

	public void setDate(Date newDate) {
		date = newDate;
	}

	public RegisteredUser getRegisteredUser() {
		return registeredUser;
	}

	public void setRegisteredUser(RegisteredUser user) {
		this.registeredUser = user;
	}
}