package businessLogic.OldStuff;

import java.sql.Timestamp;
import java.util.*;

public class Article {
	/* Attributes */
	private String id;
	private String title;
	private Timestamp submissionDate;
	private String filePath;
	private String fileName;

	private Area hasArea;
	private String state;
	private Evaluation evaluation;
	private RegisteredUser submissionResponsible;
	private ArrayList<Person> coAuthors;
	private ArrayList<Comment> comments;

	/* Constructors */
	public Article(String id, String title, Timestamp submissionDate, String filePath, String fileName, Area hasArea, RegisteredUser submissionResponsible, String state) {
		super();
		this.id = id;
		this.title = title;
		this.submissionDate = submissionDate;
		this.filePath = filePath;
		this.fileName = fileName;
		this.hasArea = hasArea;
		this.submissionResponsible = submissionResponsible;
		this.state = state;
	}

	/* Methods */
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Timestamp getSubmissionDate() {
		return submissionDate;
	}

	public void setSubmissionDate(Timestamp submissionDate) {
		this.submissionDate = submissionDate;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Area getHasArea() {
		return this.hasArea;
	}

	public void setHasArea(Area newHasArea) {
		this.hasArea = newHasArea;
	}

	public Evaluation getEvaluation() {
		return this.evaluation;
	}

	public void setEvaluation(Evaluation newEvaluation) {
		this.evaluation = newEvaluation;
	}

	public RegisteredUser getSubmissionResponsible() {
		return this.submissionResponsible;
	}

	public void setSubmissionResponsible(RegisteredUser newSubmissionResponsible) {
		this.submissionResponsible = newSubmissionResponsible;
	}

	public Person getCoAuthor(String nif) {
		for (Person p : coAuthors) {
			if (p.getNif() == nif)
				return p;
		}
		return null;
	}

	public void addCoAuthor(Person newCoAuthor) {
		coAuthors.add(newCoAuthor);
	}

	public void removeCoAuthor(Person oldCoAuthor) {
		coAuthors.remove(oldCoAuthor);
	}

	public Comment getComment(RegisteredUser user) {
		for (Comment c : comments) {
			if (c.getRegisteredUser() == user)
				return c;
		}
		return null;
	}

	public void addComment(Comment newComment) {
		comments.add(newComment);
	}

	public void removeComment(Comment oldComment) {
		comments.remove(oldComment);
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}
