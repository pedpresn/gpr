package businessLogic.OldStuff;

import java.util.*;

public class Area {
	/* Attributes */
	private String name;
	private RegisteredUser areaEditor;
	private ArrayList<Article> articles;
	private ArrayList<Article> pendingEvaluation;
	private ArrayList<Article> pendingPublication;
	private ArrayList<Article> proposedForPublication;

	/* Constructors */
	public Area(String name, RegisteredUser areaEditor) {
		super();
		this.name = name;
		this.areaEditor = areaEditor;
	}

	public Area(String name, RegisteredUser areaEditor, ArrayList<Article> articles, ArrayList<Article> pendingEvaluation, ArrayList<Article> pendingPublication) {
		super();
		this.name = name;
		this.areaEditor = areaEditor;
		this.articles = articles;
		this.pendingEvaluation = pendingEvaluation;
		this.pendingPublication = pendingPublication;
	}

	/* Methods */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public RegisteredUser getAreaEditor() {
		return this.areaEditor;
	}

	public void setAreaEditor(RegisteredUser newAreaEditor) {
		this.areaEditor = newAreaEditor;
	}

	public Article getArticle(String title) {
		for (Article a : articles) {
			if (a.getTitle() == title)
				return a;
		}
		return null;
	}

	public void addArticle(Article newArticle) {
		articles.add(newArticle);
	}

	public void removeArticle(Article oldArticle) {
		articles.remove(oldArticle);
	}

	public Article getPendingEvaluation(String title) {
		for (Article a : pendingEvaluation) {
			if (a.getTitle() == title)
				return a;
		}
		return null;
	}

	public void addPendingEvaluation(Article newPendingEvaluation) {
		pendingEvaluation.add(newPendingEvaluation);
	}

	public void removePendingEvaluation(Article oldPendingEvaluation) {
		pendingEvaluation.remove(oldPendingEvaluation);
	}

	public Article getPendingPublication(String title) {
		for (Article a : pendingPublication) {
			if (a.getTitle() == title)
				return a;
		}
		return null;
	}

	public void addPendingPublication(Article newPendingPublication) {
		pendingPublication.add(newPendingPublication);
	}

	public void removePendingPublication(Article oldPendingPublication) {
		pendingPublication.remove(oldPendingPublication);
	}

	public Article getProposedForPublication(String title) {
		for (Article a : proposedForPublication) {
			if (a.getTitle() == title)
				return a;
		}
		return null;
	}

	public void addProposedForPublication(Article newProposedForPublication) {
		pendingPublication.add(newProposedForPublication);
	}

	public void removeProposedForPublication(Article oldProposedForPublication) {
		pendingPublication.remove(oldProposedForPublication);
	}
}