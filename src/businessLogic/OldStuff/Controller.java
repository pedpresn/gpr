package businessLogic.OldStuff;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import dataAccess.OldStuff.DAL;
import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;

public class Controller {
	private static Controller cont = null;
	private DAL dal;
	public static final int NORMAL_USER = 0;
	public static final int AREA_EDITOR = 1;
	public static final int CHIEF_EDITOR = 2;
	public static final String APPLICATION_NAME = "BatHospital";
	private RegisteredUser currentUser;
	public int currentUserType;
	public String currentUserTypeName;

	private Controller() throws DomainExcepcion {
		try {
			dal = DAL.getSingletonDAL();
			currentUser = null;
		} catch (DAOExcepcion e) {
			throw new DomainExcepcion(e.getMessage());
		}
	}

	public static Controller getSingletonController() throws DomainExcepcion {
		if (cont == null)
			cont = new Controller();
		return cont;
	}
	
	public void createArticle(String id, String title, String filePath, String fileName, String areaName, String submissionResponsibleLogin, String state) throws DAOExcepcion {
		Area a = dal.findAreaByName(areaName);
		RegisteredUser ru = dal.findRegisteredUserbyLogin(submissionResponsibleLogin);
		Article art = new Article(id, title, new Timestamp(System.currentTimeMillis()), filePath, fileName, a, ru, state);
		dal.createArticle(art);
	}

	public void createArea(String areaName, String editorLogin, String magazineName) throws DAOExcepcion {
		RegisteredUser ru = dal.findRegisteredUserbyLogin(editorLogin);
		Area a = new Area(areaName, ru);
		dal.createArea(a, magazineName); // this can throw DAOExcepcions
		// ("MAGAZINE_NO_EXISTE",
		// "AREA_EXISTE",
		// "AREA_EDITOR_NO_EXISTE")
		// insertar ru en el map de usuarios registrados
		// insertar a en el map de areas
	}

	public void createMagazine(String magazineName) throws DAOExcepcion {
		Magazine m = new Magazine(magazineName);
		dal.createMagazine(m);
		// insertar m en el map de magazines
	}

	public void createMagazineWithChiefEditor(String magazineName, String nif, String firstName, String lastName, String login, String password, String email, String areas, boolean alerted)
			throws DAOExcepcion {
		RegisteredUser chiefEditor = new RegisteredUser(nif, firstName, lastName, login, password, email, areas, alerted);
		Magazine m = new Magazine(magazineName, chiefEditor);
		dal.createRegisteredUser(chiefEditor);
		dal.createMagazine(m);
		// insertar m y chiefEditor en el map de magazines y registeredUsers
		// respectivamente
	}

	public void createPerson(String nif, String firstName, String lastName) throws DAOExcepcion {
		Person p = new Person(nif, firstName, lastName);
		dal.createPerson(p);
		// insertar p en el map de persons
	}

	public void createRegisteredUser(String nif, String firstName, String lastName, String login, String password, String email, String areas, boolean alerted) throws DAOExcepcion {
		// Person p = new Person(nif, firstName, lastName);
		RegisteredUser ru = new RegisteredUser(nif, firstName, lastName, login, password, email, areas, alerted);
		dal.createRegisteredUser(ru);
		// insertar ru en el map de registered users
	}

	// returns a matrix of objects for each registered user with size 8 (nif,
	// firstName, lastName, login, password, email, areas, alerted)
	public Object[][] listAllRegisteredUsers() throws DAOExcepcion {
		int pos = 0;
		List<RegisteredUser> listRegisteredUsers = dal.findAllRegisteredUsers();
		Object[][] result = new Object[listRegisteredUsers.size()][8];
		for (RegisteredUser ru : listRegisteredUsers) {
			result[pos][0] = ru.getNif();
			result[pos][1] = ru.getFirstName();
			result[pos][2] = ru.getLastName();
			result[pos][3] = ru.getLogin();
			result[pos][4] = ru.getPassword();
			result[pos][5] = ru.getEmail();
			result[pos][6] = ru.getAreas();
			result[pos][7] = ru.isAlerted();
			++pos;
		}
		return result;
	}

	// public Area findAreaByName(String ID) throws DAOExcepcion {
	// return dal.findAreaByName(ID);
	// }
	//
	// public Magazine findMagazineByName(String ID) throws DAOExcepcion {
	// return dal.findMagazineByName(ID);
	// }
	//
	// public Person findPersonbyNif(String ID) throws DAOExcepcion {
	// // if map is empty, findAllPersons, update the map, and return the person
	// return dal.findPersonbyNif(ID);
	// }
	//
	// public RegisteredUser findRegisteredUserbyLogin(String login)
	// throws DAOExcepcion {
	// // if map is empty, findAllRegisteredUsers, update the map, and return
	// the registeredUser
	// return dal.findRegisteredUserbyLogin(login);
	// }

	// returns a matrix of strings with a row for each registered user which is
	// not area editor with size 3 (login, firstName, lastName)
	public String[][] listAllRegisteredUsersNotEditors() throws DAOExcepcion {
		int pos = 0;
		List<RegisteredUser> listRU = dal.findAllRegisteredUsersNotEditors();
		String[][] result = new String[listRU.size()][3];
		for (RegisteredUser ru : listRU) {
			result[pos][0] = ru.getLogin();
			result[pos][1] = ru.getFirstName();
			result[pos][2] = ru.getLastName();
			++pos;
		}
		return result;
	}
	
	public String getArticleTitleFromId(String articleId) throws DAOExcepcion {
		return dal.findArticleById(articleId).getTitle();
	}
	
	public String getArticleAuthorFromId(String articleId) throws DAOExcepcion {
		RegisteredUser author = dal.findArticleById(articleId).getSubmissionResponsible();
		return author.getFirstName() + " " + author.getLastName() + " (" + author.getLogin() + ")";
	}
	
	public String getArticleSubmissionDateFromId(String articleId) throws DAOExcepcion {
		return dal.findArticleById(articleId).getSubmissionDate().toString();
	}
	
	public String getArticleFilePathFromId(String articleId) throws DAOExcepcion {
		Article art = dal.findArticleById(articleId);
		return art.getFilePath()+art.getFileName();
	}
	
	public void acceptArticleById(String articleId, String comment) throws DAOExcepcion {
		Article art = dal.findArticleById(articleId);
		dal.acceptArticle(art, comment);
	}
	
	public void rejectArticleById(String articleId, String comment) throws DAOExcepcion {
		Article art = dal.findArticleById(articleId);
		dal.rejectArticle(art, comment);
	}
	
	public void publicateArticleById(String articleId) throws DAOExcepcion {
		Article art = dal.findArticleById(articleId);
		dal.publicateArticle(art);
	}
	
	// returns a matrix of objects with a row for each article which has
	// not been rejected and its state is Pending_evaluation with size 5 
	// (title, authorLogin, authorFirstName, authorLastName, submissionDate)
	public Object[][] listAllPendingEvaluationArticlesNotRejectedForEditorByLogin(String areaEditorLogin) throws DAOExcepcion {
		int pos = 0;
		List<Article> listArt = dal.findAllPendingEvaluationArticlesNotRejectedForEditorByLogin(areaEditorLogin);
		Object[][] result = new Object[listArt.size()][6];
		for (Article art : listArt) {
			result[pos][0] = art.getId();
			result[pos][1] = art.getTitle();
			result[pos][2] = art.getSubmissionResponsible().getLogin();
			result[pos][3] = art.getSubmissionResponsible().getFirstName();
			result[pos][4] = art.getSubmissionResponsible().getLastName();
			result[pos][5] = art.getSubmissionDate();
			++pos;
		}
		return result;
	}
	
	// returns a matrix of objects with a row for each article which has
	// been accepted and its state is Pending_publication with size 5 
	// (title, authorLogin, authorFirstName, authorLastName, submissionDate)
	public Object[][] listAllPendingPublicationArticlesForEditorByLogin(String areaEditorLogin) throws DAOExcepcion {
		int pos = 0;
		List<Article> listArt = dal.findAllPendingPublicationArticlesForEditorByLogin(areaEditorLogin);
		Object[][] result = new Object[listArt.size()][6];
		for (Article art : listArt) {
			result[pos][0] = art.getId();
			result[pos][1] = art.getTitle();
			result[pos][2] = art.getSubmissionResponsible().getLogin();
			result[pos][3] = art.getSubmissionResponsible().getFirstName();
			result[pos][4] = art.getSubmissionResponsible().getLastName();
			result[pos][5] = art.getSubmissionDate();
			++pos;
		}
		return result;
	}

	// 0 -> Registered User, 1 -> Area editor, 2 -> Chief editor. DAOExcepcion
	// -> User not exists or incorrect password.
	public int checkLoginAndPassword(String login, String password) throws DAOExcepcion {
		RegisteredUser ru = dal.findRegisteredUserbyLogin(login);
		if (ru != null) {
			if (ru.getPassword().equals(password)) {
				List<Magazine> listMagazines = dal.findAllMagazines();
				for (Magazine m : listMagazines) {
					if (m.getChiefEditor().getLogin().equals(login)) {
						currentUser = ru; // currentUser is a ChiefEditor
						currentUserType = CHIEF_EDITOR;
						currentUserTypeName = "Chief Editor";
						return 2;
					}
				}
				List<Area> listAreas = dal.findAllAreas();
				for (Area a : listAreas) {
					if (a.getAreaEditor().getLogin().equals(login)) {
						currentUser = ru; // currentUser is an AreaEditor
						currentUserType = AREA_EDITOR;
						currentUserTypeName = "Area Editor";
						return 1;
					}
				}
				currentUser = ru; // currentUser is a NormalUser
				currentUserType = NORMAL_USER;
				currentUserTypeName = "Registered User";
				return 0;
			} else
				throw new DAOExcepcion("PASSWORD_INCORRECTA");
		} else
			throw new DAOExcepcion("REG_USER_NO_EXISTE");
	}
	
	public void logout() {
		currentUser = null;
	}
	
	public RegisteredUser getCurrentUser() {
		return currentUser;
	}

	// returns a matrix of strings with a row for each area with size 4
	// (areaName, login, firstName, lastName)
	public String[][] listAllAreas() throws DAOExcepcion {
		int pos = 0;
		List<Area> listA = dal.findAllAreas();
		String[][] result = new String[listA.size()][4];
		for (Area a : listA) {
			result[pos][0] = a.getName();
			result[pos][1] = a.getAreaEditor().getLogin();
			result[pos][2] = a.getAreaEditor().getFirstName();
			result[pos][3] = a.getAreaEditor().getLastName();
			++pos;
		}
		return result;
	}

	// returns an array of strings with a row for each magazine
	// (name)
	public String[] listAllMagazines() throws DAOExcepcion {
		int pos = 0;
		List<Magazine> listM = dal.findAllMagazines();
		String[] result = new String[listM.size()];
		for (Magazine m : listM) {
			result[pos] = m.getName();
			++pos;
		}
		return result;
	}
}
