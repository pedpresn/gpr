	package businessLogic.OldStuff;

import java.sql.Timestamp;
import java.util.*;

public class Evaluation {
	/* Attributes */
	private String id;
	private Timestamp date;
	private String result;
	private String comments;

	/* Constructors */
	public Evaluation(String id, Timestamp date, String result, String comments) {
		super();
		this.id = id;
		this.date = date;
		this.result = result;
		this.comments = comments;
	}

	/* Methods */
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
}
