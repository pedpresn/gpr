package businessLogic.OldStuff;

import excepciones.DAOExcepcion;
import dataAccess.OldStuff.AreaDAOImp;
import dataAccess.OldStuff.MagazineDAOImp;
import dataAccess.OldStuff.PersonDAOImp;
import dataAccess.OldStuff.RegisteredUserDAOImp;

public class ApplicationTesting {

	public static void main(String[] args) {
		Person p;

		try {
			// To illustrate how from the business logic layer
			// the dataAccess layer may be accessed by just creating the
			// corresponding DAO object

			// Creating a PersonDAOImp to work with data about Persons in the DB

			PersonDAOImp pDAOImp = new PersonDAOImp();

			// if person with Nif 11111111A does not exist it is created

			if (pDAOImp.findPersonbyNif("11111111A") == null)
				pDAOImp.createPerson(new Person("11111111A", "Javier", "Jaen Martinez"));

			// If person with Nif 22222222A does not exist it is created
			if (pDAOImp.findPersonbyNif("22222222B") == null)
				pDAOImp.createPerson(new Person("22222222B", "M Carmen", "Penades Gramaje"));

			// Find person with nif 11111111A
			p = pDAOImp.findPersonbyNif("11111111A");

			// Si se encuentra un registro en la BD de la persona 11111111A
			// se muestra el Nombre y el Apellido
			if (p != null)
				System.out.print("Name: " + p.getFirstName() + " LastName: " + p.getLastName());

			// We create a DAO object RegisteredUserDAOImp
			RegisteredUserDAOImp urDAOImp = new RegisteredUserDAOImp();

			// If the user jhcanos is not registered we register this user
			// createRegisteredUser creates internally the person
			// therefore it is not necessary to call createPerson

			if (urDAOImp.findRegisteredUserbyLogin("jhcanos") == null)
				urDAOImp.createRegisteredUser(new RegisteredUser("33333333C", "Jose Hilario", "Canos Cerda", "jhcanos", "passwrd1", "jhcanos@dsic.upv.es",
						"Gestion Emergencias, WorkFlows, Bibliotecas Digitales", true));

			AreaDAOImp aDAOImp = new AreaDAOImp();
			MagazineDAOImp mDAOImp = new MagazineDAOImp();

		} catch (DAOExcepcion e) {
			System.out.println("Error de SQL " + e);
		}
	}
}
