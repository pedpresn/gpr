package businessLogic.OldStuff;

import java.sql.*;

import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;

public class ApplicationInitializing {
	static Controller control;
	private static Connection dbcon = null;

	public static void main(String[] args) {
		try {
			Class.forName("org.hsqldb.jdbcDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			dbcon = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/ISW");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			Statement sentencia = dbcon.createStatement();
			sentencia.execute("delete from PUBLIC.EVALUATION;");
			sentencia.execute("delete from PUBLIC.ARTICLE;");
			sentencia.execute("delete from PUBLIC.AREA;");
			sentencia.execute("delete from PUBLIC.MAGAZINE;");
			sentencia.execute("delete from PUBLIC.REGISTERED_USER;");
			sentencia.execute("delete from PUBLIC.PERSON;");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			control = Controller.getSingletonController();
		} catch (DomainExcepcion e) {
			e.printStackTrace();
		}
		try {
			control.createMagazineWithChiefEditor("UPVN", "52644071S", "Javier", "Jaen Martinez", "fjaen", "pazvega", "fjaen@upvnet.upv.es", "Computer Science, Famous People, Cooking, Architecture", true);
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
		try {
			control.createRegisteredUser("45798044F", "Pedro", "Perez Sanchez", "pedpresn", "1234", "pedpresn@inf.upv.es", "Computer Science", false);
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
		try {
			control.createRegisteredUser("70591140T", "Jose Vicente", "Ruiz Cepeda", "josruice", "1234", "josruice@inf.upv.es", "Architecture, Famous People", true);
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
		try {
			control.createRegisteredUser("X3269909E", "Viacheslav", "Brydko", "viabry", "1234", "viabry@inf.upv.es", "Cooking, Computer Science", true);
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
		try {
			control.createRegisteredUser("45929405S", "Miguel Angel", "Grimaldos Moreno", "migrimo", "1234", "migrimo@inf.upv.es", "Architecture", false);
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
		try {
			control.createArea("Computer Science", "pedpresn", "UPVN");
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
		try {
			control.createArea("Cooking", "josruice", "UPVN");
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
		try {
			control.createArea("Famous People", "migrimo", "UPVN");
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
		try {
			control.createArticle("1", "Most Kept Secrets Of Computer Science", "/Users/pedro/Desktop/Articles/ComputerScience/", "MostKeptSecretsOfComputerScience.pdf", "Computer Science", "viabry", "PENDING_EVALUATION");
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
		try {
			control.createArticle("2", "Agile Methodologies", "/Users/pedro/Desktop/Articles/ComputerScience/", "AgileMethodologies.pdf", "Computer Science", "fjaen", "PENDING_EVALUATION");
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
		try {
			control.createArticle("3", "How To Cook A Spanish Paella Like A Boss", "/Users/pedro/Desktop/Articles/Cooking/", "HowToCookASpanishPaellaLikeABoss.pdf", "Cooking", "fjaen", "PENDING_EVALUATION");
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
		try {
			control.createArticle("4", "Divide And Conquer For Dummies", "/Users/pedro/Desktop/Articles/ComputerScience/", "DivideAndConquerForDummies.pdf", "Computer Science", "migrimo", "PENDING_EVALUATION");
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
		try {
			control.createArticle("5", "Using Hash Tables To Get Your Applications High", "/Users/pedro/Desktop/Articles/ComputerScience/", "UsingHashTablesToGetYourApplicationsHigh.pdf", "Computer Science", "josruice", "PENDING_EVALUATION");
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
		try {
			control.createArticle("6", "LateX For Dummies", "/Users/pedro/Desktop/Articles/ComputerScience/", "LatexForDummies.pdf", "Computer Science", "pedpresn", "PENDING_EVALUATION");
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
		try {
			control.createArticle("7", "How To Crack Your Neighbour''s Wi-Fi Like A Boss", "/Users/pedro/Desktop/Articles/ComputerScience/", "HowToCrackYourNeighboursWifiLikeABoss.pdf", "Computer Science", "viabry", "PENDING_PUBLICATION");
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
		try {
			control.createArticle("8", "All About Paz Vega", "/Users/pedro/Desktop/Articles/FamousPeople/", "AllAboutPazVega.pdf", "Famous People", "fjaen", "PENDING_EVALUATION");
		} catch (DAOExcepcion e) {
			e.printStackTrace();
		}
	}

}
