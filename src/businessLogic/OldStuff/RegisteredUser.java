package businessLogic.OldStuff;

public class RegisteredUser extends Person {

	/* Attributes */
	private String login;
	private String password;
	private String email;
	private String areas;
	private boolean alerted;

	/* Constructor */
	public RegisteredUser(String nif, String firstName, String lastName, String login, String password, String email, String areas, boolean alerted) {
		super(nif, firstName, lastName);
		this.login = login;
		this.password = password;
		this.email = email;
		this.areas = areas;
		this.alerted = alerted;
	}

	/* Methods */
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAreas() {
		return areas;
	}

	public void setAreas(String areas) {
		this.areas = areas;
	}

	public boolean isAlerted() {
		return alerted;
	}

	public void setAlerted(boolean alerted) {
		this.alerted = alerted;
	}
}
