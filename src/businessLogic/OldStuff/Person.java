package businessLogic.OldStuff;

public class Person {
	/* Attributes */
	private String nif;
	private String firstName;
	private String lastName;

	/* Constructor */
	public Person(String nif, String firstName, String lastName) {
		super();
		this.nif = nif;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	/* Methods */
	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
