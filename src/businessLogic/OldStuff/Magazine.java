package businessLogic.OldStuff;

import java.util.*;

public class Magazine {
	/* Attributes */
	private String name;
	private RegisteredUser chiefEditor;
	private ArrayList<Issue> issues;
	private ArrayList<Area> areas;

	/* Constructor */
	public Magazine(String name) {
		super();
		this.name = name;
	}

	public Magazine(String name, RegisteredUser chiefEditor) {
		super();
		this.name = name;
		this.chiefEditor = chiefEditor;
	}

	public Magazine(String name, RegisteredUser chiefEditor, ArrayList<Issue> issues, ArrayList<Area> areas) {
		super();
		this.name = name;
		this.chiefEditor = chiefEditor;
		this.issues = issues;
		this.areas = areas;
	}

	/* Methods */
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public RegisteredUser getChiefEditor() {
		return this.chiefEditor;
	}

	public void setChiefEditor(RegisteredUser newChiefEditor) {
		this.chiefEditor = newChiefEditor;
	}

	public Issue getIssue(int number) {
		for (Issue i : issues) {
			if (i.getNumber() == number)
				return i;
		}
		return null;
	}

	public void addIssue(Issue newIssue) {
		issues.add(newIssue);
	}

	public void removeIssue(Issue oldIssue) {
		issues.remove(oldIssue);
	}

	public ArrayList<Area> getAreas() {
		return areas;
	}

	public Area getArea(String name) {
		for (Area a : areas) {
			if (a.getName() == name)
				return a;
		}
		return null;
	}

	public void addArea(Area newArea) {
		areas.add(newArea);
	}

	public void removeArea(Area oldArea) {
		areas.remove(oldArea);
	}
}
