package businessLogic.OldStuff;

import java.util.*;

class Issue {
	/* Attributes */
	private Date publicationDate;
	private int number;

	private ArrayList<Article> articles;

	/* Methods */
	public Article getArticle(String title) {
		for (Article a : articles) {
			if (a.getTitle() == title)
				return a;
		}
		return null;
	}

	public void addArticle(Article newArticle) {
		articles.add(newArticle);
	}

	public void removeArticle(Article oldArticle) {
		articles.remove(oldArticle);
	}

	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
}
