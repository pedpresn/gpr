package businessLogic;

import java.sql.Date;

public class Operario extends Empleado {
	/* Atributos */
	int docArea;
	
	/* Constructor */
	public Operario(String dni, String apellidos, String nombre,
			Date fechaNacimiento, String poblacion, String provincia,
			String cp, String telefono, String nombreUsuario, String contrasena, int docArea){
		super(dni, apellidos, nombre, fechaNacimiento, poblacion,
			  provincia, cp, telefono, nombreUsuario, contrasena);
		this.docArea = docArea;
	}

	/* Metodos */
	public int getDocArea() {
		return docArea;
	}

	public void setDocArea(int docArea) {
		this.docArea = docArea;
	}
}
