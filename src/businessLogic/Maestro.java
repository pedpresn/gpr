package businessLogic;

import java.sql.Date;

public class Maestro extends Empleado {
	/* Atributos */
	int area;
	
	/* Constructor */
	public Maestro(String dni, String apellidos, String nombre,
			Date fechaNacimiento, String poblacion, String provincia,
			String cp, String telefono, String nombreUsuario, String contrasena, int area){
		super(dni, apellidos, nombre, fechaNacimiento, poblacion,
			  provincia, cp, telefono, nombreUsuario, contrasena);
		this.area = area;
	}

	/* Metodos */
	public int getArea() {
		return area;
	}

	public void setArea(int area) {
		this.area = area;
	}
}
