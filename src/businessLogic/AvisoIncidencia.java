package businessLogic;

import java.sql.Timestamp;

public class AvisoIncidencia {
	/* Atributos */
	private int codigo;
	private String descripcion;
	private Timestamp fechaPeticion;
	private int maquina;
	
	/* Constructores */
	/**
	 * @param codigo
	 * @param descripcion
	 * @param fechaPeticion
	 * @param maquina
	 */
	public AvisoIncidencia(int codigo, String descripcion, Timestamp fechaPeticion, int maquina) {
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.fechaPeticion = fechaPeticion;
		this.maquina = maquina;
	}
	
	public AvisoIncidencia() {
		this.codigo = -1;
		this.descripcion = null;
		this.fechaPeticion = null;
		this.maquina = -1;
	}

	/* M�todos */
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFechaPeticion() {
		return fechaPeticion;
	}

	public void setFechaPeticion(Timestamp fechaPeticion) {
		this.fechaPeticion = fechaPeticion;
	}

	public int getMaquina() {
		return maquina;
	}

	public void setMaquina(int maquina) {
		this.maquina = maquina;
	}
}
