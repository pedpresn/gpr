package businessLogic;

import java.sql.Timestamp;

public class PeticionMaterial {
	/* Atributos */
	private int codPed;
	private Timestamp fechaPedido;
	private Timestamp fechaRecepcion;
	private boolean estado;

	public PeticionMaterial() {
		super();
		this.codPed = -1;
		this.fechaPedido = null;
		this.fechaRecepcion = null;
		this.estado = false;
	}
	
	public PeticionMaterial(int codPed, Timestamp fechaPedido,
			Timestamp fechaRecepcion, boolean estado) {
		super();
		this.codPed = codPed;
		this.fechaPedido = fechaPedido;
		this.fechaRecepcion = fechaRecepcion;
		this.estado = estado;
	}

	public int getCodPed() {
		return codPed;
	}

	public void setCodPed(int codPed) {
		this.codPed = codPed;
	}

	public Timestamp getFechaPedido() {
		return fechaPedido;
	}

	public void setFechaPedido(Timestamp fechaPedido) {
		this.fechaPedido = fechaPedido;
	}

	public boolean getEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public Timestamp getFechaRecepcion() {
		return fechaRecepcion;
	}

	public void setFechaRecepcion(Timestamp fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}
}
