package businessLogic;

public class ParteIncidencia {
	/* Atributos */
	private int codigo;
	private int prioridad;
	private int estado;
	private double costeDiario;
	private int codArea;
	
	public ParteIncidencia() {
		super();
		this.codigo = -1;
		this.prioridad = -1;
		this.estado = -1;
		this.costeDiario = -1;
		this.codArea = -1;
	}
	
	public ParteIncidencia(int codigo, int prioridad, int estado,
			double costeDiario, int codArea) {
		super();
		this.codigo = codigo;
		this.prioridad = prioridad;
		this.estado = estado;
		this.costeDiario = costeDiario;
		this.codArea = codArea;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public int getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(int prioridad) {
		this.prioridad = prioridad;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public double getCosteDiario() {
		return costeDiario;
	}

	public void setCosteDiario(double costeDiario) {
		this.costeDiario = costeDiario;
	}

	public int getCodArea() {
		return codArea;
	}

	public void setCodArea(int codArea) {
		this.codArea = codArea;
	}
	
}
