package presentation;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.SwingUtilities;

import presentation.ColumnsAutoSizer;

import businessLogic.Controlador;
import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class MaestroIncidencias extends javax.swing.JFrame implements ActionListener, KeyListener, WindowListener {
	private JButton jButtonVolver;
	private JButton jButtonCrearOrd;
	private JScrollPane jScrollPaneMIncidencias;
	private JTable jTableMaestro;
	private Controlador control;
	private String[] columnNames;
	private Object[][] listOfIncidencias;
	private int selectedIncidenciaId;

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public MaestroIncidencias(Dimension dim, JFrame frame) throws DAOExcepcion {
		try {
			control = Controlador.getSingletonControlador();
		} catch (DAOExcepcion de) {
			de.printStackTrace();
		}
		fillTable();
		initGUI(dim, frame);
	}
	
	private void fillTable() throws DAOExcepcion {
		columnNames = new String[] { "C�digo", "Fecha de Petici�n", "Descripci�n", "Prioridad" };
		// (codigo, fecha, descripcion, prioridad)
		Object[][] listOfIncidenciasRaw = control.getAllIncidenciasOfThisArea();
		listOfIncidencias = new Object[listOfIncidenciasRaw.length][4];
		for (int i = 0; i < listOfIncidenciasRaw.length; ++i) {
			listOfIncidencias[i][0] = new String(listOfIncidenciasRaw[i][0].toString()); // id
			listOfIncidencias[i][1] = new String(listOfIncidenciasRaw[i][1].toString());
			listOfIncidencias[i][2] = new String(listOfIncidenciasRaw[i][2].toString());
			listOfIncidencias[i][3] = control.PRIORIDADES[(Integer) listOfIncidenciasRaw[i][3]];
		}
	}
	
	private void volver() {
		MaestroControlPanel back = new MaestroControlPanel(this.getSize(), this);
		back.show();
		this.dispose();
	}
	
	private void initGUI(Dimension dim, JFrame frame) {
		try {
			this.setTitle(Controlador.APPLICATION_NAME + " - Maestro - Incidencias");
			this.addWindowListener(this);
			this.setSize(485, 277);
			this.setMinimumSize(new java.awt.Dimension(284, 100));
			this.setLocationRelativeTo(frame);
			GroupLayout thisLayout = new GroupLayout((JComponent)getContentPane());
			getContentPane().setLayout(thisLayout);
			{
				jButtonVolver = new JButton();
				jButtonVolver.setText("< Volver");
				jButtonVolver.addActionListener(this);
				jButtonVolver.addKeyListener(this);
			}
			{
				jButtonCrearOrd = new JButton();
				jButtonCrearOrd.setText("Crear �rden de Trabajo");
				jButtonCrearOrd.setEnabled(false);
				jButtonCrearOrd.addActionListener(this);
				jButtonCrearOrd.addKeyListener(this);
				this.getRootPane().setDefaultButton(jButtonCrearOrd);
			}
			{
				jScrollPaneMIncidencias = new JScrollPane();
				{
					jTableMaestro = new JTable(new MyTableModel(columnNames, listOfIncidencias));
					jScrollPaneMIncidencias.setViewportView(jTableMaestro);
					jTableMaestro.setEnabled(true);
					jTableMaestro.setFillsViewportHeight(true);
					jTableMaestro.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
					jTableMaestro.setPreferredSize(new java.awt.Dimension(518, 252));
					ColumnsAutoSizer.sizeColumnsToFit(jTableMaestro);
					jTableMaestro.getSelectionModel().addListSelectionListener(
			                new ListSelectionListener() {
			                    public void valueChanged(ListSelectionEvent event) {
			                    	jButtonCrearOrd.setEnabled(true);
			                        int viewRow = jTableMaestro.getSelectedRow();
			                        if (viewRow < 0) {
			                            selectedIncidenciaId = -1;
			                        } else {
			                            int modelRow = jTableMaestro.convertRowIndexToModel(viewRow);
			                            selectedIncidenciaId = Integer.parseInt((String) jTableMaestro.getValueAt(modelRow, 0));
			                        }
			                    }
			                }
			        );
				}
			}
				thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jScrollPaneMIncidencias, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED, 1, GroupLayout.PREFERRED_SIZE)
					.addGroup(thisLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
					    .addComponent(jButtonVolver, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
					    .addComponent(jButtonCrearOrd, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap());
				thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(thisLayout.createParallelGroup()
					    .addGroup(GroupLayout.Alignment.LEADING, thisLayout.createSequentialGroup()
					        .addComponent(jButtonVolver, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
					        .addGap(0, 204, Short.MAX_VALUE)
					        .addComponent(jButtonCrearOrd, GroupLayout.PREFERRED_SIZE, 176, GroupLayout.PREFERRED_SIZE))
					    .addComponent(jScrollPaneMIncidencias, GroupLayout.Alignment.LEADING, 0, 473, Short.MAX_VALUE))
					.addContainerGap());
			pack();
		} catch (Exception e) {
		    //add your error handling code here
			e.printStackTrace();
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource().equals(jButtonCrearOrd)) {
			try {
				MaestroCrearOrdenTrabajo orden = new MaestroCrearOrdenTrabajo(this.getSize(), this, selectedIncidenciaId);
				orden.show();
				this.dispose();
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error leyendo la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error conectando a la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		} else if(arg0.getSource().equals(jButtonVolver)) {
			volver();
		}
	}
	
	@Override
	public void keyPressed(KeyEvent arg0) {}

	@Override
	public void keyReleased(KeyEvent arg0) {}

	@Override
	public void keyTyped(KeyEvent arg0) {}

	@Override
	public void windowActivated(WindowEvent e) {}

	@Override
	public void windowClosed(WindowEvent e) {}

	@Override
	public void windowClosing(WindowEvent e) {
		volver();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {}

	@Override
	public void windowDeiconified(WindowEvent e) {}

	@Override
	public void windowIconified(WindowEvent e) {}

	@Override
	public void windowOpened(WindowEvent e) {}

}
