package presentation;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.SwingUtilities;

import presentation.OldStuff.AreaEditorControlPanel;
import presentation.OldStuff.ChiefEditorNewArea;
import presentation.OldStuff.ColumnsAutoSizer;

import businessLogic.Controlador;
import businessLogic.OldStuff.Controller;
import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class OperarioOrdenesTrabajoPropias extends javax.swing.JFrame implements ActionListener, KeyListener, WindowListener {
	private JButton jButtonVolver;
	private JButton jButtonVerOrd;
	private JScrollPane jScrollPaneOrdenes;
	private JTable jTableOrdenesPropias;
	private Controlador control;
	private String[] columnNames;
	private Object[][] listOfOrdenes;
	private int selectedOrdenId;

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public OperarioOrdenesTrabajoPropias(Dimension dim, JFrame frame) throws DAOExcepcion {
		try {
			control = Controlador.getSingletonControlador();
		} catch (DAOExcepcion de) {
			de.printStackTrace();
		}
		fillTable();
		initGUI(dim, frame);
	}
	
	private void fillTable() throws DAOExcepcion {
		columnNames = new String[] { "C�digo", "Fecha de Petici�n", "Descripci�n", "Prioridad" };
		// (codigo, fecha, descripcion, prioridad)
		Object[][] listOfOrdenesRaw = control.getAllOrdenesTrabajoOfThisOperario();
		listOfOrdenes = new Object[listOfOrdenesRaw.length][4];
		for (int i = 0; i < listOfOrdenesRaw.length; ++i) {
			listOfOrdenes[i][0] = new String(listOfOrdenesRaw[i][0].toString()); // id
			listOfOrdenes[i][1] = new String(listOfOrdenesRaw[i][1].toString());
			listOfOrdenes[i][2] = new String(listOfOrdenesRaw[i][2].toString());
			listOfOrdenes[i][3] = control.PRIORIDADES[(Integer) listOfOrdenesRaw[i][3]];
		}
	}
	
	private void volver() {
		OperarioControlPanel back = new OperarioControlPanel (this.getSize(), this);
		back.show();
		this.dispose();
	}
	
	private void initGUI(Dimension dim, JFrame frame) {
		try {
			this.setTitle(Controller.APPLICATION_NAME + " - Operario - Lista �rdenes Asignadas");
			this.addWindowListener(this);
			this.setSize(485, 277);
			this.setMinimumSize(new java.awt.Dimension(284, 100));
			this.setLocationRelativeTo(frame);
			GroupLayout thisLayout = new GroupLayout((JComponent)getContentPane());
			getContentPane().setLayout(thisLayout);
			{
				jButtonVolver = new JButton();
				jButtonVolver.setText("< Volver");
				jButtonVolver.addActionListener(this);
				jButtonVolver.addKeyListener(this);
			}
			{
				jButtonVerOrd = new JButton();
				jButtonVerOrd.setText("Ver Detalles de la �rden");
				jButtonVerOrd.setEnabled(false);
				jButtonVerOrd.addActionListener(this);
				jButtonVerOrd.addKeyListener(this);
				this.getRootPane().setDefaultButton(jButtonVerOrd);
			}
			{
				jScrollPaneOrdenes = new JScrollPane();
				jScrollPaneOrdenes.setMinimumSize(new java.awt.Dimension(0, 0));
				{
					jTableOrdenesPropias = new JTable(new MyTableModel(columnNames, listOfOrdenes));
					jScrollPaneOrdenes.setViewportView(jTableOrdenesPropias);
					jTableOrdenesPropias.setEnabled(true);
					jTableOrdenesPropias.setFillsViewportHeight(true);
					jTableOrdenesPropias.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); // only 1 row selectable
					ColumnsAutoSizer.sizeColumnsToFit(jTableOrdenesPropias);
					jTableOrdenesPropias.getSelectionModel().addListSelectionListener(
			                new ListSelectionListener() {
			                    public void valueChanged(ListSelectionEvent event) {
			                    	jButtonVerOrd.setEnabled(true);
			                        int viewRow = jTableOrdenesPropias.getSelectedRow();
			                        if (viewRow < 0) {
			                            selectedOrdenId = -1;
			                        } else {
			                            int modelRow = jTableOrdenesPropias.convertRowIndexToModel(viewRow);
			                            selectedOrdenId = Integer.parseInt((String) jTableOrdenesPropias.getValueAt(modelRow, 0));
			                        }
			                    }
			                }
			        );
				}
			}
				thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jScrollPaneOrdenes, 0, 202, Short.MAX_VALUE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED, 1, GroupLayout.PREFERRED_SIZE)
					.addGroup(thisLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
					    .addComponent(jButtonVolver, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
					    .addComponent(jButtonVerOrd, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap());
				thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(thisLayout.createParallelGroup()
					    .addGroup(GroupLayout.Alignment.LEADING, thisLayout.createSequentialGroup()
					        .addComponent(jButtonVolver, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
					        .addGap(0, 204, Short.MAX_VALUE)
					        .addComponent(jButtonVerOrd, GroupLayout.PREFERRED_SIZE, 176, GroupLayout.PREFERRED_SIZE))
					    .addComponent(jScrollPaneOrdenes, GroupLayout.Alignment.LEADING, 0, 473, Short.MAX_VALUE))
					.addContainerGap());
			pack();
		} catch (Exception e) {
		    //add your error handling code here
			e.printStackTrace();
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource().equals(jButtonVerOrd)) {
			try {
				OperarioEntregarOrdenFinalizada orden = new OperarioEntregarOrdenFinalizada(this.getSize(), this, selectedOrdenId);
				orden.show();
				this.dispose();
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error leyendo la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error conectando a la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		} else if(arg0.getSource().equals(jButtonVolver)) {
			volver();
		}
	}
	
	@Override
	public void keyPressed(KeyEvent arg0) {}

	@Override
	public void keyReleased(KeyEvent arg0) {}

	@Override
	public void keyTyped(KeyEvent arg0) {}

	@Override
	public void windowActivated(WindowEvent e) {}

	@Override
	public void windowClosed(WindowEvent e) {}

	@Override
	public void windowClosing(WindowEvent e) {
		volver();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {}

	@Override
	public void windowDeiconified(WindowEvent e) {}

	@Override
	public void windowIconified(WindowEvent e) {}

	@Override
	public void windowOpened(WindowEvent e) {}

}
