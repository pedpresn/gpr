package presentation;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import businessLogic.Controlador;

import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class JefeControlPanel extends javax.swing.JFrame implements ActionListener, KeyListener, WindowListener {
	private JButton jButtonRegistrarAbs;
	private JButton jButtonAvisoIn;
	private JButton jButtonIncidenciasRes;
	private JToggleButton logoutButton;
	private Controlador control;

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public JefeControlPanel(Dimension dim, JFrame frame) {
		try {
			control = Controlador.getSingletonControlador();
		} catch (DAOExcepcion de) {
			de.printStackTrace();
		}
		initGUI(dim, frame);
	}

	private void salir(){
		control.logout();
		MainLogin logout = new MainLogin(this.getSize(), this);
		logout.show();
		this.dispose();
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource().equals(logoutButton)) {
			salir();
		}
		else if (arg0.getSource().equals(jButtonAvisoIn)) {
			try {
				JefeConsultaIncidencia conIncidencia = new JefeConsultaIncidencia(this.getSize(), this);
				conIncidencia.show();
				this.dispose();
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("NO_INCIDENCIAS_FOUND")) {
					JOptionPane.showMessageDialog(this, "No hay avisos que revisar. Hurra!",
							"INFO", JOptionPane.INFORMATION_MESSAGE);
				} else if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error leyendo la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error conectando a la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	private void initGUI(Dimension dim, JFrame frame) {
		this.setTitle(Controlador.APPLICATION_NAME + " - Jefe");
		this.addWindowListener(this);
		GroupLayout thisLayout = new GroupLayout((JComponent) getContentPane());
		getContentPane().setLayout(thisLayout);
		{
			this.setSize(226, 226);
		}

		this.setLocationRelativeTo(frame);
		this.setResizable(false);
		{
			logoutButton = new JToggleButton();
			logoutButton.setText("Logout");
			logoutButton.setToolTipText("Volver a la pantalla de login");
			logoutButton.addActionListener(this);
			logoutButton.addKeyListener(this);
		}
		{
			jButtonIncidenciasRes = new JButton();
			jButtonIncidenciasRes.setEnabled(false);
			jButtonIncidenciasRes.setToolTipText("Ver incidencias resueltas");
			jButtonIncidenciasRes.setText("Incidencias Resueltas");
		}
		{
			jButtonRegistrarAbs = new JButton();
			jButtonRegistrarAbs.setEnabled(false);
			jButtonRegistrarAbs.setText("Registrar Absentismo");
			jButtonRegistrarAbs.setToolTipText("Registrar el absentismo laboral");
		}
		{
			jButtonAvisoIn = new JButton();
			jButtonAvisoIn.setText("Avisos de Incidencias");
			jButtonAvisoIn.setToolTipText("Ver avisos de incidencias");
			jButtonAvisoIn.addActionListener(this);
			jButtonAvisoIn.addKeyListener(this);
		}
		thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
			.addContainerGap(17, 17)
			.addComponent(jButtonAvisoIn, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
			.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
			.addComponent(jButtonRegistrarAbs, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
			.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
			.addComponent(jButtonIncidenciasRes, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
			.addGap(17)
			.addComponent(logoutButton, 0, 29, Short.MAX_VALUE)
			.addContainerGap(42, 42));
		thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
			.addContainerGap(29, 29)
			.addGroup(thisLayout.createParallelGroup()
			    .addGroup(thisLayout.createSequentialGroup()
			        .addComponent(jButtonAvisoIn, GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE))
			    .addGroup(thisLayout.createSequentialGroup()
			        .addComponent(jButtonRegistrarAbs, GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE))
			    .addGroup(thisLayout.createSequentialGroup()
			        .addComponent(jButtonIncidenciasRes, GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE))
			    .addGroup(thisLayout.createSequentialGroup()
			        .addComponent(logoutButton, GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE)))
			.addContainerGap(35, Short.MAX_VALUE));
		thisLayout.linkSize(SwingConstants.VERTICAL, new Component[] {jButtonIncidenciasRes, jButtonAvisoIn, jButtonRegistrarAbs, logoutButton});
		thisLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {jButtonAvisoIn, jButtonRegistrarAbs, jButtonIncidenciasRes, logoutButton});
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		salir();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

}
