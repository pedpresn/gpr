package presentation;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.SwingUtilities;

import presentation.ColumnsAutoSizer;

import businessLogic.Controlador;
import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class MaestroOrdenes extends javax.swing.JFrame implements ActionListener, KeyListener, WindowListener {
	private JButton jButtonVolver;
	private JButton jButtonVerDetalles;
	private JScrollPane jScrollPaneMIncidencias;
	private JTable jTableMaestro;
	private String[] columnNames;
	private Object[][] listOfOrdenes;
	private Controlador control;
	private int selectedOrdenId;

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public MaestroOrdenes(Dimension dim, JFrame frame) throws DAOExcepcion {
		try {
			control = Controlador.getSingletonControlador();
		} catch (DAOExcepcion de) {
			de.printStackTrace();
		}
		fillTable();
		initGUI(dim,frame);
	}
	
	private void fillTable() throws DAOExcepcion {
		columnNames = new String[] { "C�digo", "Fecha de Reparaci�n", "Descripci�n", "Operario" };
		//listOfOrdenes = control.getAllOrdenesConcluidas();
		listOfOrdenes = new Object[0][0];
	}
	
	private void volver() {
		MaestroControlPanel back = new MaestroControlPanel(this.getSize(), this);
		back.show();
		this.dispose();
	}
	
	private void initGUI(Dimension dim, JFrame frame) {
		try {
			this.addWindowListener(this);
			this.setSize(508, 366);
			this.setMinimumSize(new java.awt.Dimension(508, 366));
			this.setLocationRelativeTo(frame);
			GroupLayout thisLayout = new GroupLayout((JComponent)getContentPane());
			getContentPane().setLayout(thisLayout);
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			this.setTitle("Maestro - Ordenes Concluidas");
			{
				jButtonVolver = new JButton();
				jButtonVolver.setText("< Volver");
				jButtonVolver.addActionListener(this);
			}
			{
				jButtonVerDetalles = new JButton();
				jButtonVerDetalles.setText("Ver Detalles");
				jButtonVerDetalles.addActionListener(this);
				jButtonVerDetalles.setEnabled(false);
			}
			{
				jScrollPaneMIncidencias = new JScrollPane();
				{
					jTableMaestro = new JTable(new MyTableModel(columnNames, listOfOrdenes));
					jScrollPaneMIncidencias.setViewportView(jTableMaestro);
					jTableMaestro.setEnabled(true);
					jTableMaestro.setFillsViewportHeight(true);
					jTableMaestro.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
					jTableMaestro.setPreferredSize(new java.awt.Dimension(518, 252));
					ColumnsAutoSizer.sizeColumnsToFit(jTableMaestro);
					jTableMaestro.getSelectionModel().addListSelectionListener(
			                new ListSelectionListener() {
			                    public void valueChanged(ListSelectionEvent event) {
			                    	jButtonVerDetalles.setEnabled(true);
			                        int viewRow = jTableMaestro.getSelectedRow();
			                        if (viewRow < 0) {
			                            selectedOrdenId = -1;
			                        } else {
			                            int modelRow = jTableMaestro.convertRowIndexToModel(viewRow);
			                            selectedOrdenId = Integer.parseInt((String) jTableMaestro.getValueAt(modelRow, 0));
			                        }
			                    }
			                }
			        );
				}
			}
				thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
					.addContainerGap(11, 11)
					.addComponent(jScrollPaneMIncidencias, 0, 286, Short.MAX_VALUE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED, 1, GroupLayout.PREFERRED_SIZE)
					.addGroup(thisLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
					    .addComponent(jButtonVolver, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
					    .addComponent(jButtonVerDetalles, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap());
				thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(thisLayout.createParallelGroup()
					    .addComponent(jScrollPaneMIncidencias, GroupLayout.Alignment.LEADING, 0, 569, Short.MAX_VALUE)
					    .addGroup(GroupLayout.Alignment.LEADING, thisLayout.createSequentialGroup()
					        .addPreferredGap(jScrollPaneMIncidencias, jButtonVolver, LayoutStyle.ComponentPlacement.INDENT)
					        .addComponent(jButtonVolver, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
					        .addGap(0, 294, Short.MAX_VALUE)
					        .addComponent(jButtonVerDetalles, GroupLayout.PREFERRED_SIZE, 176, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap());
			pack();
			this.setSize(581, 353);
		} catch (Exception e) {
		    //add your error handling code here
			e.printStackTrace();
		}
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		volver();	
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource().equals(jButtonVerDetalles)) {
			try {
				MaestroRegistrarOrdenConcluida orden = new MaestroRegistrarOrdenConcluida(this.getSize(), this, selectedOrdenId);
				orden.show();
				this.dispose();
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error leyendo la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error conectando a la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		} else if(arg0.getSource().equals(jButtonVolver)) {
			volver();
		}
	}

}
