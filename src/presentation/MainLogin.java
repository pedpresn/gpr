package presentation;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;

import businessLogic.Controlador;
import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class MainLogin extends javax.swing.JFrame implements ActionListener, KeyListener, WindowListener {
	private JButton loginButton;
	private JButton cancelarButton;
	private JButton enviarAvisoButton;
	private JPasswordField contrasenaField;
	private JPanel jPanel1;
	private JLabel contrasenaLabel;
	private JLabel usuarioLabel;
	private JTextField usuarioField;
	private Controlador control;

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Auto-generated main method to display this JFrame
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				MainLogin inst = new MainLogin();
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
			}
		});
	}

	public MainLogin() {
		super();
		try {
			control = Controlador.getSingletonControlador();
		} catch (DAOExcepcion de) {
			de.printStackTrace();
		}
		initGUI();
	}

	public MainLogin(Dimension dim, JFrame frame) {
		super();
		try {
			control = Controlador.getSingletonControlador();
		} catch (DAOExcepcion de) {
			de.printStackTrace();
		}
		initGUI();
		this.setLocationRelativeTo(frame);
	}

	private void initGUI() {
		this.setTitle(Controlador.APPLICATION_NAME + " - Login");
		this.setSize(434, 137);
		this.setResizable(false);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		GroupLayout thisLayout = new GroupLayout((JComponent) getContentPane());
		getContentPane().setLayout(thisLayout);
		{
			jPanel1 = new JPanel();
			GroupLayout jPanel1Layout = new GroupLayout((JComponent) jPanel1);
			jPanel1.setLayout(jPanel1Layout);
			{
				usuarioLabel = new JLabel();
				usuarioLabel.setText("Usuario");
			}
			{
				usuarioField = new JTextField();
				usuarioField.setText("");
				usuarioField.setToolTipText("Insertar usuario");
				usuarioField.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, false));
			}
			{
				contrasenaLabel = new JLabel();
				contrasenaLabel.setText("Contrase�a");
			}
			{
				contrasenaField = new JPasswordField();
				contrasenaField.setText("");
				contrasenaField.setToolTipText("Insertar contrase�a");
				contrasenaField.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, false));
			}
			{
				enviarAvisoButton = new JButton();
				enviarAvisoButton.setText("Aviso de incidencia");
				enviarAvisoButton.setToolTipText("Enviar aviso de incidencia");
				enviarAvisoButton.addActionListener(this);
				enviarAvisoButton.addKeyListener(this);
			}
			{
				loginButton = new JButton();
				loginButton.setText("Log In");
				loginButton.setToolTipText("Entrar en la aplicaci�n");
				loginButton.addActionListener(this);
				loginButton.addKeyListener(this);
				this.getRootPane().setDefaultButton(loginButton);
			}
			{
				cancelarButton = new JButton();
				cancelarButton.setText("Cancel");
				cancelarButton.setToolTipText("Cerrar la aplicaci�n");
				cancelarButton.addActionListener(this);
				cancelarButton.addKeyListener(this);
			}
			jPanel1Layout.setHorizontalGroup(jPanel1Layout.createSequentialGroup()
				.addContainerGap()
				.addGroup(jPanel1Layout.createParallelGroup()
				    .addGroup(GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
				        .addComponent(enviarAvisoButton, GroupLayout.PREFERRED_SIZE, 161, GroupLayout.PREFERRED_SIZE)
				        .addGap(0, 51, Short.MAX_VALUE)
				        .addComponent(cancelarButton, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
				        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
				        .addComponent(loginButton, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
				        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 0, GroupLayout.PREFERRED_SIZE))
				    .addGroup(jPanel1Layout.createSequentialGroup()
				        .addGroup(jPanel1Layout.createParallelGroup()
				            .addComponent(contrasenaLabel, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
				            .addComponent(usuarioLabel, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE))
				        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
				        .addGroup(jPanel1Layout.createParallelGroup()
				            .addGroup(jPanel1Layout.createSequentialGroup()
				                .addComponent(contrasenaField, GroupLayout.PREFERRED_SIZE, 289, GroupLayout.PREFERRED_SIZE))
				            .addGroup(jPanel1Layout.createSequentialGroup()
				                .addComponent(usuarioField, GroupLayout.PREFERRED_SIZE, 289, GroupLayout.PREFERRED_SIZE)))
				        .addGap(0, 0, Short.MAX_VALUE)))
				.addContainerGap(19, 19));
			jPanel1Layout.linkSize(SwingConstants.HORIZONTAL, new Component[] {contrasenaField, usuarioField});
			jPanel1Layout.linkSize(SwingConstants.HORIZONTAL, new Component[] {contrasenaLabel, usuarioLabel});
			jPanel1Layout.setVerticalGroup(jPanel1Layout.createSequentialGroup()
				.addContainerGap()
				.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				    .addComponent(usuarioField, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
				    .addComponent(usuarioLabel, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
				.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				    .addComponent(contrasenaField, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
				    .addComponent(contrasenaLabel, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
				.addGap(0, 29, Short.MAX_VALUE)
				.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				    .addComponent(enviarAvisoButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				    .addComponent(cancelarButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				    .addComponent(loginButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
				.addContainerGap());
			jPanel1Layout.linkSize(SwingConstants.VERTICAL, new Component[] {contrasenaField, usuarioField});
		}
		thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
			.addComponent(jPanel1, 0, 137, Short.MAX_VALUE));
		thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
			.addComponent(jPanel1, 0, 434, Short.MAX_VALUE));
		pack();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		if (arg0.getSource().equals(loginButton)) {
			try {
				if (usuarioField.getText().trim().isEmpty()) {
					JOptionPane.showMessageDialog(this, "Debes especificar un usuario.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (contrasenaField.getText().trim().isEmpty()) {
					JOptionPane.showMessageDialog(this, "Debes especificar una contrase�a.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					int userType = control.comprobarUsuarioYContrasena(usuarioField.getText().trim(), contrasenaField.getText().trim());
					switch (userType) {
					case Controlador.OPERARIO:
						OperarioControlPanel logged = new OperarioControlPanel(this.getSize(), this);
						logged.show();
						this.dispose();
						break;
					case Controlador.MAESTRO:
						MaestroControlPanel areaLogged = new MaestroControlPanel(this.getSize(), this);
						areaLogged.show();
						this.dispose();
						break;
					case Controlador.JEFE:
						JefeControlPanel chiefLogged = new JefeControlPanel(this.getSize(), this);
						chiefLogged.show();
						this.dispose();
						break;
					}
				}
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("CONTRASENA_INCORRECTA")) {
					JOptionPane.showMessageDialog(this, "La contrase�a introducida no es correcta, o el usuario no existe.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error leyendo la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error conectando a la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		}

		else if (arg0.getSource().equals(cancelarButton))
			System.exit(0);

		else if (arg0.getSource().equals(enviarAvisoButton)) {
			try {
				MainEnviarAviso mainEnviarAviso = new MainEnviarAviso(this);
				mainEnviarAviso.setModal(true);
				mainEnviarAviso.setVisible(true);
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error leyendo la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error conectando a la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		}

	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		System.exit(0);
	}

	@Override
	public void windowClosing(WindowEvent e) {
		System.exit(0);
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

}
