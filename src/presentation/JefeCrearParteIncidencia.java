package presentation;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;

import businessLogic.Controlador;
import excepciones.DAOExcepcion;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class JefeCrearParteIncidencia extends javax.swing.JFrame implements ActionListener, KeyListener, WindowListener {
	private JLabel jLabelCod;
	private JLabel jLabelPrior;
	private JScrollPane jScrollPaneDescrip;
	private JLabel fecha;
	private JLabel cod;
	private JLabel jLabelOperAsign;
	private JComboBox jComboBoxPrio;
	private JButton jButtonCancel;
	private JComboBox jComboBoxOper;
	private JComboBox jComboBoxMaquina;
	private JCheckBox jCheckBoxIncidencia;
	private JTextArea jTextAreaDescrip;
	private JLabel jLabelDescrip;
	private JLabel jLabelFecha;
	private Controlador control;
	private JButton jButtonRechazar;
	private JButton jButtonCrear;
	private Object[] incidenciaInfo;
	private Object[][] allAreas;
	
	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public JefeCrearParteIncidencia(Dimension dim, JFrame frame, int codigoIncidencia) throws DAOExcepcion {
		try {
			control = Controlador.getSingletonControlador();
		} catch (DAOExcepcion de) {
			de.printStackTrace();
		}
		incidenciaInfo = control.getAvisoIncidenciaById(codigoIncidencia);
		allAreas = control.getAllAreas();
		initGUI(dim, frame);
	}
	
	private void volver() throws DAOExcepcion {
		JefeConsultaIncidencia back = new JefeConsultaIncidencia(this.getSize(), this);
		back.show();
		this.dispose();
	}
	
	private void initGUI(Dimension dim, JFrame frame) {
		try {
			this.addWindowListener(this);
			this.setSize(546, 366);
			this.setMinimumSize(new java.awt.Dimension(546, 366));
			this.setLocationRelativeTo(frame);
			GroupLayout thisLayout = new GroupLayout((JComponent)getContentPane());
			getContentPane().setLayout(thisLayout);
			this.setTitle("Parte de incidencias");
			{
				jLabelCod = new JLabel();
				jLabelCod.setText("Codigo:");
				jLabelCod.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				jLabelFecha = new JLabel();
				jLabelFecha.setText("Fecha de Peticion:");
				jLabelFecha.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				jLabelPrior = new JLabel();
				jLabelPrior.setText("Prioridad:");
				jLabelPrior.setHorizontalTextPosition(SwingConstants.RIGHT);
				jLabelPrior.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				jLabelOperAsign = new JLabel();
				jLabelOperAsign.setText("Area asignada: ");
				jLabelOperAsign.setHorizontalTextPosition(SwingConstants.RIGHT);
				jLabelOperAsign.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				jLabelDescrip = new JLabel();
				jLabelDescrip.setText("Descripcion de la incidencia:  ");
			}
			{
				jButtonCancel = new JButton();
				jButtonCancel.setText("Cancelar");
				jButtonCancel.addActionListener(this);
			}
			{
				jCheckBoxIncidencia = new JCheckBox();
				jCheckBoxIncidencia.setText("Incidencia relacionada con una maquina");
				jCheckBoxIncidencia.setEnabled(false);
				jCheckBoxIncidencia.setSelected((Boolean) incidenciaInfo[3]);
			}
			{
				String[] contents = new String[1];
				ComboBoxModel jComboBoxMaquinaModel;
				if(incidenciaInfo[5]!=null) {
					contents[0] = incidenciaInfo[5]+" ("+incidenciaInfo[6]+")"+" - "+incidenciaInfo[4];
				} else {
					contents[0] = "";
				}
				jComboBoxMaquinaModel = new DefaultComboBoxModel(contents);
				jComboBoxMaquina = new JComboBox();
				jComboBoxMaquina.setModel(jComboBoxMaquinaModel);
				jComboBoxMaquina.setEnabled(false);
			}
			{
				jButtonRechazar = new JButton();
				jButtonRechazar.setText("Rechazar Aviso");
				jButtonRechazar.addActionListener(this);
			}
			{
				jButtonCrear = new JButton();
				jButtonCrear.setText("Crear Incidencia");
				jButtonCrear.addActionListener(this);
				this.getRootPane().setDefaultButton(jButtonCrear);
			}
			{
				jScrollPaneDescrip = new JScrollPane();
				{
					jTextAreaDescrip = new JTextArea();
					jTextAreaDescrip.setText(incidenciaInfo[2].toString());
					jScrollPaneDescrip.setViewportView(jTextAreaDescrip);
					jTextAreaDescrip.setEnabled(false);
					jTextAreaDescrip.setEditable(false);
					jTextAreaDescrip.setPreferredSize(new java.awt.Dimension(477, 22));
				}
			}
			{
				cod = new JLabel();
				cod.setText(incidenciaInfo[0].toString());
			}
			{
				fecha = new JLabel();
				fecha.setText(incidenciaInfo[1].toString());
			}
			{
				String[] contents = Controlador.PRIORIDADES;
				ComboBoxModel jComboBoxPrioModel = new DefaultComboBoxModel(contents);
				jComboBoxPrio = new JComboBox();
				jComboBoxPrio.setModel(jComboBoxPrioModel);
			}
			{
				String[] content = new String[allAreas.length];
				for(int i=0; i<content.length; ++i) {
					content[i] = allAreas[i][1].toString();
				}
				ComboBoxModel jComboBoxOperModel = new DefaultComboBoxModel(content);
				jComboBoxOper = new JComboBox();
				jComboBoxOper.setModel(jComboBoxOperModel);
			}
				thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(thisLayout.createParallelGroup()
					    .addComponent(jLabelCod, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
					    .addComponent(cod, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addGroup(thisLayout.createParallelGroup()
					    .addComponent(jLabelFecha, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
					    .addComponent(fecha, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(jLabelDescrip, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(jScrollPaneDescrip, 0, 66, Short.MAX_VALUE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
					.addComponent(jCheckBoxIncidencia, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(jComboBoxMaquina, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addGroup(thisLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
					    .addComponent(jLabelPrior, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
					    .addComponent(jComboBoxPrio, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addGroup(thisLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
					    .addComponent(jLabelOperAsign, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
					    .addComponent(jComboBoxOper, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
					.addGap(25)
					.addGroup(thisLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
					    .addComponent(jButtonCancel, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					    .addComponent(jButtonCrear, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					    .addComponent(jButtonRechazar, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap());
				thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(thisLayout.createParallelGroup()
					    .addGroup(thisLayout.createSequentialGroup()
					        .addGroup(thisLayout.createParallelGroup()
					            .addComponent(jLabelFecha, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
					            .addComponent(jLabelCod, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
					        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					        .addGroup(thisLayout.createParallelGroup()
					            .addGroup(thisLayout.createSequentialGroup()
					                .addComponent(fecha, GroupLayout.PREFERRED_SIZE, 267, GroupLayout.PREFERRED_SIZE)
					                .addGap(0, 0, Short.MAX_VALUE))
					            .addComponent(cod, GroupLayout.Alignment.LEADING, 0, 267, Short.MAX_VALUE))
					        .addGap(111))
					    .addGroup(GroupLayout.Alignment.LEADING, thisLayout.createSequentialGroup()
					        .addComponent(jLabelDescrip, GroupLayout.PREFERRED_SIZE, 238, GroupLayout.PREFERRED_SIZE)
					        .addGap(0, 32, Short.MAX_VALUE)
					        .addComponent(jButtonRechazar, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
					        .addComponent(jButtonCrear, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE))
					    .addComponent(jScrollPaneDescrip, GroupLayout.Alignment.LEADING, 0, 534, Short.MAX_VALUE)
					    .addGroup(thisLayout.createSequentialGroup()
					        .addComponent(jCheckBoxIncidencia, GroupLayout.PREFERRED_SIZE, 534, GroupLayout.PREFERRED_SIZE)
					        .addGap(0, 0, Short.MAX_VALUE))
					    .addGroup(thisLayout.createSequentialGroup()
					        .addComponent(jComboBoxMaquina, GroupLayout.PREFERRED_SIZE, 534, GroupLayout.PREFERRED_SIZE)
					        .addGap(0, 0, Short.MAX_VALUE))
					    .addGroup(thisLayout.createSequentialGroup()
					        .addGroup(thisLayout.createParallelGroup()
					            .addGroup(thisLayout.createSequentialGroup()
					                .addPreferredGap(jButtonCancel, jLabelOperAsign, LayoutStyle.ComponentPlacement.INDENT)
					                .addGroup(thisLayout.createParallelGroup()
					                    .addComponent(jLabelOperAsign, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
					                    .addComponent(jLabelPrior, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)))
					            .addComponent(jButtonCancel, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE))
					        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					        .addGroup(thisLayout.createParallelGroup()
					            .addGroup(thisLayout.createSequentialGroup()
					                .addComponent(jComboBoxOper, GroupLayout.PREFERRED_SIZE, 399, GroupLayout.PREFERRED_SIZE))
					            .addGroup(thisLayout.createSequentialGroup()
					                .addComponent(jComboBoxPrio, GroupLayout.PREFERRED_SIZE, 399, GroupLayout.PREFERRED_SIZE)))
					        .addGap(0, 0, Short.MAX_VALUE)))
					.addContainerGap());
				thisLayout.linkSize(SwingConstants.VERTICAL, new Component[] {jLabelFecha, fecha, cod, jLabelCod});
				thisLayout.linkSize(SwingConstants.VERTICAL, new Component[] {jComboBoxPrio, jLabelPrior});
				thisLayout.linkSize(SwingConstants.VERTICAL, new Component[] {jLabelOperAsign, jComboBoxOper});
				thisLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {jComboBoxMaquina, jCheckBoxIncidencia});
				thisLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {fecha, cod});
				thisLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {jLabelFecha, jLabelCod});
				thisLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {jLabelOperAsign, jLabelPrior});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource().equals(jButtonCancel)) {
			try {
				volver();
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("NO_INCIDENCIAS_FOUND")) {
					JefeControlPanel back = new JefeControlPanel(this.getSize(), this);
					back.show();
					this.dispose();
				} else if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error leyendo la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_WRITE_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error escribiendo en la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error conectando a la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
		else if(arg0.getSource().equals(jButtonCrear)) {
			try {
				control.crearParteIncidencia(Integer.parseInt(cod.getText()),jComboBoxPrio.getSelectedIndex(),0,0.0,(Integer) allAreas[jComboBoxOper.getSelectedIndex()][0]);
				volver();
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("NO_INCIDENCIAS_FOUND")) {
					JefeControlPanel back = new JefeControlPanel(this.getSize(), this);
					back.show();
					this.dispose();
				} else if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error leyendo la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_WRITE_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error escribiendo en la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error conectando a la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		}else if(arg0.getSource().equals(jButtonRechazar)) {
			try {
				control.eliminarAvisoIncidenciaById(Integer.parseInt(cod.getText()));
				volver();
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("NO_INCIDENCIAS_FOUND")) {
					JefeControlPanel back = new JefeControlPanel(this.getSize(), this);
					back.show();
					this.dispose();
				} else if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error leyendo la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_WRITE_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error escribiendo en la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error conectando a la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		try {
			volver();
		} catch (DAOExcepcion de) {
			if (de.getMessage().equals("NO_INCIDENCIAS_FOUND")) {
				JefeControlPanel back = new JefeControlPanel(this.getSize(), this);
				back.show();
				this.dispose();
			} else if (de.getMessage().equals("DB_READ_ERROR")) {
				JOptionPane.showMessageDialog(this, "Error leyendo la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
			} else if (de.getMessage().equals("DB_WRITE_ERROR")) {
				JOptionPane.showMessageDialog(this, "Error escribiendo en la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
			} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
				JOptionPane.showMessageDialog(this, "Error conectando a la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
			}
		}
		
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}

