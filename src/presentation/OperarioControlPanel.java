package presentation;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.SwingUtilities;

import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;

import businessLogic.Controlador;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class OperarioControlPanel extends javax.swing.JFrame implements ActionListener,
		KeyListener, WindowListener {
	private JButton jButtonOrdenesProp;
	private JButton logoutButton;
	private Controlador control;

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public OperarioControlPanel(Dimension dim, JFrame frame) {
		try {
			control = Controlador.getSingletonControlador();
		} catch (DAOExcepcion de) {
			de.printStackTrace();
		}
		initGUI(dim, frame);
	}
	
	private void salir() {
		control.logout();
		MainLogin logout = new MainLogin(this.getSize(), this);
		logout.show();
		this.dispose();
	}

	private void initGUI(Dimension dim, JFrame frame) {
		this.setTitle("Operario - Panel de Control");
		this.addWindowListener(this);
		GroupLayout thisLayout = new GroupLayout((JComponent) getContentPane());
		getContentPane().setLayout(thisLayout);
		this.setSize(238, 117);
		this.setLocationRelativeTo(frame);
		this.setResizable(false);
		{
			logoutButton = new JButton();
			logoutButton.setText("Logout");
			logoutButton.setToolTipText("Volver a la pantalla de login");
			logoutButton.addActionListener(this);
			logoutButton.addKeyListener(this);
		}
		{
			jButtonOrdenesProp = new JButton();
			jButtonOrdenesProp.setToolTipText("Ver �rdenes de trabajo propias");
			jButtonOrdenesProp.setText("�rdenes de Trabajo Propias ");
			jButtonOrdenesProp.addActionListener(this);
			jButtonOrdenesProp.addKeyListener(this);
		}
		thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
			.addContainerGap(15, 15)
			.addComponent(jButtonOrdenesProp, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
			.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
			.addComponent(logoutButton, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
			.addContainerGap(25, Short.MAX_VALUE));
		thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
			.addContainerGap(14, 14)
			.addGroup(thisLayout.createParallelGroup()
			    .addGroup(thisLayout.createSequentialGroup()
			        .addComponent(jButtonOrdenesProp, GroupLayout.PREFERRED_SIZE, 207, GroupLayout.PREFERRED_SIZE))
			    .addGroup(thisLayout.createSequentialGroup()
			        .addComponent(logoutButton, GroupLayout.PREFERRED_SIZE, 207, GroupLayout.PREFERRED_SIZE)))
			.addContainerGap(17, Short.MAX_VALUE));
		thisLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {jButtonOrdenesProp, logoutButton});

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		if (arg0.getSource().equals(logoutButton)) {
			salir();
		}

		else if (arg0.getSource().equals(jButtonOrdenesProp)) {
			try {
				new OperarioOrdenesTrabajoPropias(this.getSize(), this).show();
				this.dispose();
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("NO_ORDENES_FOUND")) {
					JOptionPane.showMessageDialog(this, "No tienes �rdenes asignadas. Hurra!",
							"INFO", JOptionPane.INFORMATION_MESSAGE);
				} else if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error leyendo la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error conectando a la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent arg0) {}

	@Override
	public void keyReleased(KeyEvent arg0) {}

	@Override
	public void keyTyped(KeyEvent arg0) {}

	@Override
	public void windowActivated(WindowEvent arg0) {}

	@Override
	public void windowClosed(WindowEvent arg0) {}

	@Override
	public void windowClosing(WindowEvent arg0) {
		salir();
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {}

	@Override
	public void windowDeiconified(WindowEvent arg0) {}

	@Override
	public void windowIconified(WindowEvent arg0) {}

	@Override
	public void windowOpened(WindowEvent arg0) {}

}
