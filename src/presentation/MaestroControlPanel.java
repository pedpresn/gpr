package presentation;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.BorderFactory;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import businessLogic.Controlador;

import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class MaestroControlPanel extends javax.swing.JFrame implements ActionListener, KeyListener, WindowListener {
	private JButton jButtonMaterialpp;
	private JButton logoutButton;
	private Controlador control;
	private JButton jButtonIncidencias;
	private JButton jButtonOrdenes;
	private JButton jButtonAbsentismoLab;
	private JButton jButtonMaterialu;
	private JButton jButtonMaquinas;
	private JPanel jPanelGestion;

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public MaestroControlPanel(Dimension dim, JFrame frame) {
		try {
			control = Controlador.getSingletonControlador();
		} catch (DAOExcepcion de) {
			de.printStackTrace();
		}
		initGUI(dim, frame);
	}
	
	private void salir() {
		control.logout();
		MainLogin logout = new MainLogin(this.getSize(), this);
		logout.show();
		this.dispose();
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {

		if (arg0.getSource().equals(logoutButton)) {
			salir();
		}
		else if (arg0.getSource().equals(jButtonIncidencias)) {
			try {
				MaestroIncidencias incidencias = new MaestroIncidencias(this.getSize(), this);
				incidencias.show();
				this.dispose();
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("NO_INCIDENCIAS_FOUND")) {
					JOptionPane.showMessageDialog(this, "No hay incidencias en tu area. Hurra!",
							"INFO", JOptionPane.INFORMATION_MESSAGE);
				} else if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error leyendo la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error conectando a la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
		else if (arg0.getSource().equals(jButtonOrdenes)) {
			try {
				MaestroOrdenes incidencias = new MaestroOrdenes(this.getSize(), this);
				incidencias.show();
				this.dispose();
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("NO_ORDENES_FOUND")) {
					JOptionPane.showMessageDialog(this, "No hay ordenes concluidas en tu area. Hurra!",
							"INFO", JOptionPane.INFORMATION_MESSAGE);
				} else if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error leyendo la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error conectando a la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		}

	} 

	private void initGUI(Dimension dim, JFrame frame) {
		this.setTitle(Controlador.APPLICATION_NAME +" - Maestro");
		this.addWindowListener(this);
		GroupLayout thisLayout = new GroupLayout((JComponent)getContentPane());
		getContentPane().setLayout(thisLayout);
		{
			this.setSize(336, 377);
		}

		this.setLocationRelativeTo(frame);
		this.setResizable(false);
		{
			logoutButton = new JButton();
			logoutButton.setText("Logout");
			logoutButton.setToolTipText("Volver a la pantalla de login");
			logoutButton.addActionListener(this);
			logoutButton.addKeyListener(this);
		}
		{
			jPanelGestion = new JPanel();
			jPanelGestion.setBorder(BorderFactory.createTitledBorder("Gesti�n de Incidencias"));
			{
				jButtonIncidencias = new JButton();
				jPanelGestion.add(jButtonIncidencias);
				jButtonIncidencias.setText("Incidencias del �rea");
				jButtonIncidencias.setToolTipText("Ver incidencias del �rea");
				jButtonIncidencias.setPreferredSize(new java.awt.Dimension(291, 29));
				jButtonIncidencias.addActionListener(this);
				jButtonIncidencias.addKeyListener(this);
			}
			{
				jButtonMaterialpp = new JButton();
				jPanelGestion.add(jButtonMaterialpp);
				jButtonMaterialpp.setText("Material por pedir");
				jButtonMaterialpp.setToolTipText("Ver material por pedir");
				jButtonMaterialpp.setPreferredSize(new java.awt.Dimension(291, 29));
				jButtonMaterialpp.setEnabled(false);
				jButtonMaterialpp.addActionListener(this);
				jButtonMaterialpp.addKeyListener(this);
			}
			{
				jButtonOrdenes = new JButton();
				jPanelGestion.add(jButtonOrdenes);
				jButtonOrdenes.setText("�rdenes de Trabajo Concluidas");
				jButtonOrdenes.setPreferredSize(new java.awt.Dimension(291, 29));
				jButtonOrdenes.setToolTipText("Ver las �rdenes de trabajo finalizadas");
				jButtonOrdenes.addActionListener(this);
				jButtonOrdenes.addKeyListener(this);
			}
		}
		{
			jButtonMaquinas = new JButton();
			jButtonMaquinas.setText("M�quinas con contrato de mantenimiento");
			jButtonMaquinas.setToolTipText("Ver m�quinas con contrato de mantenimiento");
			jButtonMaquinas.setEnabled(false);
		}
		{
			jButtonMaterialu = new JButton();
			jButtonMaterialu.setText("Material Utilizado");
			jButtonMaterialu.setToolTipText("Ver el material utilizado");
			jButtonMaterialu.setEnabled(false);
		}
		{
			jButtonAbsentismoLab = new JButton();
			jButtonAbsentismoLab.setText("Absentismo Laboral");
			jButtonAbsentismoLab.setToolTipText("Consultar absentismo laboral");
			jButtonAbsentismoLab.setEnabled(false);
		}
		thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
			.addContainerGap()
			.addComponent(jPanelGestion, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
			.addGap(21)
			.addComponent(jButtonMaquinas, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
			.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
			.addComponent(jButtonMaterialu, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
			.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
			.addComponent(jButtonAbsentismoLab, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
			.addGap(35)
			.addComponent(logoutButton, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
			.addContainerGap(21, Short.MAX_VALUE));
		thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
			.addContainerGap()
			.addGroup(thisLayout.createParallelGroup()
			    .addComponent(jPanelGestion, GroupLayout.Alignment.LEADING, 0, 324, Short.MAX_VALUE)
			    .addGroup(thisLayout.createSequentialGroup()
			        .addGap(12)
			        .addGroup(thisLayout.createParallelGroup()
			            .addComponent(jButtonMaquinas, GroupLayout.Alignment.LEADING, 0, 297, Short.MAX_VALUE)
			            .addGroup(thisLayout.createSequentialGroup()
			                .addComponent(jButtonMaterialu, GroupLayout.PREFERRED_SIZE, 297, GroupLayout.PREFERRED_SIZE)
			                .addGap(0, 0, Short.MAX_VALUE))
			            .addComponent(jButtonAbsentismoLab, GroupLayout.Alignment.LEADING, 0, 297, Short.MAX_VALUE)
			            .addGroup(thisLayout.createSequentialGroup()
			                .addComponent(logoutButton, GroupLayout.PREFERRED_SIZE, 297, GroupLayout.PREFERRED_SIZE)
			                .addGap(0, 0, Short.MAX_VALUE)))
			        .addGap(15)))
			.addContainerGap());
		thisLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {jButtonMaquinas, jButtonMaterialu, jButtonAbsentismoLab, logoutButton});
	}

	@Override
	public void keyPressed(KeyEvent arg0) {}

	@Override
	public void keyReleased(KeyEvent arg0) {}

	@Override
	public void keyTyped(KeyEvent arg0) {}

	@Override
	public void windowActivated(WindowEvent e) {}

	@Override
	public void windowClosed(WindowEvent e) {}

	@Override
	public void windowClosing(WindowEvent e) {
		salir();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {}

	@Override
	public void windowDeiconified(WindowEvent e) {}

	@Override
	public void windowIconified(WindowEvent e) {}

	@Override
	public void windowOpened(WindowEvent e) {}
}
