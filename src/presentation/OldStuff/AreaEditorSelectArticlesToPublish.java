package presentation.OldStuff;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import presentation.MyTableModel;

import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;
import java.awt.Component;
import java.io.File;
import java.io.IOException;

import businessLogic.OldStuff.Article;
import businessLogic.OldStuff.Controller;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class AreaEditorSelectArticlesToPublish extends javax.swing.JFrame implements ActionListener, KeyListener, WindowListener {
	private JButton backButton;
	private JButton openArticleButton;
	private JButton proposeButton;
	private JTable tableOfArticles;
	private JScrollPane tableScrollPane;
	private String[] columnNames;
	private Object[][] listOfArticles;
	private Controller control;
	private String articleId;

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public AreaEditorSelectArticlesToPublish(Dimension dim, JFrame frame) throws DAOExcepcion {
		try {
			control = Controller.getSingletonController();
		} catch (DomainExcepcion de) {
			de.printStackTrace();
		}
		fillTable();
		initGUI(dim, frame);
	}
	
	private void fillTable() throws DAOExcepcion {
		columnNames = new String[] { "ID", "Article Title", "Author", "Submission Date" };
		try {
			Controller control = Controller.getSingletonController();
			// (id, title, authorLogin, authorFirstName, authorLastName, submissionDate)
			Object[][] listOfArticlesRaw;
			if(control.getCurrentUser() != null)
				listOfArticlesRaw = control.listAllPendingPublicationArticlesForEditorByLogin(control.getCurrentUser().getLogin());
			else
				listOfArticlesRaw = new Object[0][0];
			// (id, title, Author, submissionDate)
			listOfArticles = new Object[listOfArticlesRaw.length][4];
			for (int i = 0; i < listOfArticlesRaw.length; ++i) {
				listOfArticles[i][0] = new String(listOfArticlesRaw[i][0].toString()); // id
				listOfArticles[i][1] = new String(listOfArticlesRaw[i][1].toString());
				listOfArticles[i][2] = new String(listOfArticlesRaw[i][3].toString() + " " + listOfArticlesRaw[i][4].toString() + " (" + listOfArticlesRaw[i][2].toString() + ")");
				listOfArticles[i][3] = listOfArticlesRaw[i][5];
			}
		} catch (DomainExcepcion e) {
			e.printStackTrace();
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		if (arg0.getSource().equals(backButton)) {
			AreaEditorControlPanel back = new AreaEditorControlPanel(this.getSize(), this);
			back.show();
			this.dispose();
		} else if (arg0.getSource().equals(proposeButton)) {
			try {
				control.publicateArticleById(articleId);
				AreaEditorSelectArticlesToPublish again = new AreaEditorSelectArticlesToPublish(this.getSize(), this);
				again.show();
				this.dispose();
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("NO_ART_PENDIENTE_PUBLICACION_FOUND")) {
					AreaEditorControlPanel back = new AreaEditorControlPanel(this.getSize(), this);
					back.show();
					this.dispose();
				} else if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error while reading the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error connecting to the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_WRITE_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error writting to the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		} else if (arg0.getSource().equals(openArticleButton)) {
			if (Desktop.isDesktopSupported()) {
				try {
					File myFile = new File(control.getArticleFilePathFromId(articleId));
					Desktop.getDesktop().open(myFile);
				} catch (IllegalArgumentException iae) {
					try {
						JOptionPane.showMessageDialog(this, "File " + control.getArticleFilePathFromId(articleId) + " doesn't exist.", "ERROR", JOptionPane.ERROR_MESSAGE);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} catch (IOException ex) { // File not found
					try {
						JOptionPane.showMessageDialog(this, "File " + control.getArticleFilePathFromId(articleId) + " doesn't exist.", "ERROR", JOptionPane.ERROR_MESSAGE);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} catch (DAOExcepcion de) {
					if (de.getMessage().equals("DB_READ_ERROR")) {
						JOptionPane.showMessageDialog(this, "Error while reading the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
					} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
						JOptionPane.showMessageDialog(this, "Error connecting to the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
					}
				}
			} else {
				JOptionPane.showMessageDialog(this, "Impossible to open the file.", "ERROR", JOptionPane.ERROR_MESSAGE);
			}
		}

	}

	private void initGUI(Dimension dim, JFrame frame) {
		this.setTitle(Controller.APPLICATION_NAME + " - Area Editor - Evaluate Articles");
		this.addWindowListener(this);
		this.setMinimumSize(new java.awt.Dimension(700, 300));
		{
			Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
			this.setSize((int) (dimension.getWidth() * 0.7), (int) (dimension.getHeight() * 0.5));
		}
		this.setLocationRelativeTo(frame);
		GroupLayout thisLayout = new GroupLayout((JComponent)getContentPane());
		getContentPane().setLayout(thisLayout);
		{
			openArticleButton = new JButton();
			openArticleButton.setText("Open Article");
			openArticleButton.setEnabled(false);
			openArticleButton.setToolTipText("Open the current article with the predefined reader");
			openArticleButton.addKeyListener(this);
			openArticleButton.addActionListener(this);
		}
		{
			proposeButton = new JButton();
			proposeButton.setText("Propose for publication");
			proposeButton.setToolTipText("Propose the selected article for publication in the next issue");
			proposeButton.setEnabled(false);
			proposeButton.addActionListener(this);
			proposeButton.addKeyListener(this);
		}
		{
			backButton = new JButton();
			backButton.setText("Back");
			backButton.setToolTipText("Return to the control panel");
			backButton.addActionListener(this);
			backButton.addKeyListener(this);
		}
		{
			tableScrollPane = new JScrollPane();
			{
				AbstractTableModel leTableModel = new MyTableModel(columnNames, listOfArticles);
				tableOfArticles = new JTable();
				tableOfArticles.setModel(leTableModel);
				tableScrollPane.setViewportView(tableOfArticles);
				tableOfArticles.setEnabled(true);
				tableOfArticles.setFillsViewportHeight(true);
				tableOfArticles.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); // only 1 row selectable
				ColumnsAutoSizer.sizeColumnsToFit(tableOfArticles);
				//When selection changes, provide user with selected row content
		        tableOfArticles.getSelectionModel().addListSelectionListener(
		                new ListSelectionListener() {
		                    public void valueChanged(ListSelectionEvent event) {
		                    	proposeButton.setEnabled(true);
		                    	openArticleButton.setEnabled(true);
		                        int viewRow = tableOfArticles.getSelectedRow();
		                        if (viewRow < 0) {
		                        } else {
		                            int modelRow = tableOfArticles.convertRowIndexToModel(viewRow);
		                            articleId = tableOfArticles.getValueAt(modelRow, 0).toString();
		                        }
		                    }
		                }
		        );
			}
		}
		thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
			.addContainerGap(21, 21)
			.addComponent(tableScrollPane, 0, 337, Short.MAX_VALUE)
			.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
			.addGroup(thisLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
			    .addComponent(proposeButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
			    .addComponent(backButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
			    .addComponent(openArticleButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
			.addContainerGap());
		thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
			.addContainerGap()
			.addGroup(thisLayout.createParallelGroup()
			    .addGroup(GroupLayout.Alignment.LEADING, thisLayout.createSequentialGroup()
			        .addComponent(openArticleButton, GroupLayout.PREFERRED_SIZE, 198, GroupLayout.PREFERRED_SIZE)
			        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
			        .addComponent(proposeButton, GroupLayout.PREFERRED_SIZE, 198, GroupLayout.PREFERRED_SIZE)
			        .addGap(0, 199, Short.MAX_VALUE)
			        .addComponent(backButton, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE))
			    .addComponent(tableScrollPane, GroupLayout.Alignment.LEADING, 0, 709, Short.MAX_VALUE))
			.addContainerGap());
		thisLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {openArticleButton, proposeButton});
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		AreaEditorControlPanel back = new AreaEditorControlPanel(this.getSize(), this);
		back.show();
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
