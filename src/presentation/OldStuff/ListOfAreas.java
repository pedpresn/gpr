package presentation.OldStuff;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;

import businessLogic.OldStuff.Controller;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class ListOfAreas extends javax.swing.JFrame implements WindowListener, KeyListener, ActionListener {
	private JTable tableOfAreas;
	protected JButton backButton;
	private TableModel leTableModel;
	private JScrollPane tableScrollPane;
	private Controller control;

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ListOfAreas(Dimension dim, JFrame frame) throws DAOExcepcion {
		try {
			control = Controller.getSingletonController();
		} catch (DomainExcepcion e) {
			e.printStackTrace();
		}
		fillTable();
		initGUI(dim, frame);
	}

	private void fillTable() throws DAOExcepcion {
		try {
			Controller control = Controller.getSingletonController();
			// (areaName, login, firstName, lastName)
			String[][] listOfAreas = control.listAllAreas();
			String[][] listOfAreasTable = new String[listOfAreas.length][2];
			for (int i = 0; i < listOfAreas.length; ++i) {
				listOfAreasTable[i][0] = new String(listOfAreas[i][0]);
				listOfAreasTable[i][1] = new String(listOfAreas[i][2] + " " + listOfAreas[i][3] + " (" + listOfAreas[i][1] + ")");
			}
			leTableModel = new DefaultTableModel(listOfAreasTable, new String[] { "Area Name", "Area Editor" });
		} catch (DomainExcepcion e) {
			e.printStackTrace();
		}
	}

	private void initGUI(Dimension dim, JFrame frame) {
		{
			Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
			this.setSize((int) (dimension.getWidth() * 0.5), (int) (dimension.getHeight() * 0.5));
		}

		this.setTitle(Controller.APPLICATION_NAME + " - " + control.currentUserTypeName + " - List of Areas");
		this.addWindowListener(this);
		this.setLocationRelativeTo(frame);
		GroupLayout thisLayout = new GroupLayout((JComponent) getContentPane());
		getContentPane().setLayout(thisLayout);
		this.setMinimumSize(new java.awt.Dimension(600, 300));
		{
			tableScrollPane = new JScrollPane();
			{
				tableOfAreas = new JTable();
				tableScrollPane.setViewportView(tableOfAreas);
				tableOfAreas.setModel(leTableModel);
				tableOfAreas.setEnabled(false);
				tableOfAreas.setPreferredSize(new java.awt.Dimension(658, 314));
				tableOfAreas.setFillsViewportHeight(true);
			}
		}
		{
			backButton = new JButton();
			backButton.setText("Back");
			backButton.setToolTipText("Return to the control panel");
			backButton.addActionListener(this);
			backButton.addKeyListener(this);
			this.getRootPane().setDefaultButton(backButton);
		}
		thisLayout.setVerticalGroup(thisLayout.createSequentialGroup().addContainerGap().addComponent(tableScrollPane, 0, 406, Short.MAX_VALUE).addPreferredGap(
				LayoutStyle.ComponentPlacement.UNRELATED).addComponent(backButton, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addContainerGap());
		thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup().addContainerGap().addGroup(
				thisLayout.createParallelGroup().addComponent(tableScrollPane, GroupLayout.Alignment.LEADING, 0, 817, Short.MAX_VALUE).addGroup(GroupLayout.Alignment.LEADING,
						thisLayout.createSequentialGroup().addGap(0, 688, Short.MAX_VALUE).addComponent(backButton, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE))).addContainerGap());
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		if (arg0.getSource().equals(backButton)) {
			if(control.currentUserType == Controller.NORMAL_USER) {
				new NormalUserControlPanel(this.getSize(), this).show();
			}
			else if(control.currentUserType == Controller.AREA_EDITOR) {
				new AreaEditorControlPanel(this.getSize(), this).show();
			}
			else if(control.currentUserType == Controller.CHIEF_EDITOR){
				new ChiefEditorControlPanel(this.getSize(), this).show();
			}
			this.dispose();
		}
	}

	@Override
	public void keyPressed(KeyEvent arg0) {}

	@Override
	public void keyReleased(KeyEvent arg0) {}

	@Override
	public void keyTyped(KeyEvent arg0) {}

	@Override
	public void windowActivated(WindowEvent arg0) {}

	@Override
	public void windowClosed(WindowEvent arg0) {}

	@Override
	public void windowClosing(WindowEvent arg0) {}

	@Override
	public void windowDeactivated(WindowEvent arg0) {}

	@Override
	public void windowDeiconified(WindowEvent arg0) {}

	@Override
	public void windowIconified(WindowEvent arg0) {}

	@Override
	public void windowOpened(WindowEvent arg0) {}
}
