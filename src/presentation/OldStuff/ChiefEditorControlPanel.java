package presentation.OldStuff;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;

import businessLogic.OldStuff.Controller;

import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class ChiefEditorControlPanel extends javax.swing.JFrame implements ActionListener, KeyListener, WindowListener {
	private JButton viewAllUsersButton;
	private JButton newAreaButton;
	private JButton viewAllAreasButton;
	private JToggleButton logoutButton;
	private Controller control;

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ChiefEditorControlPanel(Dimension dim, JFrame frame) {
		try {
			control = Controller.getSingletonController();
		} catch (DomainExcepcion de) {
			de.printStackTrace();
		}
		initGUI(dim, frame);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		if (arg0.getSource().equals(logoutButton)) {
			control.logout();
			MainLogin logout = new MainLogin(this.getSize(), this);
			logout.show();
			this.dispose();

		}

		if (arg0.getSource().equals(viewAllUsersButton)) {
			try {
				ChiefEditorListOfUsers rUsers = new ChiefEditorListOfUsers(this.getSize(), this);
				rUsers.show();
				this.dispose();
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error while reading the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error connecting to the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		}

		if (arg0.getSource().equals(newAreaButton)) {
			try {
				ChiefEditorNewArea nArea = new ChiefEditorNewArea(this.getSize(), this);
				nArea.setModal(true);
				nArea.setVisible(true);
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("NO_USU_REG_NOT_EDITOR_FOUND")) {
					JOptionPane.showMessageDialog(this, "You cannot create a new area because there are not possible editors available.\nA new user must be registered before creating a new area.",
							"ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error while reading the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error connecting to the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		}

		if (arg0.getSource().equals(viewAllAreasButton)) {
			try {
				new ListOfAreas(this.getSize(), this).show();
				this.dispose();
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error while reading the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error connecting to the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		}

	}

	private void initGUI(Dimension dim, JFrame frame) {
		this.setTitle(Controller.APPLICATION_NAME + " - Chief Editor - Control Panel");
		this.addWindowListener(this);
		GroupLayout thisLayout = new GroupLayout((JComponent) getContentPane());
		getContentPane().setLayout(thisLayout);
		{
			this.setSize(800, 80);
		}

		this.setLocationRelativeTo(frame);
		this.setResizable(false);
		{
			logoutButton = new JToggleButton();
			logoutButton.setText("Logout");
			logoutButton.setToolTipText("Return to the login window");
			logoutButton.addActionListener(this);
			logoutButton.addKeyListener(this);
		}
		{
			viewAllAreasButton = new JButton();
			viewAllAreasButton.setText("View All Areas");
			viewAllAreasButton.setToolTipText("View all areas and their editors");
			viewAllAreasButton.addActionListener(this);
			viewAllAreasButton.addKeyListener(this);
		}
		{
			viewAllUsersButton = new JButton();
			viewAllUsersButton.setText("View All Users");
			viewAllUsersButton.setToolTipText("View a list of all the registered users in the system");
			viewAllUsersButton.addActionListener(this);
			viewAllUsersButton.addKeyListener(this);
		}
		{
			newAreaButton = new JButton();
			newAreaButton.setText("New Area");
			newAreaButton.setToolTipText("Create a new area in the system");
			newAreaButton.addActionListener(this);
			newAreaButton.addKeyListener(this);
		}
		thisLayout.setVerticalGroup(thisLayout.createSequentialGroup().addContainerGap(49, 49).addGroup(
				thisLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(newAreaButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
						GroupLayout.PREFERRED_SIZE)
						.addComponent(viewAllUsersButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(
								logoutButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(viewAllAreasButton,
								GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)).addContainerGap(48, 48));
		thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup().addContainerGap().addComponent(newAreaButton, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE).addGap(24)
				.addComponent(viewAllUsersButton, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE).addGap(28).addComponent(viewAllAreasButton, GroupLayout.PREFERRED_SIZE, 128,
						GroupLayout.PREFERRED_SIZE).addGap(191).addComponent(logoutButton, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE).addContainerGap(46, Short.MAX_VALUE));
		thisLayout.linkSize(SwingConstants.VERTICAL, new Component[] { viewAllAreasButton, newAreaButton, viewAllUsersButton, logoutButton });
		thisLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] { viewAllAreasButton, newAreaButton, viewAllUsersButton, logoutButton });
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

}
