package presentation.OldStuff;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.SoftBevelBorder;

import businessLogic.OldStuff.Controller;
import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class MainRegister extends JDialog implements ActionListener, KeyListener {

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private JPanel jPanel1;
	private JLabel lastNameLabel;
	private JLabel usernameLabel;
	private JLabel emailLabel;
	private JButton cancelButton;
	private JButton createButton;
	private JTextField interestsField;
	private JTextField emailAgainField;
	private JLabel emailAgainLabel;
	private JPanel registrationInfoPanel;
	private JPanel personalInfoPanel;
	private JPasswordField passwordAgainField;
	private JLabel passwordAgainLabel;
	private JLabel interestsLabel;
	private JCheckBox alertedCheckBox;
	private JPasswordField passwordField;
	private JTextField emailField;
	private JLabel passwordLabel;
	private JTextField usernameField;
	private JTextField lastNameField;
	private JTextField firstNameField;
	private JTextField nifField;
	private JLabel firstNameLabel;
	private JLabel nifLabel;

	public MainRegister(JFrame frame) {
		initGUI(frame);
	}

	private void initGUI(JFrame frame) {
		{
			this.setTitle(Controller.APPLICATION_NAME + " - Register");
			this.setSize(694, 242);
			this.setResizable(false);
			this.setModal(true);
			this.setLocationRelativeTo(frame);
			{
				jPanel1 = new JPanel();
				getContentPane().add(jPanel1, BorderLayout.CENTER);
				GroupLayout jPanel1Layout = new GroupLayout((JComponent) jPanel1);
				jPanel1.setLayout(jPanel1Layout);
				jPanel1.setPreferredSize(new java.awt.Dimension(675, 391));
				{
					personalInfoPanel = new JPanel();
					GroupLayout personalInfoPanelLayout = new GroupLayout((JComponent) personalInfoPanel);
					personalInfoPanel.setLayout(personalInfoPanelLayout);
					personalInfoPanel.setBorder(BorderFactory.createTitledBorder("Personal Information"));
					{
						nifLabel = new JLabel();
						nifLabel.setText("Nif *");
						nifLabel.setHorizontalAlignment(SwingConstants.LEFT);
						nifLabel.setFont(new java.awt.Font("Dialog", 1, 12));
					}
					{
						nifField = new JTextField();
						nifLabel.setLabelFor(nifField);
						nifField.setToolTipText("Insert a valid NIF (E.g. 12345678Z)");
						nifField.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, false));
					}
					{
						firstNameLabel = new JLabel();
						firstNameLabel.setText("First Name");
						firstNameLabel.setHorizontalAlignment(SwingConstants.LEFT);
					}
					{
						firstNameField = new JTextField();
						firstNameLabel.setLabelFor(firstNameField);
						firstNameField.setToolTipText("Insert your first name");
						firstNameField.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, false));
					}
					{
						lastNameLabel = new JLabel();
						lastNameLabel.setText("Last Name");
						lastNameLabel.setHorizontalAlignment(SwingConstants.LEFT);
					}
					{
						lastNameField = new JTextField();
						lastNameLabel.setLabelFor(lastNameField);
						lastNameField.setToolTipText("Insert your last name");
						lastNameField.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, false));
					}
					{
						emailLabel = new JLabel();
						emailLabel.setText("Email");
						emailLabel.setHorizontalAlignment(SwingConstants.LEFT);
					}
					{
						emailField = new JTextField();
						emailLabel.setLabelFor(emailField);
						emailField.setToolTipText("Insert your email address");
						emailField.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, false));
					}
					{
						emailAgainLabel = new JLabel();
						emailAgainLabel.setText("Repeat Email");
						emailAgainLabel.setHorizontalAlignment(SwingConstants.LEFT);
					}
					{
						emailAgainField = new JTextField();
						emailAgainLabel.setLabelFor(emailAgainField);
						emailAgainField.setToolTipText("Type your email address again");
						emailAgainField.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, false));
					}
					personalInfoPanelLayout.setHorizontalGroup(personalInfoPanelLayout.createSequentialGroup().addContainerGap().addGroup(
							personalInfoPanelLayout.createParallelGroup().addComponent(nifLabel, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE)
									.addComponent(emailLabel, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE).addComponent(firstNameLabel,
											GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE).addComponent(emailField, GroupLayout.Alignment.LEADING,
											GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE).addComponent(nifField, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 127,
											GroupLayout.PREFERRED_SIZE).addComponent(firstNameField, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE))
							.addGap(18).addGroup(
									personalInfoPanelLayout.createParallelGroup().addGroup(
											personalInfoPanelLayout.createSequentialGroup().addComponent(lastNameField, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE)).addGroup(
											personalInfoPanelLayout.createSequentialGroup().addComponent(lastNameLabel, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE)).addGroup(
											personalInfoPanelLayout.createSequentialGroup().addComponent(emailAgainLabel, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE)).addGroup(
											GroupLayout.Alignment.LEADING,
											personalInfoPanelLayout.createSequentialGroup().addComponent(emailAgainField, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE)))
							.addContainerGap(12, Short.MAX_VALUE));
					personalInfoPanelLayout.setVerticalGroup(personalInfoPanelLayout.createSequentialGroup().addComponent(nifLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
							GroupLayout.PREFERRED_SIZE).addComponent(nifField, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addPreferredGap(
							LayoutStyle.ComponentPlacement.UNRELATED).addGroup(
							personalInfoPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(firstNameLabel, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE,
									GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(lastNameLabel, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE,
									GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)).addGroup(
							personalInfoPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(lastNameField, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE,
									GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(firstNameField, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE,
									GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(
							personalInfoPanelLayout.createParallelGroup().addGroup(
									GroupLayout.Alignment.LEADING,
									personalInfoPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(emailLabel, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE,
											GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(emailAgainLabel, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE,
											GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)).addGroup(
									personalInfoPanelLayout.createSequentialGroup().addGap(14).addGroup(
											personalInfoPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(emailAgainField, GroupLayout.Alignment.BASELINE,
													GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(emailField, GroupLayout.Alignment.BASELINE,
													GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)))).addContainerGap(22, 22));
				}
				{
					registrationInfoPanel = new JPanel();
					GroupLayout registrationInfoPanelLayout = new GroupLayout((JComponent) registrationInfoPanel);
					registrationInfoPanel.setLayout(registrationInfoPanelLayout);
					registrationInfoPanel.setBorder(BorderFactory.createTitledBorder("Registration Information"));
					{
						usernameLabel = new JLabel();
						usernameLabel.setText("Username *");
						usernameLabel.setHorizontalAlignment(SwingConstants.LEFT);
						usernameLabel.setFont(new java.awt.Font("Dialog", 1, 12));
					}
					{
						usernameField = new JTextField();
						usernameLabel.setLabelFor(usernameField);
						usernameField.setToolTipText("Insert a username");
						usernameField.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, false));
					}
					{
						passwordLabel = new JLabel();
						passwordLabel.setText("Password *");
						passwordLabel.setHorizontalAlignment(SwingConstants.LEFT);
						passwordLabel.setFont(new java.awt.Font("Dialog", 1, 12));
					}
					{
						passwordField = new JPasswordField();
						passwordLabel.setLabelFor(passwordField);
						passwordField.setToolTipText("Select a password");
						passwordField.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, false));
					}
					{
						passwordAgainLabel = new JLabel();
						passwordAgainLabel.setText("Repeat Password *");
						passwordAgainLabel.setHorizontalAlignment(SwingConstants.LEFT);
						passwordAgainLabel.setFont(new java.awt.Font("Dialog", 1, 12));
					}
					{
						passwordAgainField = new JPasswordField();
						passwordAgainLabel.setLabelFor(passwordAgainField);
						passwordAgainField.setToolTipText("Type your password again");
						passwordAgainField.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, false));
					}
					{
						interestsLabel = new JLabel();
						interestsLabel.setText("Interests");
						interestsLabel.setHorizontalAlignment(SwingConstants.LEFT);
					}
					{
						interestsField = new JTextField();
						interestsLabel.setLabelFor(interestsField);
						interestsField.setToolTipText("Comma-separated list of areas that interest you");
						interestsField.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, false));
					}
					{
						alertedCheckBox = new JCheckBox();
						alertedCheckBox.setText("Alerted?");
						alertedCheckBox.setToolTipText("Check this box to be alerted when your areas of interest are updated");
					}
					registrationInfoPanelLayout.setHorizontalGroup(registrationInfoPanelLayout.createSequentialGroup().addContainerGap().addGroup(
							registrationInfoPanelLayout.createParallelGroup().addComponent(usernameField, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE)
									.addComponent(passwordLabel, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE).addComponent(usernameLabel,
											GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE).addComponent(passwordField, GroupLayout.Alignment.LEADING,
											GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE).addComponent(interestsLabel, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 126,
											GroupLayout.PREFERRED_SIZE).addComponent(interestsField, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE))
							.addGap(32).addGroup(
									registrationInfoPanelLayout.createParallelGroup().addGroup(
											registrationInfoPanelLayout.createSequentialGroup().addComponent(passwordAgainLabel, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE).addGap(0,
													0, Short.MAX_VALUE)).addGroup(
											registrationInfoPanelLayout.createSequentialGroup().addComponent(passwordAgainField, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE).addGap(0,
													0, Short.MAX_VALUE)).addGroup(
											GroupLayout.Alignment.LEADING,
											registrationInfoPanelLayout.createSequentialGroup().addComponent(alertedCheckBox, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
													GroupLayout.PREFERRED_SIZE).addGap(0, 54, Short.MAX_VALUE))).addContainerGap(50, 50));
					registrationInfoPanelLayout.setVerticalGroup(registrationInfoPanelLayout.createSequentialGroup().addComponent(usernameLabel, GroupLayout.PREFERRED_SIZE,
							GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(usernameField, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(
									registrationInfoPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(passwordLabel, GroupLayout.Alignment.BASELINE,
											GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(passwordAgainLabel, GroupLayout.Alignment.BASELINE,
											GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)).addGroup(
									registrationInfoPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(passwordAgainField, GroupLayout.Alignment.BASELINE,
											GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(passwordField, GroupLayout.Alignment.BASELINE,
											GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(
									registrationInfoPanelLayout.createParallelGroup().addGroup(
											GroupLayout.Alignment.LEADING,
											registrationInfoPanelLayout.createSequentialGroup().addComponent(interestsLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
													GroupLayout.PREFERRED_SIZE).addComponent(interestsField, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)).addGroup(
											GroupLayout.Alignment.LEADING,
											registrationInfoPanelLayout.createSequentialGroup().addGap(9).addComponent(alertedCheckBox, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
													GroupLayout.PREFERRED_SIZE))).addContainerGap(12, Short.MAX_VALUE));
				}
				{
					createButton = new JButton();
					createButton.setText("Create");
					createButton.setToolTipText("Register");
					createButton.addActionListener(this);
					createButton.addKeyListener(this);
					this.getRootPane().setDefaultButton(createButton);
				}
				{
					cancelButton = new JButton();
					cancelButton.setText("Cancel");
					cancelButton.setToolTipText("Return to the login window");
					cancelButton.addActionListener(this);
					cancelButton.addKeyListener(this);
				}
				jPanel1Layout.setHorizontalGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addComponent(personalInfoPanel, GroupLayout.PREFERRED_SIZE, 306, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(
								jPanel1Layout.createParallelGroup().addGroup(
										jPanel1Layout.createSequentialGroup().addComponent(registrationInfoPanel, GroupLayout.PREFERRED_SIZE, 356, GroupLayout.PREFERRED_SIZE).addGap(0, 0,
												Short.MAX_VALUE)).addGroup(
										GroupLayout.Alignment.LEADING,
										jPanel1Layout.createSequentialGroup().addGap(0, 204, Short.MAX_VALUE).addComponent(createButton, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
												GroupLayout.PREFERRED_SIZE).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(cancelButton, GroupLayout.PREFERRED_SIZE,
												GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))).addContainerGap());
				jPanel1Layout.setVerticalGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(
						jPanel1Layout.createParallelGroup().addComponent(registrationInfoPanel, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 157, GroupLayout.PREFERRED_SIZE)
								.addComponent(personalInfoPanel, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 157, GroupLayout.PREFERRED_SIZE)).addPreferredGap(
						LayoutStyle.ComponentPlacement.UNRELATED).addGroup(
						jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(createButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE,
								GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(cancelButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE,
								GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)).addContainerGap(57, 57));
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		if (arg0.getSource().equals(createButton)) {
			try {
				if (nifField.getText().trim().isEmpty()) {
					JOptionPane.showMessageDialog(this, "You have to specify a nif.\nAll marked fields are required.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (usernameField.getText().trim().isEmpty()) {
					JOptionPane.showMessageDialog(this, "You have to specify a username.\nAll marked fields are required.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (passwordField.getText().trim().isEmpty()) {
					JOptionPane.showMessageDialog(this, "You have to specify a password.\nAll marked fields are required.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (passwordAgainField.getText().trim().isEmpty()) {
					JOptionPane.showMessageDialog(this, "You have to type your password twice.\nAll marked fields are required.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (!checkCorrectNif(nifField.getText())) {
					JOptionPane.showMessageDialog(this, "Your NIF is not well-formatted.\nA correct NIF is composed of 8 digits \nand its corresponding letter (E.g. 12345678Z).", "ERROR",
							JOptionPane.ERROR_MESSAGE);
				} else if (!emailField.getText().equals(emailAgainField.getText())) {
					JOptionPane.showMessageDialog(this, "Your email and repeated email don't match.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (!passwordField.getText().equals(passwordAgainField.getText())) {
					JOptionPane.showMessageDialog(this, "Your password and repeated password don't match.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					Controller control = Controller.getSingletonController();
					control.createRegisteredUser(this.nifField.getText().trim(), this.firstNameField.getText().trim(), this.lastNameField.getText().trim(), this.usernameField.getText().trim(),
							this.passwordField.getText().trim(), this.emailField.getText().trim(), this.interestsField.getText().trim(), this.alertedCheckBox.isSelected());
					this.dispose();
				}
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("PERSON_EXISTE")) {
					JOptionPane.showMessageDialog(this, "A person with that nif is already registered.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("USU_REG_EXISTE")) {
					JOptionPane.showMessageDialog(this, "Selected user already exists.\nTry another username.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error while reading the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error connecting to the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			} catch (DomainExcepcion e) {
				e.printStackTrace();
			}
		}

		else if (arg0.getSource().equals(cancelButton))
			this.dispose();

	}

	private boolean checkCorrectNif(String nif) {
		int dni;
		String theLetters = "TRWAGMYFPDXBNJZSQVHLCKE";
		if (nif.length() != 9)
			return false;
		try {
			dni = Integer.parseInt(nif.substring(0, 8));
		} catch (NumberFormatException nfe) {
			return false;
		}
		if (nif.toUpperCase().charAt(8) != theLetters.charAt(dni % 23))
			return false;
		return true;
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

}
