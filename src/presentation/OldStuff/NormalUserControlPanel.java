package presentation.OldStuff;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.SwingUtilities;

import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;

import businessLogic.OldStuff.Controller;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class NormalUserControlPanel extends javax.swing.JFrame implements ActionListener,
		KeyListener, WindowListener {
	private JButton viewAllAreasButton;
	private JButton logoutButton;
	private Controller control;

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public NormalUserControlPanel(Dimension dim, JFrame frame) {
		try {
			control = Controller.getSingletonController();
		} catch (DomainExcepcion de) {
			de.printStackTrace();
		}
		initGUI(dim, frame);
	}

	private void initGUI(Dimension dim, JFrame frame) {
		this.setTitle(Controller.APPLICATION_NAME + " - "+ control.currentUserTypeName + " - Control Panel");
		this.addWindowListener(this);
		GroupLayout thisLayout = new GroupLayout((JComponent) getContentPane());
		getContentPane().setLayout(thisLayout);
		{
			this.setSize(414, 100);
		}

		this.setLocationRelativeTo(frame);
		this.setResizable(false);
		{
			logoutButton = new JButton();
			logoutButton.setText("Logout");
			logoutButton.setToolTipText("Return to the login window");
			logoutButton.addActionListener(this);
			logoutButton.addKeyListener(this);
		}
		{
			viewAllAreasButton = new JButton();
			viewAllAreasButton.setText("View All Areas");
			viewAllAreasButton.setToolTipText("View all areas and their editors");
			viewAllAreasButton.addActionListener(this);
			viewAllAreasButton.addKeyListener(this);
		}
		thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
			.addContainerGap()
			.addComponent(viewAllAreasButton, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)
			.addGap(0, 131, Short.MAX_VALUE)
			.addComponent(logoutButton, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
			.addContainerGap());
		thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
			.addContainerGap(27, 27)
			.addGroup(thisLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
			    .addComponent(viewAllAreasButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
			    .addComponent(logoutButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
			.addContainerGap(25, 25));
		thisLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {logoutButton, viewAllAreasButton});

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		if (arg0.getSource().equals(logoutButton)) {
			control.logout();
			MainLogin logout = new MainLogin(this.getSize(), this);
			logout.show();
			this.dispose();
		}

		else if (arg0.getSource().equals(viewAllAreasButton)) {
			try {
				new ListOfAreas(this.getSize(), this).show();
				this.dispose();
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error while reading the database.",
							"ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error connecting to the database.",
							"ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent arg0) {}

	@Override
	public void keyReleased(KeyEvent arg0) {}

	@Override
	public void keyTyped(KeyEvent arg0) {}

	@Override
	public void windowActivated(WindowEvent arg0) {}

	@Override
	public void windowClosed(WindowEvent arg0) {}

	@Override
	public void windowClosing(WindowEvent arg0) {}

	@Override
	public void windowDeactivated(WindowEvent arg0) {}

	@Override
	public void windowDeiconified(WindowEvent arg0) {}

	@Override
	public void windowIconified(WindowEvent arg0) {}

	@Override
	public void windowOpened(WindowEvent arg0) {}

}
