package presentation.OldStuff;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import presentation.MyTableModel;

import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;
import java.awt.Component;

import businessLogic.OldStuff.Article;
import businessLogic.OldStuff.Controller;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class AreaEditorEvaluateArticles extends javax.swing.JFrame implements ActionListener, KeyListener, WindowListener {
	private JButton backButton;
	private JLabel selectedArticleLabel;
	private JTextField selectedArticleField;
	private JButton evaluateButton;
	private JTable tableOfArticles;
	private JScrollPane tableScrollPane;
	private String[] columnNames;
	private Object[][] listOfArticles;
	private Controller control;
	private String selectedArticleId;

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public AreaEditorEvaluateArticles(Dimension dim, JFrame frame) throws DAOExcepcion {
		try {
			control = Controller.getSingletonController();
		} catch (DomainExcepcion de) {
			de.printStackTrace();
		}
		fillTable();
		initGUI(dim, frame);
	}
	
	private void fillTable() throws DAOExcepcion {
		columnNames = new String[] { "ID", "Article Title", "Author", "Submission Date" };
		try {
			Controller control = Controller.getSingletonController();
			// (id, title, authorLogin, authorFirstName, authorLastName, submissionDate)
			Object[][] listOfArticlesRaw;
			if(control.getCurrentUser() != null)
				listOfArticlesRaw = control.listAllPendingEvaluationArticlesNotRejectedForEditorByLogin(control.getCurrentUser().getLogin());
			else
				listOfArticlesRaw = new Object[0][0];
			// (id, title, Author, submissionDate)
			listOfArticles = new Object[listOfArticlesRaw.length][4];
			for (int i = 0; i < listOfArticlesRaw.length; ++i) {
				listOfArticles[i][0] = new String(listOfArticlesRaw[i][0].toString()); // id
				listOfArticles[i][1] = new String(listOfArticlesRaw[i][1].toString());
				listOfArticles[i][2] = new String(listOfArticlesRaw[i][3].toString() + " " + listOfArticlesRaw[i][4].toString() + " (" + listOfArticlesRaw[i][2].toString() + ")");
				listOfArticles[i][3] = listOfArticlesRaw[i][5];
			}
		} catch (DomainExcepcion e) {
			e.printStackTrace();
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		if (arg0.getSource().equals(backButton)) {
			AreaEditorControlPanel back = new AreaEditorControlPanel(this.getSize(), this);
			back.show();
			this.dispose();
		} else if (arg0.getSource().equals(evaluateButton)) {
			try {
				AreaEditorEvaluateArticlesComment comment = new AreaEditorEvaluateArticlesComment(this.getSize(), this, selectedArticleId);
				comment.show();
				this.dispose();
			} catch(DAOExcepcion de) {
				if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error while reading the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error connecting to the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		}

	}

	private void initGUI(Dimension dim, JFrame frame) {
		this.setTitle(Controller.APPLICATION_NAME + " - Area Editor - Evaluate Articles");
		this.addWindowListener(this);
		this.setMinimumSize(new java.awt.Dimension(700, 300));
		{
			Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
			this.setSize((int) (dimension.getWidth() * 0.7), (int) (dimension.getHeight() * 0.5));
		}
		this.setLocationRelativeTo(frame);
		GroupLayout thisLayout = new GroupLayout((JComponent)getContentPane());
		getContentPane().setLayout(thisLayout);
		{
			tableScrollPane = new JScrollPane();
			{
				tableOfArticles = new JTable(new MyTableModel(columnNames, listOfArticles));
				tableScrollPane.setViewportView(tableOfArticles);
				tableOfArticles.setEnabled(true);
				tableOfArticles.setFillsViewportHeight(true);
				tableOfArticles.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); // only 1 row selectable
				ColumnsAutoSizer.sizeColumnsToFit(tableOfArticles);
				//When selection changes, provide user with selected row content
		        tableOfArticles.getSelectionModel().addListSelectionListener(
		                new ListSelectionListener() {
		                    public void valueChanged(ListSelectionEvent event) {
		                    	evaluateButton.setEnabled(true);
		                    	selectedArticleLabel.setEnabled(true);
		                    	selectedArticleField.setEnabled(true);
		                        int viewRow = tableOfArticles.getSelectedRow();
		                        if (viewRow < 0) {
		                            selectedArticleField.setText("");
		                        } else {
		                            int modelRow = tableOfArticles.convertRowIndexToModel(viewRow);
		                            selectedArticleId = tableOfArticles.getValueAt(modelRow, 0).toString();
		                            selectedArticleField.setText( String.format("%s", tableOfArticles.getValueAt(modelRow, 1).toString()) );
		                        }
		                    }
		                }
		        );
		        tableOfArticles.addKeyListener(this);
			}
		}
		{
			selectedArticleLabel = new JLabel();
			selectedArticleLabel.setText("Selected Article:");
			selectedArticleLabel.setEnabled(false);
		}
		{
			selectedArticleField = new JTextField();
			selectedArticleField.setEditable(false);
			selectedArticleField.setHorizontalAlignment(SwingConstants.LEFT);
			selectedArticleField.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
			selectedArticleField.setEnabled(false);
		}
		{
			evaluateButton = new JButton();
			evaluateButton.setText("Evaluate");
			evaluateButton.setToolTipText("Evaluate the selected article");
			evaluateButton.setEnabled(false);
			evaluateButton.addActionListener(this);
			evaluateButton.addKeyListener(this);
			this.getRootPane().setDefaultButton(evaluateButton);
		}
		{
			backButton = new JButton();
			backButton.setText("Back");
			backButton.setToolTipText("Return to the control panel");
			backButton.addActionListener(this);
			backButton.addKeyListener(this);
		}
		thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
			.addContainerGap(21, 21)
			.addComponent(tableScrollPane, 0, 336, Short.MAX_VALUE)
			.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
			.addGroup(thisLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
			    .addComponent(evaluateButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
			    .addComponent(selectedArticleLabel, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
			    .addComponent(selectedArticleField, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
			    .addComponent(backButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
			.addContainerGap());
		thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
			.addContainerGap()
			.addGroup(thisLayout.createParallelGroup()
			    .addGroup(GroupLayout.Alignment.LEADING, thisLayout.createSequentialGroup()
			        .addComponent(selectedArticleLabel, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
			        .addComponent(selectedArticleField, 0, 470, Short.MAX_VALUE)
			        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
			        .addComponent(evaluateButton, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
			        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
			        .addComponent(backButton, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE))
			    .addComponent(tableScrollPane, GroupLayout.Alignment.LEADING, 0, 762, Short.MAX_VALUE))
			.addContainerGap());
		thisLayout.linkSize(SwingConstants.VERTICAL, new Component[] {evaluateButton, backButton});
		thisLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {evaluateButton, backButton});
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		if(arg0.getKeyCode() == KeyEvent.VK_ENTER) {
			this.actionPerformed(new ActionEvent(evaluateButton, 0, "" ));
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		AreaEditorControlPanel back = new AreaEditorControlPanel(this.getSize(), this);
		back.show();
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
