package presentation.OldStuff;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.LayoutStyle;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import presentation.MyTableModel;

import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;

import businessLogic.OldStuff.Controller;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class ChiefEditorListOfUsers extends javax.swing.JFrame implements ActionListener, KeyListener, WindowListener {
	private JTable tableOfRegisteredUsers;
	private JButton backButton;
	private JScrollPane tableScrollPane;
	private String[] columnNames;
	private Object[][] listOfRegUsers;

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ChiefEditorListOfUsers(Dimension dim, JFrame frame) throws DAOExcepcion {
		fillTable();
		initGUI(dim, frame);
	}

	private void fillTable() throws DAOExcepcion {
		columnNames = new String[] { "Nif", "First Name", "Last Name", "Login", "Password", "Email", "Interests", "Alerted" };
		try {
			Controller control = Controller.getSingletonController();
			// (areaName, login, firstName, lastName)
			listOfRegUsers = control.listAllRegisteredUsers();
		} catch (DomainExcepcion e) {
			e.printStackTrace();
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		if (arg0.getSource().equals(backButton)) {

			ChiefEditorControlPanel back = new ChiefEditorControlPanel(this.getSize(), this);
			back.show();
			this.dispose();

		}

	}

	private void initGUI(Dimension dim, JFrame frame) {
		this.setTitle(Controller.APPLICATION_NAME + " - Chief Editor - List of Users");
		this.addWindowListener(this);
		this.setMinimumSize(new java.awt.Dimension(600, 300));
		{
			Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
			this.setSize((int) (dimension.getWidth() * 0.7), (int) (dimension.getHeight() * 0.5));
		}
		this.setLocationRelativeTo(frame);
		GroupLayout thisLayout = new GroupLayout((JComponent) getContentPane());
		getContentPane().setLayout(thisLayout);
		{
			tableScrollPane = new JScrollPane();
			{

				tableOfRegisteredUsers = new JTable(new MyTableModel(columnNames, listOfRegUsers));
				tableScrollPane.setViewportView(tableOfRegisteredUsers);
				tableOfRegisteredUsers.setEnabled(false);
				tableOfRegisteredUsers.setFillsViewportHeight(true);
				tableOfRegisteredUsers.setPreferredSize(new java.awt.Dimension(673, 305));
			}
		}
		{
			backButton = new JButton();
			backButton.setText("Back");
			backButton.addActionListener(this);
			backButton.addKeyListener(this);
			this.getRootPane().setDefaultButton(backButton);
		}
		thisLayout.setVerticalGroup(thisLayout.createSequentialGroup().addContainerGap().addComponent(tableScrollPane, 0, 318, Short.MAX_VALUE).addPreferredGap(
				LayoutStyle.ComponentPlacement.UNRELATED, 1, GroupLayout.PREFERRED_SIZE).addComponent(backButton, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addContainerGap());
		thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup().addContainerGap().addGroup(
				thisLayout.createParallelGroup().addComponent(tableScrollPane, GroupLayout.Alignment.LEADING, 0, 676, Short.MAX_VALUE)
						.addGroup(
								GroupLayout.Alignment.LEADING,
								thisLayout.createSequentialGroup().addGap(0, 595, Short.MAX_VALUE).addComponent(backButton, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
										GroupLayout.PREFERRED_SIZE))).addContainerGap());

	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		ChiefEditorControlPanel back = new ChiefEditorControlPanel(this.getSize(), this);
		back.show();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

}
