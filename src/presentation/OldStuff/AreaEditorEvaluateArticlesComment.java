package presentation.OldStuff;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;

import javax.swing.WindowConstants;
import javax.swing.SwingUtilities;

import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;
import java.awt.Component;
import java.io.File;
import java.io.IOException;

import businessLogic.OldStuff.Controller;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class AreaEditorEvaluateArticlesComment extends javax.swing.JFrame implements ActionListener, KeyListener, WindowListener {
	private JLabel commentLabel;
	private JLabel submissionDate;
	private JLabel author;
	private JLabel title;
	private JLabel id;
	private JButton openArticleButton;
	private JLabel submissionDateLabel;
	private JLabel authorLabel;
	private JLabel titleLabel;
	private JLabel idLabel;
	private JPanel articleInformationPanel;
	private JButton rejectButton;
	private JButton backButton;
	private JButton acceptButton;
	private JTextPane commentField;
	private JScrollPane scrollPane;
	private Controller control;
	private String articleId;

	/**
	 * Auto-generated main method to display this JFrame
	 */

	public AreaEditorEvaluateArticlesComment(Dimension dim, JFrame frame, String articleId) throws DAOExcepcion {
		super();
		try {
			control = Controller.getSingletonController();
		} catch (DomainExcepcion e) {
			e.printStackTrace();
		}
		this.articleId = articleId;
		initGUI(dim, frame);
	}

	private void initGUI(Dimension dim, JFrame frame) throws DAOExcepcion {
		this.setTitle(Controller.APPLICATION_NAME + " - Area Editor - Evaluate Article");
		this.addWindowListener(this);
		this.setMinimumSize(new java.awt.Dimension(500, 300));
		{
			Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
			this.setSize((int) (dimension.getWidth() * 0.6), (int) (dimension.getHeight() * 0.6));
		}
		this.setLocationRelativeTo(frame);
		GroupLayout thisLayout = new GroupLayout((JComponent) getContentPane());
		getContentPane().setLayout(thisLayout);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		{
			commentLabel = new JLabel();
			commentLabel.setText("Comment:");
		}
		{
			scrollPane = new JScrollPane();
			{
				commentField = new JTextPane();
				scrollPane.setViewportView(commentField);
				commentField.setPreferredSize(new java.awt.Dimension(543, 199));
			}
		}
		{
			articleInformationPanel = new JPanel();
			GroupLayout articleInformationPanelLayout = new GroupLayout((JComponent) articleInformationPanel);
			articleInformationPanel.setLayout(articleInformationPanelLayout);
			articleInformationPanel.setBorder(BorderFactory.createTitledBorder("Article Information"));
			{
				idLabel = new JLabel();
				idLabel.setText("Id:");
				idLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				submissionDateLabel = new JLabel();
				submissionDateLabel.setText("Submission Date:");
				submissionDateLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				openArticleButton = new JButton();
				openArticleButton.setText("Open Article");
				openArticleButton.setToolTipText("Open the current article with the predefined reader");
				openArticleButton.addKeyListener(this);
				openArticleButton.addActionListener(this);
			}
			{
				id = new JLabel();
				id.setText(articleId);
			}
			{
				title = new JLabel();
				title.setText(control.getArticleTitleFromId(articleId));
			}
			{
				author = new JLabel();
				author.setText(control.getArticleAuthorFromId(articleId));
			}
			{
				submissionDate = new JLabel();
				submissionDate.setText(control.getArticleSubmissionDateFromId(articleId));
			}
			{
				titleLabel = new JLabel();
				titleLabel.setText("Title:");
				titleLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				authorLabel = new JLabel();
				authorLabel.setText("Author:");
				authorLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			articleInformationPanelLayout.setHorizontalGroup(articleInformationPanelLayout.createSequentialGroup().addContainerGap().addGroup(
					articleInformationPanelLayout.createParallelGroup().addComponent(authorLabel, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
							.addComponent(titleLabel, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE).addComponent(idLabel, GroupLayout.Alignment.LEADING,
									GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE).addComponent(submissionDateLabel, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 112,
									GroupLayout.PREFERRED_SIZE)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(
					articleInformationPanelLayout.createParallelGroup().addComponent(id, GroupLayout.Alignment.LEADING, 0, 285, Short.MAX_VALUE).addComponent(title, GroupLayout.Alignment.LEADING, 0,
							285, Short.MAX_VALUE).addComponent(author, GroupLayout.Alignment.LEADING, 0, 285, Short.MAX_VALUE).addComponent(submissionDate, GroupLayout.Alignment.LEADING, 0, 285,
							Short.MAX_VALUE)).addGap(0, 22, GroupLayout.PREFERRED_SIZE).addComponent(openArticleButton, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE).addContainerGap());
			articleInformationPanelLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] { submissionDateLabel, authorLabel, titleLabel, idLabel });
			articleInformationPanelLayout.setVerticalGroup(articleInformationPanelLayout.createSequentialGroup().addGroup(
					articleInformationPanelLayout.createParallelGroup().addComponent(idLabel, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
							GroupLayout.PREFERRED_SIZE).addComponent(id, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)).addPreferredGap(
					LayoutStyle.ComponentPlacement.UNRELATED).addGroup(
					articleInformationPanelLayout.createParallelGroup().addComponent(titleLabel, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
							GroupLayout.PREFERRED_SIZE).addComponent(title, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)).addPreferredGap(
					LayoutStyle.ComponentPlacement.UNRELATED).addGroup(
					articleInformationPanelLayout.createParallelGroup().addComponent(authorLabel, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
							GroupLayout.PREFERRED_SIZE).addComponent(author, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)).addPreferredGap(
					LayoutStyle.ComponentPlacement.UNRELATED).addGroup(
					articleInformationPanelLayout.createParallelGroup().addComponent(submissionDateLabel, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
							GroupLayout.PREFERRED_SIZE).addComponent(submissionDate, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)).addPreferredGap(
					LayoutStyle.ComponentPlacement.RELATED, 0, Short.MAX_VALUE).addComponent(openArticleButton, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap());
		}
		{
			rejectButton = new JButton();
			rejectButton.setText("Reject");
			rejectButton.setToolTipText("Reject this article");
			rejectButton.addActionListener(this);
			rejectButton.addKeyListener(this);
		}
		{
			acceptButton = new JButton();
			acceptButton.setText("Accept");
			acceptButton.setToolTipText("Accept this article and move it to \"Pending for publication\"");
			acceptButton.addActionListener(this);
			acceptButton.addKeyListener(this);
		}
		{
			backButton = new JButton();
			backButton.setText("Back");
			backButton.setToolTipText("Cancel evaluation and return to the list of articles to evaluate");
			backButton.addActionListener(this);
			backButton.addKeyListener(this);
		}
		thisLayout.setVerticalGroup(thisLayout.createSequentialGroup().addContainerGap().addComponent(articleInformationPanel, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(commentLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(scrollPane, 0, 203, Short.MAX_VALUE).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(
						thisLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(acceptButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE,
								GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(backButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE,
								GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(rejectButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE,
								GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)).addContainerGap());
		thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup().addContainerGap().addGroup(
				thisLayout.createParallelGroup().addGroup(
						GroupLayout.Alignment.LEADING,
						thisLayout.createSequentialGroup().addGroup(
								thisLayout.createParallelGroup().addComponent(acceptButton, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE).addGroup(
										GroupLayout.Alignment.LEADING,
										thisLayout.createSequentialGroup().addComponent(commentLabel, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE).addGap(29))).addPreferredGap(
								LayoutStyle.ComponentPlacement.UNRELATED).addComponent(rejectButton, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE).addGap(0, 283, Short.MAX_VALUE)
								.addComponent(backButton, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)).addComponent(articleInformationPanel, GroupLayout.Alignment.LEADING, 0, 576,
						Short.MAX_VALUE).addComponent(scrollPane, GroupLayout.Alignment.LEADING, 0, 576, Short.MAX_VALUE)).addContainerGap());
		thisLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] { rejectButton, acceptButton, backButton });
		pack();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource().equals(backButton)) {
			try {
				AreaEditorEvaluateArticles back = new AreaEditorEvaluateArticles(this.getSize(), this);
				back.show();
				this.dispose();
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("NO_ART_PENDIENTE_NOT_REJECTED_FOUND")) { // no more articles to evaluate
					AreaEditorControlPanel back = new AreaEditorControlPanel(this.getSize(), this);
					back.show();
					this.dispose();
				} else if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error while reading the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error connecting to the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_WRITE_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error writting to the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		} else if (arg0.getSource().equals(openArticleButton)) {
			if (Desktop.isDesktopSupported()) {
				try {
					File myFile = new File(control.getArticleFilePathFromId(articleId));
					Desktop.getDesktop().open(myFile);
				} catch (IllegalArgumentException iae) {
					try {
						JOptionPane.showMessageDialog(this, "File " + control.getArticleFilePathFromId(articleId) + " doesn't exist.", "ERROR", JOptionPane.ERROR_MESSAGE);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} catch (IOException ex) { // File not found
					try {
						JOptionPane.showMessageDialog(this, "File " + control.getArticleFilePathFromId(articleId) + " doesn't exist.", "ERROR", JOptionPane.ERROR_MESSAGE);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} catch (DAOExcepcion de) {
					if (de.getMessage().equals("DB_READ_ERROR")) {
						JOptionPane.showMessageDialog(this, "Error while reading the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
					} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
						JOptionPane.showMessageDialog(this, "Error connecting to the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
					}
				}
			} else {
				JOptionPane.showMessageDialog(this, "Impossible to open the file.", "ERROR", JOptionPane.ERROR_MESSAGE);
			}
		} else if (arg0.getSource().equals(acceptButton)) {
			try {
				control.acceptArticleById(articleId, commentField.getText());
				AreaEditorEvaluateArticles back = new AreaEditorEvaluateArticles(this.getSize(), this);
				back.show();
				this.dispose();
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("NO_ART_PENDIENTE_NOT_REJECTED_FOUND")) { // no more articles to evaluate
					AreaEditorControlPanel back = new AreaEditorControlPanel(this.getSize(), this);
					back.show();
					this.dispose();
				} else if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error while reading the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error connecting to the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		} else if (arg0.getSource().equals(rejectButton)) {
			try {
				control.rejectArticleById(articleId, commentField.getText());
				AreaEditorEvaluateArticles back = new AreaEditorEvaluateArticles(this.getSize(), this);
				back.show();
				this.dispose();
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("NO_ART_PENDIENTE_NOT_REJECTED_FOUND")) { // no more articles to evaluate
					AreaEditorControlPanel back = new AreaEditorControlPanel(this.getSize(), this);
					back.show();
					this.dispose();
				} else if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error while reading the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error connecting to the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		}

	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent e) {
		try {
			AreaEditorEvaluateArticles back = new AreaEditorEvaluateArticles(this.getSize(), this);
			back.show();
		} catch (DAOExcepcion de) { // database error or no more articles to evaluate
			AreaEditorControlPanel back = new AreaEditorControlPanel(this.getSize(), this);
			back.show();
		}
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

}
