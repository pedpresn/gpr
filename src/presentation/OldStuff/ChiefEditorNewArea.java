package presentation.OldStuff;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.border.LineBorder;

import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;

import businessLogic.OldStuff.Controller;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class ChiefEditorNewArea extends JDialog implements ActionListener, KeyListener {

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private JPanel jPanel1;
	private JLabel magazineLabel;
	private JComboBox magazineComboBox;
	private JComboBox areaEditorComboBox;
	private JButton cancelButton;
	private JButton createButton;
	private JTextField areaNameField;
	private JLabel areaEditorLabel;
	private JLabel areaNameLabel;

	private ComboBoxModel leComboBoxUserModel;
	private ComboBoxModel leComboBoxMagModel;

	private String[][] listOfUsers;

	public ChiefEditorNewArea(Dimension dim, JFrame frame) throws DAOExcepcion {
		fillComboBoxUsers();
		fillComboBoxMagazines();
		initGUI(dim, frame);
	}

	protected void fillComboBoxUsers() throws DAOExcepcion {
		try {
			Controller control = Controller.getSingletonController();
			// (login, firstName, lastName)
			listOfUsers = control.listAllRegisteredUsersNotEditors();
			String[] listComboBox = new String[listOfUsers.length];
			for (int i = 0; i < listOfUsers.length; ++i) {
				listComboBox[i] = listOfUsers[i][0] + " - " + listOfUsers[i][1] + " " + listOfUsers[i][2];
			}
			leComboBoxUserModel = new DefaultComboBoxModel(listComboBox);
		} catch (DomainExcepcion e) {
			e.printStackTrace();
		}
	}

	protected void fillComboBoxMagazines() throws DAOExcepcion {
		try {
			Controller control = Controller.getSingletonController();
			// (login, firstName, lastName)
			String[] listOfMagazines = control.listAllMagazines();
			leComboBoxMagModel = new DefaultComboBoxModel(listOfMagazines);
		} catch (DomainExcepcion e) {
			e.printStackTrace();
		}
	}

	private void initGUI(Dimension dim, JFrame frame) {
		{
			this.setTitle(Controller.APPLICATION_NAME + " - Chief Editor - Create New Area");
			jPanel1 = new JPanel();
			getContentPane().add(jPanel1, BorderLayout.CENTER);
			GroupLayout jPanel1Layout = new GroupLayout((JComponent) jPanel1);
			jPanel1.setLayout(jPanel1Layout);
			jPanel1.setPreferredSize(new java.awt.Dimension(365, 328));
			{
				areaNameLabel = new JLabel();
				areaNameLabel.setText("Area Name");
			}
			{
				areaEditorLabel = new JLabel();
				areaEditorLabel.setText("Area Editor");
			}
			{
				areaEditorComboBox = new JComboBox();
				areaEditorComboBox.setToolTipText("Select the user that will be the area editor");
				areaEditorComboBox.setModel(leComboBoxUserModel);
				areaEditorComboBox.setEditable(false);
				areaEditorComboBox.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, false));
			}
			{
				magazineComboBox = new JComboBox();
				magazineComboBox.setToolTipText("Select the magazine to which the area will belong");
				magazineComboBox.setModel(leComboBoxMagModel);
				magazineComboBox.setEditable(false);
				magazineComboBox.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, false));
			}
			{
				magazineLabel = new JLabel();
				magazineLabel.setText("Magazine");
			}
			{
				createButton = new JButton();
				createButton.setText("Create");
				createButton.setToolTipText("Create the area");
				createButton.addActionListener(this);
				this.getRootPane().setDefaultButton(createButton);
			}
			{
				cancelButton = new JButton();
				cancelButton.setText("Cancel");
				cancelButton.setToolTipText("Return to the control panel");
				cancelButton.addActionListener(this);
			}
			{
				areaNameField = new JTextField();
				areaNameField.setToolTipText("Select the name of the area you are creating");
				areaNameField.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, false));
			}
			jPanel1Layout.setHorizontalGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(
					jPanel1Layout.createParallelGroup().addGroup(GroupLayout.Alignment.LEADING,
							jPanel1Layout.createSequentialGroup().addComponent(magazineLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addGap(10))
							.addComponent(areaNameLabel, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(
									areaEditorLabel, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)).addGap(47).addGroup(
					jPanel1Layout.createParallelGroup().addGroup(
							jPanel1Layout.createSequentialGroup().addComponent(magazineComboBox, GroupLayout.PREFERRED_SIZE, 213, GroupLayout.PREFERRED_SIZE).addGap(0, 0, Short.MAX_VALUE)).addGroup(
							jPanel1Layout.createSequentialGroup().addComponent(areaEditorComboBox, GroupLayout.PREFERRED_SIZE, 213, GroupLayout.PREFERRED_SIZE).addGap(0, 0, Short.MAX_VALUE))
							.addComponent(areaNameField, GroupLayout.Alignment.LEADING, 0, 213, Short.MAX_VALUE).addGroup(
									GroupLayout.Alignment.LEADING,
									jPanel1Layout.createSequentialGroup().addGap(0, 58, Short.MAX_VALUE).addComponent(createButton, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
											GroupLayout.PREFERRED_SIZE).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(cancelButton, GroupLayout.PREFERRED_SIZE,
											GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))).addContainerGap(28, 28));
			jPanel1Layout.setVerticalGroup(jPanel1Layout.createSequentialGroup().addContainerGap()
					.addGroup(
							jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(areaNameField, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 21,
									GroupLayout.PREFERRED_SIZE).addComponent(areaNameLabel, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
									GroupLayout.PREFERRED_SIZE)).addGap(27).addGroup(
							jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(areaEditorComboBox, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE,
									GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(areaEditorLabel, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE,
									GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)).addGap(30).addGroup(
							jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(magazineComboBox, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 25,
									GroupLayout.PREFERRED_SIZE).addComponent(magazineLabel, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
									GroupLayout.PREFERRED_SIZE)).addGap(0, 64, Short.MAX_VALUE).addGroup(
							jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(createButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE,
									GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(cancelButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE,
									GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)).addContainerGap(19, 19));
		}
		{
			this.setSize(365, 234);
			this.setModal(true);
			this.setLocationRelativeTo(frame);
			this.setResizable(false);
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		if (arg0.getSource().equals(createButton)) {
			try {
				if (areaNameField.getText().trim().isEmpty()) {
					JOptionPane.showMessageDialog(this, "You have to specify an area name.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					Controller control = Controller.getSingletonController();
					control.createArea(this.areaNameField.getText().trim(), listOfUsers[this.areaEditorComboBox.getSelectedIndex()][0], (String) this.magazineComboBox.getSelectedItem());
					this.dispose();
				}
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("AREA_EXISTE")) {
					JOptionPane.showMessageDialog(this, "That area already exists.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error while reading the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error connecting to the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			} catch (DomainExcepcion e) {
				e.printStackTrace();
			}
		}

		if (arg0.getSource().equals(cancelButton))
			this.dispose();

	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

}
