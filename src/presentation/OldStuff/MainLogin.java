package presentation.OldStuff;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;

import businessLogic.OldStuff.Controller;
import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class MainLogin extends javax.swing.JFrame implements ActionListener, KeyListener, WindowListener {
	private JButton loginButton;
	private JButton cancelButton;
	private JButton registerButton;
	private JPasswordField passwordField;
	private JPanel jPanel1;
	private JLabel passwordLabel;
	private JLabel usernameLabel;
	private JTextField usernameField;
	private Controller control;

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Auto-generated main method to display this JFrame
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				MainLogin inst = new MainLogin();
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
			}
		});
	}

	public MainLogin() {
		super();
		try {
			control = Controller.getSingletonController();
		} catch (DomainExcepcion de) {
			de.printStackTrace();
		}
		initGUI();
	}

	public MainLogin(Dimension dim, JFrame frame) {
		super();
		try {
			control = Controller.getSingletonController();
		} catch (DomainExcepcion de) {
			de.printStackTrace();
		}
		initGUI();
		this.setLocationRelativeTo(frame);
	}

	private void initGUI() {
		this.setTitle(Controller.APPLICATION_NAME + " - Log In");
		this.setSize(450, 150);
		this.setResizable(false);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		GroupLayout thisLayout = new GroupLayout((JComponent) getContentPane());
		getContentPane().setLayout(thisLayout);
		{
			jPanel1 = new JPanel();
			GroupLayout jPanel1Layout = new GroupLayout((JComponent) jPanel1);
			jPanel1.setLayout(jPanel1Layout);
			{
				usernameLabel = new JLabel();
				usernameLabel.setText("Username");
			}
			{
				usernameField = new JTextField();
				usernameField.setText("");
				usernameField.setToolTipText("Insert username");
				usernameField.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, false));
			}
			{
				passwordLabel = new JLabel();
				passwordLabel.setText("Password");
			}
			{
				passwordField = new JPasswordField();
				passwordField.setText("");
				passwordField.setToolTipText("Insert password");
				passwordField.setBorder(new LineBorder(new java.awt.Color(0, 0, 0), 1, false));
			}
			{
				registerButton = new JButton();
				registerButton.setText("Register");
				registerButton.setToolTipText("Create a new registered user.");
				registerButton.addActionListener(this);
				registerButton.addKeyListener(this);
			}
			{
				loginButton = new JButton();
				loginButton.setText("Log In");
				loginButton.setToolTipText("Log into the application");
				loginButton.addActionListener(this);
				loginButton.addKeyListener(this);
				this.getRootPane().setDefaultButton(loginButton);
			}
			{
				cancelButton = new JButton();
				cancelButton.setText("Cancel");
				cancelButton.setToolTipText("Close the application");
				cancelButton.addActionListener(this);
				cancelButton.addKeyListener(this);
			}
			jPanel1Layout.setHorizontalGroup(jPanel1Layout.createSequentialGroup().addContainerGap(41, 41).addGroup(
					jPanel1Layout.createParallelGroup().addGroup(
							GroupLayout.Alignment.LEADING,
							jPanel1Layout.createSequentialGroup().addComponent(registerButton, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE).addGap(81).addComponent(loginButton,
									GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(cancelButton,
									GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE).addGap(0, 0, Short.MAX_VALUE)).addGroup(
							jPanel1Layout.createSequentialGroup().addGroup(
									jPanel1Layout.createParallelGroup().addComponent(passwordLabel, GroupLayout.Alignment.LEADING, 0, 79, Short.MAX_VALUE).addComponent(usernameLabel,
											GroupLayout.Alignment.LEADING, 0, 79, Short.MAX_VALUE)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(
									jPanel1Layout.createParallelGroup().addComponent(passwordField, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 289, GroupLayout.PREFERRED_SIZE)
											.addComponent(usernameField, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 289, GroupLayout.PREFERRED_SIZE)))).addContainerGap(29, 29));
			jPanel1Layout.linkSize(SwingConstants.HORIZONTAL, new Component[] { passwordField, usernameField });
			jPanel1Layout.linkSize(SwingConstants.HORIZONTAL, new Component[] { cancelButton, loginButton, registerButton });
			jPanel1Layout.setVerticalGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(
					jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(usernameField, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 21,
							GroupLayout.PREFERRED_SIZE).addComponent(usernameLabel, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)).addPreferredGap(
					LayoutStyle.ComponentPlacement.RELATED).addGroup(
					jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(passwordField, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 21,
							GroupLayout.PREFERRED_SIZE).addComponent(passwordLabel, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)).addGap(23).addGroup(
					jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(registerButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE,
							GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(loginButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
							GroupLayout.PREFERRED_SIZE).addComponent(cancelButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(20, 20));
			jPanel1Layout.linkSize(SwingConstants.VERTICAL, new Component[] { passwordField, usernameField });
		}
		thisLayout.setVerticalGroup(thisLayout.createSequentialGroup().addComponent(jPanel1, 0, 128, Short.MAX_VALUE));
		thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup().addComponent(jPanel1, 0, 450, Short.MAX_VALUE));
		pack();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		if (arg0.getSource().equals(loginButton)) {
			try {
				if (usernameField.getText().trim().isEmpty()) {
					JOptionPane.showMessageDialog(this, "You have to specify a username.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (passwordField.getText().trim().isEmpty()) {
					JOptionPane.showMessageDialog(this, "You have to specify a password.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					int userType = control.checkLoginAndPassword(usernameField.getText().trim(), passwordField.getText().trim());
					switch (userType) {
					case Controller.NORMAL_USER:
						NormalUserControlPanel logged = new NormalUserControlPanel(this.getSize(), this);
						logged.show();
						this.dispose();
						break;
					case Controller.AREA_EDITOR:
						AreaEditorControlPanel areaLogged = new AreaEditorControlPanel(this.getSize(), this);
						areaLogged.show();
						this.dispose();
						break;
					case Controller.CHIEF_EDITOR:
						ChiefEditorControlPanel chiefLogged = new ChiefEditorControlPanel(this.getSize(), this);
						chiefLogged.show();
						this.dispose();
						break;
					}
				}
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("PASSWORD_INCORRECTA")) {
					JOptionPane.showMessageDialog(this, "The introduced password is not correct.", "ERROR", JOptionPane.ERROR_MESSAGE);

				} else if (de.getMessage().equals("REG_USER_NO_EXISTE")) {
					JOptionPane.showMessageDialog(this, "The introduced user doesn't exist.\nClick Register button to register.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error while reading the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error connecting to the database.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		}

		else if (arg0.getSource().equals(cancelButton))
			System.exit(0);

		else if (arg0.getSource().equals(registerButton)) {
			MainRegister mainRegister = new MainRegister(this);
			mainRegister.setModal(true);
			mainRegister.setVisible(true);
		}

	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		System.exit(0);
	}

	@Override
	public void windowClosing(WindowEvent e) {
		System.exit(0);
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

}
