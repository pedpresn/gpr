package presentation;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.jdesktop.swingx.plaf.DatePickerAddon;

import presentation.OldStuff.ColumnsAutoSizer;

import businessLogic.Controlador;
import businessLogic.OldStuff.Controller;

import com.cloudgarden.layout.AnchorLayout;

import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class JefeConsultaIncidencia extends javax.swing.JFrame implements ActionListener, KeyListener, WindowListener {
	private JButton jButtonVolver;
	private JTable jTableIncidencias;
	private JScrollPane jScrollPaneIncidencias;
	private Controlador control;
	private String[] columnNames;
	private Object[][] listOfIncidencias;
	private int selectedAvisoId;
	private JButton jButtonVerAviso;

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public JefeConsultaIncidencia(Dimension dim, JFrame frame) throws DAOExcepcion {
		try {
			control = Controlador.getSingletonControlador();
		} catch (DAOExcepcion de) {
			de.printStackTrace();
		}
		fillTable();
		initGUI(dim, frame);
	}
	
	private void fillTable() throws DAOExcepcion {
		columnNames = new String[] { "C�digo", "Fecha de Petici�n", "Descripci�n", "Asociado a m�quina" };
		// Matrix row structure: codigo, fecha(dd-mm-yyyy), descripcion,
		// maquina (true,false)
		listOfIncidencias = control.getAllAvisosIncidencia();
	}
	
	private void volver() {
		JefeControlPanel panel = new JefeControlPanel(this.getSize(), this);
		panel.show();
		this.dispose();
	}

	private void initGUI(Dimension dim, JFrame frame) {
		try {
			this.setTitle(control.APPLICATION_NAME + "Jefe - Avisos de Incidencias");
			this.addWindowListener(this);
			GroupLayout thisLayout = new GroupLayout((JComponent)getContentPane());
			getContentPane().setLayout(thisLayout);
			this.setSize(557, 393);
			this.setLocationRelativeTo(frame);
			{
				jButtonVolver = new JButton();
				jButtonVolver.setText("< Volver");
				jButtonVolver.addActionListener(this);
				jButtonVolver.addKeyListener(this);
			}
			{
				jScrollPaneIncidencias = new JScrollPane();
				jScrollPaneIncidencias.setMinimumSize(new java.awt.Dimension(0, 0));
				{
					jTableIncidencias = new JTable(new MyTableModel(columnNames, listOfIncidencias));
					jScrollPaneIncidencias.setViewportView(jTableIncidencias);
					jTableIncidencias.setEnabled(true);
					jTableIncidencias.setFillsViewportHeight(true);
					jTableIncidencias.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); // only 1 row selectable
					ColumnsAutoSizer.sizeColumnsToFit(jTableIncidencias);
					jTableIncidencias.getSelectionModel().addListSelectionListener(
			                new ListSelectionListener() {
			                    public void valueChanged(ListSelectionEvent event) {
			                    	jButtonVerAviso.setEnabled(true);
			                        int viewRow = jTableIncidencias.getSelectedRow();
			                        if (viewRow < 0) {
			                            selectedAvisoId = -1;
			                        } else {
			                            int modelRow = jTableIncidencias.convertRowIndexToModel(viewRow);
			                            selectedAvisoId = Integer.parseInt((String) jTableIncidencias.getValueAt(modelRow, 0));
			                        }
			                    }
			                }
			        );
				}
			}
			{
				jButtonVerAviso = new JButton();
				jButtonVerAviso.setText("Ver Aviso");
				jButtonVerAviso.setEnabled(false);
				jButtonVerAviso.addActionListener(this);
				jButtonVerAviso.addKeyListener(this);
				this.getRootPane().setDefaultButton(jButtonVerAviso);
			}
			thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
				.addContainerGap()
				.addComponent(jScrollPaneIncidencias, 0, 318, Short.MAX_VALUE)
				.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
				.addGroup(thisLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				    .addComponent(jButtonVolver, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				    .addComponent(jButtonVerAviso, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
				.addContainerGap());
			thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
				.addContainerGap()
				.addGroup(thisLayout.createParallelGroup()
				    .addGroup(GroupLayout.Alignment.LEADING, thisLayout.createSequentialGroup()
				        .addComponent(jButtonVolver, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
				        .addGap(0, 349, Short.MAX_VALUE)
				        .addComponent(jButtonVerAviso, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE))
				    .addGroup(GroupLayout.Alignment.LEADING, thisLayout.createSequentialGroup()
				        .addPreferredGap(jButtonVolver, jScrollPaneIncidencias, LayoutStyle.ComponentPlacement.INDENT)
				        .addComponent(jScrollPaneIncidencias, GroupLayout.PREFERRED_SIZE, 539, GroupLayout.PREFERRED_SIZE)
				        .addGap(0, 0, Short.MAX_VALUE)))
				.addContainerGap());
			thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
				.addContainerGap(18, 18)
				.addComponent(jScrollPaneIncidencias, 0, 321, Short.MAX_VALUE)
				.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
				.addGroup(thisLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				    .addComponent(jButtonVerAviso, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				    .addComponent(jButtonVolver, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
				.addContainerGap());
			thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
				.addContainerGap()
				.addGroup(thisLayout.createParallelGroup()
				    .addComponent(jScrollPaneIncidencias, GroupLayout.Alignment.LEADING, 0, 533, Short.MAX_VALUE)
				    .addGroup(GroupLayout.Alignment.LEADING, thisLayout.createSequentialGroup()
				        .addPreferredGap(jScrollPaneIncidencias, jButtonVolver, LayoutStyle.ComponentPlacement.INDENT)
				        .addComponent(jButtonVolver, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
				        .addGap(0, 311, Short.MAX_VALUE)
				        .addComponent(jButtonVerAviso, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
				        .addGap(16)))
				.addContainerGap());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource().equals(jButtonVolver)) {
			volver();
		}
		else if (arg0.getSource().equals(jButtonVerAviso)) {
			try {
				JefeCrearParteIncidencia aviso = new JefeCrearParteIncidencia(this.getSize(), this, selectedAvisoId);
				aviso.show();
				this.dispose();
			} catch (DAOExcepcion de) {
				if (de.getMessage().equals("DB_READ_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error leyendo la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
					JOptionPane.showMessageDialog(this, "Error conectando a la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		volver();
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
