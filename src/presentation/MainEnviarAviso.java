package presentation;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.SoftBevelBorder;

import businessLogic.Controlador;
import businessLogic.OldStuff.Controller;
import excepciones.DAOExcepcion;
import excepciones.DomainExcepcion;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class MainEnviarAviso extends JDialog implements ActionListener, KeyListener {

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private JPanel jPanel1;
	private JButton cancelButton;
	private JButton createButton;
	private JLabel descripcionLabel;
	private JComboBox maquinaComboBox;
	private JCheckBox maquinaCheckBox;
	private JScrollPane descripcionScrollPane;
	private JTextArea descripcionTextArea;
	private Controlador control;
	private Object maquinas[][];

	public MainEnviarAviso(JFrame frame) throws DAOExcepcion {
		try {
			control = Controlador.getSingletonControlador();
		} catch (DAOExcepcion de) {
			de.printStackTrace();
		}
		maquinas = control.getAllMaquinasWithAreas();
		initGUI(frame);
	}

	private void initGUI(JFrame frame) {
		{
			this.setTitle(Controller.APPLICATION_NAME + " - Enviar Aviso");
			this.setSize(399, 319);
			this.setResizable(false);
			this.setModal(true);
			this.setLocationRelativeTo(frame);
			{
				jPanel1 = new JPanel();
				getContentPane().add(jPanel1, BorderLayout.CENTER);
				GroupLayout jPanel1Layout = new GroupLayout((JComponent)jPanel1);
				jPanel1.setLayout(jPanel1Layout);
				jPanel1.setPreferredSize(new java.awt.Dimension(675, 391));
				{
					createButton = new JButton();
					createButton.setText("Enviar Aviso");
					createButton.setToolTipText("Register");
					createButton.addActionListener(this);
					createButton.addKeyListener(this);
					this.getRootPane().setDefaultButton(createButton);
				}
				{
					cancelButton = new JButton();
					cancelButton.setText("Cancel");
					cancelButton.setToolTipText("Return to the login window");
					cancelButton.addActionListener(this);
					cancelButton.addKeyListener(this);
				}
				{
					descripcionLabel = new JLabel();
					descripcionLabel.setText("Descripci�n de la incidencia:");
				}
				{
					descripcionScrollPane = new JScrollPane();
					{
						descripcionTextArea = new JTextArea();
						descripcionScrollPane.setViewportView(descripcionTextArea);
					}
				}
				{
					maquinaCheckBox = new JCheckBox();
					maquinaCheckBox.setText("Incidencia relacionada con una m�quina");
					maquinaCheckBox.addActionListener(this);
				}
				{
					String[] contents = new String[maquinas.length];
					for(int i=0; i<maquinas.length; ++i) {
						contents[i] = maquinas[i][2]+" - "+maquinas[i][1]+" ("+maquinas[i][3]+")";
					}
					ComboBoxModel maquinaComboBoxModel = new DefaultComboBoxModel(contents);
					maquinaComboBox = new JComboBox();
					maquinaComboBox.setModel(maquinaComboBoxModel);
					maquinaComboBox.setEnabled(false);
				}
				jPanel1Layout.setHorizontalGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap(12, 12)
					.addGroup(jPanel1Layout.createParallelGroup()
					    .addGroup(GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
					        .addComponent(descripcionScrollPane, 0, 375, Short.MAX_VALUE)
					        .addGap(6))
					    .addGroup(GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
					        .addComponent(maquinaCheckBox, GroupLayout.PREFERRED_SIZE, 318, GroupLayout.PREFERRED_SIZE)
					        .addGap(0, 63, Short.MAX_VALUE))
					    .addGroup(GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
					        .addGroup(jPanel1Layout.createParallelGroup()
					            .addGroup(GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
					                .addComponent(maquinaComboBox, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE)
					                .addGap(0, 8, Short.MAX_VALUE)
					                .addComponent(cancelButton, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
					            .addGroup(GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
					                .addComponent(descripcionLabel, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE)
					                .addGap(59)))
					        .addComponent(createButton, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap());
				jPanel1Layout.setVerticalGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap(18, 18)
					.addComponent(descripcionLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(descripcionScrollPane, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
					.addGap(20)
					.addComponent(maquinaCheckBox, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(maquinaComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(40)
					.addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
					    .addComponent(cancelButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					    .addComponent(createButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap());
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource().equals(maquinaCheckBox)) {
			maquinaComboBox.setEnabled(maquinaCheckBox.isSelected());
		}
		else if (arg0.getSource().equals(createButton)) {
			String descr = descripcionTextArea.getText().trim().replace("'", "''");
			if(descr.isEmpty()) {
				JOptionPane.showMessageDialog(this, "Debes especificar una descripcion.", "ERROR", JOptionPane.ERROR_MESSAGE);
			} else {
				try {
					if(!maquinaCheckBox.isSelected()) {
						control.crearAvisoIncidencia(descr, -1);
					} else {
						control.crearAvisoIncidencia(descr, (Integer) maquinas[maquinaComboBox.getSelectedIndex()][0]);
					}
					this.dispose();
				} catch(DAOExcepcion de) {
					if (de.getMessage().equals("DB_READ_ERROR")) {
						JOptionPane.showMessageDialog(this, "Error leyendo la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
					} else if (de.getMessage().equals("DB_CONNECT_ERROR")) {
						JOptionPane.showMessageDialog(this, "Error conectando a la base de datos.", "ERROR", JOptionPane.ERROR_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(this, de.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		}
		if (arg0.getSource().equals(cancelButton))
			this.dispose();

	}

	private boolean checkCorrectNif(String nif) {
		int dni;
		String theLetters = "TRWAGMYFPDXBNJZSQVHLCKE";
		if (nif.length() != 9)
			return false;
		try {
			dni = Integer.parseInt(nif.substring(0, 8));
		} catch (NumberFormatException nfe) {
			return false;
		}
		if (nif.toUpperCase().charAt(8) != theLetters.charAt(dni % 23))
			return false;
		return true;
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

}
