package presentation;
import java.awt.Component;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.jdesktop.swingx.plaf.DatePickerAddon;

import com.cloudgarden.layout.AnchorLayout;
/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class OperarioListaMaterial extends javax.swing.JFrame {
	private JButton jButtonCancelar;
	private JButton jButtonRealizarPeticion;
	private JTable jTableIncidencias;
	private JScrollPane jScrollPaneIncidencias;

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	* Auto-generated main method to display this JDialog
	*/
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JFrame frame = new JFrame();
				OperarioListaMaterial inst = new OperarioListaMaterial(frame);
				inst.setVisible(true);
			}
		});
	}
	
	public OperarioListaMaterial(JFrame frame) {
		super();
		initGUI();
	}
	
	private void initGUI() {
		try {
			GroupLayout thisLayout1 = new GroupLayout((JComponent)getContentPane());
			getContentPane().setLayout(thisLayout1);
			this.setTitle("Parte Peticion Material");
			{
				jButtonCancelar = new JButton();
				AnchorLayout jButtonVolverLayout = new AnchorLayout();
				jButtonCancelar.setLayout(null);
				jButtonCancelar.setText("Cancelar");
			}
			{
				jButtonRealizarPeticion = new JButton();
				AnchorLayout jButtonVerAvisoLayout = new AnchorLayout();
				jButtonRealizarPeticion.setText("Realizar Pedido");
				jButtonRealizarPeticion.setLayout(null);
			}
			{
				jScrollPaneIncidencias = new JScrollPane();
				jScrollPaneIncidencias.getVerticalScrollBar().setOpaque(false);
				jScrollPaneIncidencias.getVerticalScrollBar().setAutoscrolls(true);
				{
					TableModel jTableIncidenciasModel = 
						new DefaultTableModel(
								new String[][] { { "One", "Two" }, { "Three", "Four" } },
								new String[] { "Column 1", "Column 2" });
					jTableIncidencias = new JTable();
					jScrollPaneIncidencias.setViewportView(jTableIncidencias);
					jTableIncidencias.setPreferredSize(new java.awt.Dimension(522, 241));
					jTableIncidencias.setIntercellSpacing(new java.awt.Dimension(1, 1));
					jTableIncidencias.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
					jTableIncidencias.setOpaque(false);
					jTableIncidencias.setShowHorizontalLines(false);
					jTableIncidencias.setShowVerticalLines(false);
				}
			}
			thisLayout1.setVerticalGroup(thisLayout1.createSequentialGroup()
				.addContainerGap(18, 18)
				.addComponent(jScrollPaneIncidencias, GroupLayout.PREFERRED_SIZE, 277, GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
				.addGroup(thisLayout1.createParallelGroup(GroupLayout.Alignment.BASELINE)
				    .addComponent(jButtonRealizarPeticion, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
				    .addComponent(jButtonCancelar, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
				.addContainerGap());
			thisLayout1.setHorizontalGroup(thisLayout1.createSequentialGroup()
				.addContainerGap()
				.addGroup(thisLayout1.createParallelGroup()
				    .addComponent(jScrollPaneIncidencias, GroupLayout.Alignment.LEADING, 0, 526, Short.MAX_VALUE)
				    .addGroup(GroupLayout.Alignment.LEADING, thisLayout1.createSequentialGroup()
				        .addPreferredGap(jScrollPaneIncidencias, jButtonCancelar, LayoutStyle.ComponentPlacement.INDENT)
				        .addComponent(jButtonCancelar, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
				        .addGap(0, 295, Short.MAX_VALUE)
				        .addComponent(jButtonRealizarPeticion, GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE)))
				.addContainerGap());
			{
				this.setSize(538, 364);
			}
			GroupLayout thisLayout = new GroupLayout((JComponent)getContentPane());
			thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
				.addContainerGap(28, 28)
				.addComponent(jScrollPaneIncidencias, 0, 246, Short.MAX_VALUE)
				.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
				.addGroup(thisLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				    .addComponent(jButtonCancelar, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				    .addComponent(jButtonRealizarPeticion, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
				.addContainerGap());
			thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
				.addContainerGap()
				.addGroup(thisLayout.createParallelGroup()
				    .addGroup(GroupLayout.Alignment.LEADING, thisLayout.createSequentialGroup()
				        .addComponent(jButtonCancelar, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
				        .addGap(333)
				        .addComponent(jButtonRealizarPeticion, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
				        .addGap(0, 0, Short.MAX_VALUE))
				    .addGroup(GroupLayout.Alignment.LEADING, thisLayout.createSequentialGroup()
				        .addGap(11)
				        .addComponent(jScrollPaneIncidencias, GroupLayout.PREFERRED_SIZE, 509, GroupLayout.PREFERRED_SIZE)
				        .addGap(0, 11, Short.MAX_VALUE)))
				.addContainerGap());
			thisLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {jButtonCancelar, jButtonRealizarPeticion});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
