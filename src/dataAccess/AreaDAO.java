package dataAccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import businessLogic.Area;
import businessLogic.Empleado;
import businessLogic.Operario;
import excepciones.DAOExcepcion;

public class AreaDAO {
	protected ConnectionManager connManager;
	private static AreaDAO aDAO;
	
	private AreaDAO() throws DAOExcepcion {
		super();
		try {
			connManager = new ConnectionManager("GPR");
		} catch (ClassNotFoundException e) {
			throw new DAOExcepcion("DB_CONNECT_ERROR");
		}
	}
	
	public static AreaDAO getSingleton() throws DAOExcepcion {
		if (aDAO == null)
			aDAO = new AreaDAO();
		return aDAO;
	}
	
	public Area getAreaByCodArea(int cod) throws DAOExcepcion {
		try {
			Area a = null;
			connManager.connect();
			ResultSet rs =
					connManager.queryDB("select distinct * from AREA where codArea=" + cod);
			connManager.close();
			try{
				while(rs.next()){
					a = new Area(rs.getInt("codArea"),rs.getString("nombre"));
				}
				if (a == null)
					throw new DAOExcepcion("NO_AREA_FOUND");
				else
					return a;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
	
	public List<Area> getAllAreas() throws DAOExcepcion {
		try {
			List<Area> areas = new ArrayList<Area>();
			connManager.connect();
			ResultSet rs =
					connManager.queryDB("SELECT * FROM area");
			connManager.close();
			try {
				while (rs.next()) {
					areas.add(new Area(rs.getInt("codArea"), rs
							.getString("nombre")));
				}
				return areas;
			} catch (SQLException e) {
				e.printStackTrace();
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
}