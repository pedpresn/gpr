package dataAccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import businessLogic.AvisoIncidencia;
import excepciones.DAOExcepcion;

public class AvisoIncidenciaDAO {
	protected ConnectionManager connManager;
	private static AvisoIncidenciaDAO aiDAO;

	private AvisoIncidenciaDAO() throws DAOExcepcion {
		super();
		try {
			connManager = new ConnectionManager("GPR");
		} catch (ClassNotFoundException e) {
			throw new DAOExcepcion("DB_CONNECT_ERROR");
		}
	}

	public static AvisoIncidenciaDAO getSingleton() throws DAOExcepcion {
		if (aiDAO == null)
			aiDAO = new AvisoIncidenciaDAO();
		return aiDAO;
	}

	public void crearAvisoIncidencia(AvisoIncidencia ai) throws DAOExcepcion {
		try {
			connManager.connect();
			connManager
					.updateDB("insert into AVISO_INCIDENCIA (descripcion, fechaPeticion, maquina) values ('"
							+ ai.getDescripcion()
							+ "', '"
							+ ai.getFechaPeticion()
							+ "', "
							+ ((ai.getMaquina() < 0)? "NULL" : "'"+ai.getMaquina()+"'") + ")");
			connManager.close();
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
	
	public void borrarAvisoIncidencia(AvisoIncidencia ai) throws DAOExcepcion {
		try {
			connManager.connect();
			connManager
					.updateDB("delete from AVISO_INCIDENCIA where codigo="+ ai.getCodigo());
			connManager.close();
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
	
	public AvisoIncidencia getAvisoIncidenciaByCod(int codigo) throws DAOExcepcion {
		try {
			int maquinaAux;
			AvisoIncidencia ai;
			connManager.connect();
			ResultSet rs = connManager.queryDB("select distinct * from AVISO_INCIDENCIA A where codigo = '"+ codigo +
					"' AND not exists (select * from PARTE_INCIDENCIA P where A.codigo = P.codigo)");
			connManager.close();
			try{
				if (!rs.next()) throw new DAOExcepcion("NO_AVISO_INCIDENCIA_FOUND");
				else{
					maquinaAux = rs.getInt("maquina");
					if(rs.wasNull()) maquinaAux = -1;
					ai = new AvisoIncidencia(rs.getInt("codigo"),rs.getString("descripcion"),rs.getTimestamp("fechaPeticion"),
							maquinaAux);
				}
				if (ai == null) throw new DAOExcepcion("NO_AVISO_INCIDENCIA_FOUND");
				else
					return ai;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
	
	public List<AvisoIncidencia> getAllAvisosIncidencia() throws DAOExcepcion {
		try {
			int maquinaAux;
			List<AvisoIncidencia> incidencias = new ArrayList<AvisoIncidencia>();
			connManager.connect();
			ResultSet rs =
					connManager.queryDB("select distinct * from AVISO_INCIDENCIA A " +
										"where not exists (select * from PARTE_INCIDENCIA P where A.codigo = P.codigo)");
			connManager.close();
			try{
				while(rs.next()){
					maquinaAux = rs.getInt("maquina");
					if(rs.wasNull()) maquinaAux = -1;
					AvisoIncidencia ai = new AvisoIncidencia(rs.getInt("codigo"),rs.getString("descripcion"),rs.getTimestamp("fechaPeticion"),
							maquinaAux);
					incidencias.add(ai);
				}
				if (incidencias.size() == 0)
					throw new DAOExcepcion("NO_INCIDENCIAS_FOUND");
				else
					return incidencias;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
	
	public void eliminarAvisoIncidenciaById(int codigo) throws DAOExcepcion {
		try {
			connManager.connect();
			connManager.updateDB("DELETE FROM aviso_incidencia WHERE codigo = '"+ codigo +"'");
			connManager.close();
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
}