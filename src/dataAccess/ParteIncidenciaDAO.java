package dataAccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import businessLogic.AvisoIncidencia;
import businessLogic.Maquina;
import businessLogic.ParteIncidencia;
import excepciones.DAOExcepcion;

public class ParteIncidenciaDAO {
	protected ConnectionManager connManager;
	private static ParteIncidenciaDAO piDAO;

	private ParteIncidenciaDAO() throws DAOExcepcion {
		super();
		try {
			connManager = new ConnectionManager("GPR");
		} catch (ClassNotFoundException e) {
			throw new DAOExcepcion("DB_CONNECT_ERROR");
		}
	}

	public static ParteIncidenciaDAO getSingleton() throws DAOExcepcion {
		if (piDAO == null)
			piDAO = new ParteIncidenciaDAO();
		return piDAO;
	}

	public void crearParteIncidencia(ParteIncidencia pi) throws DAOExcepcion {
		try {
			connManager.connect();
			connManager
					.updateDB("insert into PARTE_INCIDENCIA (codigo, prioridad, estado, costeDiario, area) values ('"
							+ pi.getCodigo()
							+ "', '"
							+ pi.getPrioridad()
							+ "', '"
							+ pi.getEstado()
							+ "', '"
							+ pi.getCosteDiario()
							+ "', '"
							+ pi.getCodArea()
							+ "')");
			connManager.close();
		} catch (DAOExcepcion e) {
			throw e;
		}
	}

	public Object[] getAllIncidenciasAndAvisosByCodArea(int area) throws DAOExcepcion {
		try {
			int maquinaAux;
			Object[] result = new Object[2];
			List<ParteIncidencia> partes = new ArrayList<ParteIncidencia>();
			List<AvisoIncidencia> avisos = new ArrayList<AvisoIncidencia>();
			connManager.connect();
			ResultSet rs =
					connManager
							.queryDB("select distinct P.codigo, P.prioridad, P.estado, P.costeDiario, P.area, A.descripcion," +
									" A.fechaPeticion, A.maquina from PARTE_INCIDENCIA P, AVISO_INCIDENCIA A where not exists(select * from ORDEN_TRABAJO O where O.codigo = P.codigo) and A.codigo = P.codigo and P.estado=0 AND P.area = " + area);
			connManager.close();
			try {
				while (rs.next()) {
					maquinaAux = rs.getInt("maquina");
					if(rs.wasNull()) maquinaAux = -1;
					partes.add(new ParteIncidencia(rs.getInt("codigo"), rs
							.getInt("prioridad"), rs.getInt("estado"), rs
							.getDouble("costeDiario"), rs.getInt("area")));
					avisos.add(new AvisoIncidencia(rs.getInt("codigo"), rs
							.getString("descripcion"), rs
							.getTimestamp("fechaPeticion"), maquinaAux));
				}
				if(partes.isEmpty() || avisos.isEmpty()){
					throw new DAOExcepcion("NO_INCIDENCIAS_FOUND");
				}
				else{
					result[0] = partes;
					result[1] = avisos;
					return result;
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}	
	
	public void eliminarParteIncidenciaById(int codigo) throws DAOExcepcion {
		try {
			connManager.connect();
			connManager.updateDB("DELETE FROM parte_incidencia WHERE codigo = '"+ codigo +"'");
			connManager.updateDB("DELETE FROM aviso_incidencia WHERE codigo = '"+ codigo +"'");
			connManager.close();
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
}