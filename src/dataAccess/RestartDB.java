package dataAccess;

import java.sql.SQLException;
import java.sql.Timestamp;

import excepciones.DAOExcepcion;

public class RestartDB {
	protected ConnectionManager connManager;
	
	public RestartDB() throws DAOExcepcion {
		super();
		try {
			connManager = new ConnectionManager("GPR");
		} catch (ClassNotFoundException e) {
			throw new DAOExcepcion("DB_CONNECT_ERROR");
		}
	}
	
	public void cleanDB() throws DAOExcepcion {
		try {
			connManager.connect();
			connManager.updateDB("DELETE FROM tiene");
			connManager.updateDB("DELETE FROM contrato_mantenimiento");
			connManager.updateDB("DELETE FROM maestro");
			connManager.updateDB("DELETE FROM orden_trabajo");
			connManager.updateDB("DELETE FROM parte_incidencia");
			connManager.updateDB("DELETE FROM operario");
			connManager.updateDB("DELETE FROM peticion_material");
			connManager.updateDB("DELETE FROM material");
			connManager.updateDB("DELETE FROM jefe");
			connManager.updateDB("DELETE FROM empleado");
			connManager.updateDB("DELETE FROM aviso_incidencia");
			connManager.updateDB("DELETE FROM maquina");
			connManager.updateDB("DELETE FROM area");
			connManager.close();
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
	
	public void initDB() throws DAOExcepcion {
		try {
			connManager.connect();
			//Insertando area
			connManager.updateDB("INSERT INTO area (nombre) VALUES ('cardiologia')");
			connManager.updateDB("INSERT INTO area (nombre) VALUES ('yedai')");
			//Insertando empleados y cargo
			connManager.updateDB("INSERT INTO empleado (dni,apellidos,nombre,nombreUsuario,contrasena,fechaNacimiento,poblacion,provincia,cp,telefono)" +
					" VALUES ('11111111','Alemany Bordera','Jose','cagentot','casapepe','"+ new Timestamp(1992,0,7,0,0,0,0) +"','Alzira','Valencia','46600','645158287')");
			connManager.updateDB("INSERT INTO jefe (dni) VALUES ('11111111')");
			connManager.updateDB("INSERT INTO empleado (dni,apellidos,nombre,nombreUsuario,contrasena,fechaNacimiento,poblacion,provincia,cp,telefono)" +
					" VALUES ('22222222','Roig Martinez','Tomas','toms','casapepe','"+ new Timestamp(1991,5,14,0,0,0,0) +"','Alzira','Valencia','46600','645195285')");
			connManager.updateDB("INSERT INTO maestro (dni,area) VALUES ('22222222',(SELECT codArea FROM area WHERE nombre = 'cardiologia'))");
			connManager.updateDB("INSERT INTO empleado (dni,apellidos,nombre,nombreUsuario,contrasena,fechaNacimiento,poblacion,provincia,cp,telefono)" +
					" VALUES ('33333333','Marco Año','Alvaro','amarco','casapepe','"+ new Timestamp(1992,10,8,0,0,0,0) +"','Guadasuar','Valencia','46600','689858287')");
			connManager.updateDB("INSERT INTO operario (dni,area) VALUES ('33333333', (SELECT codArea FROM area WHERE nombre = 'cardiologia'))");
			connManager.updateDB("INSERT INTO empleado (dni,apellidos,nombre,nombreUsuario,contrasena,fechaNacimiento,poblacion,provincia,cp,telefono)" +
					" VALUES ('44444444','Perez Sanchez','Pedro','pedrolo','casapepe','"+ new Timestamp(1992,11,24,0,0,0,0) +"','Pobla','Valencia','46600','645458287')");
			connManager.updateDB("INSERT INTO maestro (dni,area) VALUES ('44444444', (SELECT codArea FROM area WHERE nombre = 'yedai'))");
			connManager.updateDB("INSERT INTO empleado (dni,apellidos,nombre,nombreUsuario,contrasena,fechaNacimiento,poblacion,provincia,cp,telefono)" +
					" VALUES ('55555555','Ruiz Cepeda','Jose Vicente','jovi','casapepe','"+ new Timestamp(1992,3,30,0,0,0,0) +"','Mulejos','Valencia','46600','645180087')");
			connManager.updateDB("INSERT INTO operario (dni,area) VALUES ('55555555', (SELECT codArea FROM area WHERE nombre = 'yedai'))");
			//Insertando maquinas
			connManager.updateDB("INSERT INTO maquina (marca,modelo,area,descripcion,fechaUltimaRev) VALUES ('fujitsu','007',(SELECT codArea FROM area WHERE nombre " +
					"= 'cardiologia'),'aire acondicionado','"+ new Timestamp(System.currentTimeMillis()) +"')");
			connManager.updateDB("INSERT INTO maquina (marca,modelo,area,descripcion,fechaUltimaRev) VALUES ('maxpower','v4beta',(SELECT codArea FROM area WHERE nombre " +
					"= 'cardiologia'),'maquina rayos X','"+ new Timestamp(System.currentTimeMillis()) +"')");
			//Insertando material
			connManager.updateDB("INSERT INTO material (proveedor,descripcion,precio,stock) VALUES ('carrefour','tuercas','200','500')");
			connManager.updateDB("INSERT INTO material (proveedor,descripcion,precio,stock) VALUES ('X','pilas para rayos X','2000000','2')");
			connManager.updateDB("INSERT INTO material (proveedor,descripcion,precio,stock) VALUES ('bricoDepot','Set de herramientas','500','30')");
			//Insertando aviso de incidencia
			connManager.updateDB("INSERT INTO aviso_incidencia (codigo,descripcion,fechaPeticion) VALUES (0,'Jovi me ha pegado','"+ new Timestamp(2013,5,2,0,0,0,0) +"')");
			connManager.updateDB("INSERT INTO aviso_incidencia (codigo,descripcion,fechaPeticion,maquina) VALUES (1,'Rota, pero que muy rota...','"+ new Timestamp(2013,4,8,0,0,0,0) +"'," +
					"(SELECT codMaquina FROM maquina WHERE marca = 'maxpower'))");
			connManager.updateDB("INSERT INTO aviso_incidencia (codigo,descripcion,fechaPeticion) VALUES (2,'Aqui lo llevas pedro','"+ new Timestamp(2012,5,2,0,0,0,0) +"')");
			//Insertando parte de incidencia (en curso = 0, pendiente = 1, terminada = 2)
			connManager.updateDB("INSERT INTO parte_incidencia (codigo,prioridad,estado,costeDiario,area) VALUES (0,'2','0','30.9',(SELECT codArea FROM area WHERE nombre = 'yedai'))");
			connManager.updateDB("INSERT INTO parte_incidencia (codigo,prioridad,estado,costeDiario,area) VALUES (1,'1','0','23.4',(SELECT codArea FROM area WHERE nombre = 'yedai'))");
			connManager.updateDB("INSERT INTO parte_incidencia (codigo,prioridad,estado,costeDiario,area) VALUES (2,'2','0','30.9',(SELECT codArea FROM area WHERE nombre = 'yedai'))");
			//Insertando orden de trabajo
			connManager.updateDB("INSERT INTO orden_trabajo (codigo,dni) VALUES (1,'55555555')");
			connManager.close();
			
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
}
