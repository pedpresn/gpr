package dataAccess.OldStuff;

import java.sql.ResultSet;
import java.sql.SQLException;

import businessLogic.OldStuff.Person;
import businessLogic.OldStuff.RegisteredUser;
import excepciones.DAOExcepcion;
import java.util.List;
import java.util.ArrayList;

public class RegisteredUserDAOImp extends PersonDAOImp implements IRegisteredUserDAO {

	public RegisteredUserDAOImp() throws DAOExcepcion {
		super();
	}

	public void createRegisteredUser(RegisteredUser p) throws DAOExcepcion {
		try {
			if (this.findPersonbyNif(p.getNif()) == null) {
				if (this.findRegisteredUserbyLogin(p.getLogin()) == null) {
					this.createPerson(p);
					connManager.connect();

					connManager.updateDB("insert into REGISTERED_USER (LOGIN, PASSWORD, EMAIL, AREAS, ALERTED, NIF) values ('" + p.getLogin() + "', '" + p.getPassword() + "', '" + p.getEmail()
							+ "', '" + p.getAreas() + "', " + p.isAlerted() + " , '" + p.getNif() + "')");

					connManager.close();
				} else
					throw new DAOExcepcion("USU_REG_EXISTE");
			} else
				throw new DAOExcepcion("PERSON_EXISTE");
		} catch (DAOExcepcion e) {
			throw e;
		}
	}

	public RegisteredUser findRegisteredUserbyLogin(String login) throws DAOExcepcion {
		try {

			connManager.connect();
			ResultSet rs = connManager.queryDB("select LOGIN, PASSWORD, EMAIL, AREAS, ALERTED, NIF from REGISTERED_USER where LOGIN= '" + login + "'");
			connManager.close();
			try {
				if (rs.next()) {
					Person p = this.findPersonbyNif(rs.getString("NIF"));
					RegisteredUser u = new RegisteredUser(p.getNif(), p.getFirstName(), p.getLastName(), rs.getString("LOGIN"), rs.getString("PASSWORD"), rs.getString("EMAIL"), rs.getString("AREAS"),
							rs.getBoolean("ALERTED"));
					return u;
				} else
					return null;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}

	public List<RegisteredUser> findAllRegisteredUsers() throws DAOExcepcion {
		try {
			String login;
			RegisteredUser u;
			connManager.connect();
			ResultSet rs = connManager.queryDB("select * from REGISTERED_USER");
			connManager.close();
			List<RegisteredUser> listRegisteredUsers = new ArrayList<RegisteredUser>();
			try {
				while (rs.next()) {
					Person p = this.findPersonbyNif(rs.getString("NIF"));
					login = rs.getString("LOGIN");
					u = new RegisteredUser(p.getNif(), p.getFirstName(), p.getLastName(), login, rs.getString("PASSWORD"), rs.getString("EMAIL"), rs.getString("AREAS"), rs.getBoolean("ALERTED"));
					listRegisteredUsers.add(u);
				}
				return listRegisteredUsers;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}

	public List<RegisteredUser> findAllRegisteredUsersNotEditors() throws DAOExcepcion {
		try {
			String login;
			RegisteredUser u;
			connManager.connect();
			ResultSet rs = connManager
					.queryDB("select distinct * from REGISTERED_USER where (select count(*) from AREA where AREA_EDITOR=LOGIN) = 0 and (select count(*) from MAGAZINE where CHIEF_EDITOR=LOGIN) = 0;");
			connManager.close();
			List<RegisteredUser> listRegisteredUsers = new ArrayList<RegisteredUser>();
			try {
				while (rs.next()) {
					Person p = this.findPersonbyNif(rs.getString("NIF"));
					login = rs.getString("LOGIN");
					u = new RegisteredUser(p.getNif(), p.getFirstName(), p.getLastName(), login, rs.getString("PASSWORD"), rs.getString("EMAIL"), rs.getString("AREAS"), rs.getBoolean("ALERTED"));
					listRegisteredUsers.add(u);
				}
				if (listRegisteredUsers.size() == 0)
					throw new DAOExcepcion("NO_USU_REG_NOT_EDITOR_FOUND");
				else
					return listRegisteredUsers;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
}
