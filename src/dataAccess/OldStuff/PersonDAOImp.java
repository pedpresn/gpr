package dataAccess.OldStuff;

import java.sql.*;

import businessLogic.OldStuff.Person;
import excepciones.DAOExcepcion;

public class PersonDAOImp implements IPersonDAO {
	protected ConnectionManager connManager;

	public PersonDAOImp() throws DAOExcepcion {
		super();
		try {
			connManager = new ConnectionManager("ISW");
		} catch (ClassNotFoundException e) {
			throw new DAOExcepcion("DB_CONNECT_ERROR");
		}
	}

	public void createPerson(Person p) throws DAOExcepcion {
		try {
			connManager.connect();
			connManager.updateDB("insert into PERSON (NIF, FIRST_NAME, LAST_NAME) values ('" + p.getNif() + "', '" + p.getFirstName() + "', '" + p.getLastName() + "')");
			connManager.close();
		} catch (DAOExcepcion e) {
			throw e;
		}
	}

	public Person findPersonbyNif(String nif) throws DAOExcepcion {
		try {
			connManager.connect();
			ResultSet rs = connManager.queryDB("select FIRST_NAME, LAST_NAME from PERSON where NIF= '" + nif + "'");
			connManager.close();
			try {
				if (rs.next())
					return new Person(nif, rs.getString("FIRST_NAME"), rs.getString("LAST_NAME"));
				else
					return null;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
}
