package dataAccess.OldStuff;

import java.sql.*;

import businessLogic.OldStuff.Article;
import businessLogic.OldStuff.Evaluation;
import excepciones.DAOExcepcion;

public class EvaluationDAOImp implements IEvaluationDAO {
	protected ConnectionManager connManager;
	private IArticleDAO artDAO;

	public EvaluationDAOImp() throws DAOExcepcion {
		super();
		try {
			connManager = new ConnectionManager("ISW");
			artDAO = new ArticleDAOImp();
		} catch (ClassNotFoundException e) {
			throw new DAOExcepcion("DB_CONNECT_ERROR");
		}
	}

	public void createEvaluation(Evaluation ev) throws DAOExcepcion {
		try {
			connManager.connect();
			connManager.updateDB("insert into EVALUATION (ARTICLE, DATE, RESULT, COMMENTS) values ('" + ev.getId() + "', '" + ev.getDate().toString() + "', '" + ev.getResult() + "', '" + ev.getComments() + "')");
			connManager.close();
		} catch (DAOExcepcion e) {
			throw e;
		}
	}

	public Evaluation findEvaluationByArticleId(String articleId) throws DAOExcepcion {
		try {
			connManager.connect();
			ResultSet rs = connManager.queryDB("select ARTICLE, DATE, RESULT, COMMENTS from EVALUATION where ARTICLE= '" + articleId + "'");
			connManager.close();
			try {
				if (rs.next()) {
					return new Evaluation(articleId, rs.getTimestamp("DATE"), rs.getString("RESULT"), rs.getString("COMMENTS"));
				} else
					return null;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
}
