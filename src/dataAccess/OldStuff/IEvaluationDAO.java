package dataAccess.OldStuff;

import excepciones.*;
import businessLogic.OldStuff.Evaluation;

public interface IEvaluationDAO {
	public Evaluation findEvaluationByArticleId(String ArticleId) throws DAOExcepcion;

	public void createEvaluation(Evaluation e) throws DAOExcepcion;
}
