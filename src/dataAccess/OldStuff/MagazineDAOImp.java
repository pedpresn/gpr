package dataAccess.OldStuff;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import businessLogic.OldStuff.Magazine;
import businessLogic.OldStuff.RegisteredUser;
import excepciones.DAOExcepcion;

public class MagazineDAOImp implements IMagazineDAO {
	protected ConnectionManager connManager;
	private IRegisteredUserDAO ruDAO;

	public MagazineDAOImp() throws DAOExcepcion {
		super();
		try {
			connManager = new ConnectionManager("ISW");
			ruDAO = new RegisteredUserDAOImp();
		} catch (ClassNotFoundException e) {
			throw new DAOExcepcion("DB_CONNECT_ERROR");
		}
	}

	public void createMagazine(Magazine m) throws DAOExcepcion {
		try {
			if (m.getChiefEditor() != null) {
				connManager.connect();
				connManager.updateDB("insert into MAGAZINE (NAME, CHIEF_EDITOR) values ('" + m.getName() + "', '" + m.getChiefEditor().getLogin() + "')");
				connManager.close();
			} else {
				connManager.connect();
				connManager.updateDB("insert into MAGAZINE (NAME) values ('" + m.getName() + "')");
				connManager.close();
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}

	public Magazine findMagazineByName(String name) throws DAOExcepcion {
		try {
			connManager.connect();
			ResultSet rs = connManager.queryDB("select NAME, CHIEF_EDITOR from MAGAZINE where NAME= '" + name + "'");
			connManager.close();
			try {
				if (rs.next()) {
					return new Magazine(rs.getString("NAME"), ruDAO.findRegisteredUserbyLogin(rs.getString("CHIEF_EDITOR")));
				} else
					return null;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}

	public List<Magazine> findAllMagazines() throws DAOExcepcion {
		try {
			String name;
			Magazine m;
			connManager.connect();
			ResultSet rs = connManager.queryDB("select * from MAGAZINE");
			connManager.close();
			List<Magazine> listMagazines = new ArrayList<Magazine>();
			try {
				while (rs.next()) {
					RegisteredUser ru = ruDAO.findRegisteredUserbyLogin(rs.getString("CHIEF_EDITOR"));
					name = rs.getString("NAME");
					m = new Magazine(name, ru);
					listMagazines.add(m);
				}
				return listMagazines;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
}
