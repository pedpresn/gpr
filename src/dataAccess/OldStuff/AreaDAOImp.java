package dataAccess.OldStuff;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import businessLogic.OldStuff.Area;
import businessLogic.OldStuff.RegisteredUser;
import excepciones.DAOExcepcion;

public class AreaDAOImp implements IAreaDAO {
	protected ConnectionManager connManager;
	private IMagazineDAO mDAO;
	private IRegisteredUserDAO ruDAO;

	public AreaDAOImp() throws DAOExcepcion {
		super();
		try {
			connManager = new ConnectionManager("ISW");
			mDAO = new MagazineDAOImp();
			ruDAO = new RegisteredUserDAOImp();
		} catch (ClassNotFoundException e) {
			throw new DAOExcepcion("DB_CONNECT_ERROR");
		}
	}

	public void createArea(Area a, String magazineName) throws DAOExcepcion {
		try {
			if (this.findAreaByName(a.getName()) == null) {
				if (mDAO.findMagazineByName(magazineName) != null) {
					if (a.getAreaEditor() != null) {
						try {
							connManager.connect();
							connManager.updateDB("insert into AREA (NAME, AREA_EDITOR, MAGAZINE) values ('" + a.getName() + "', '" + a.getAreaEditor().getLogin() + "', '" + magazineName + "')");
							connManager.close();
						} catch (DAOExcepcion e) {
							throw e;
						}
					} else
						throw new DAOExcepcion("AREA_EDITOR_NO_EXISTE");
				} else
					throw new DAOExcepcion("MAGAZINE_NO_EXISTE");
			} else
				throw new DAOExcepcion("AREA_EXISTE");
		} catch (DAOExcepcion e) {
			throw e;
		}
	}

	public Area findAreaByName(String name) throws DAOExcepcion {
		try {
			connManager.connect();
			ResultSet rs = connManager.queryDB("select AREA_EDITOR from AREA where NAME= '" + name + "'");
			connManager.close();
			try {
				if (rs.next()) {
					return new Area(name, ruDAO.findRegisteredUserbyLogin(rs.getString("AREA_EDITOR")));
				} else
					return null;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}

	public Area findAreaByEditor(RegisteredUser editor) throws DAOExcepcion {
		try {
			connManager.connect();
			ResultSet rs = connManager.queryDB("select NAME from AREA where AREA_EDITOR= '" + editor.getLogin() + "'");
			connManager.close();
			try {
				if (rs.next()) {
					return new Area(rs.getString("NAME"), editor);
				} else
					return null;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}

	public List<Area> findAllAreas() throws DAOExcepcion {
		try {
			String name;
			Area a;
			connManager.connect();
			ResultSet rs = connManager.queryDB("select * from AREA");
			connManager.close();
			List<Area> listAreas = new ArrayList<Area>();
			try {
				while (rs.next()) {
					RegisteredUser ru = ruDAO.findRegisteredUserbyLogin(rs.getString("AREA_EDITOR"));
					name = rs.getString("NAME");
					a = new Area(name, ru);
					listAreas.add(a);
				}
				return listAreas;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
}