package dataAccess.OldStuff;

import java.util.List;

import excepciones.DAOExcepcion;
import businessLogic.OldStuff.Area;
import businessLogic.OldStuff.RegisteredUser;

public interface IAreaDAO {
	public Area findAreaByName(String ID) throws DAOExcepcion;

	public Area findAreaByEditor(RegisteredUser editor) throws DAOExcepcion;

	public void createArea(Area a, String magazineName) throws DAOExcepcion;

	public List<Area> findAllAreas() throws DAOExcepcion;
}