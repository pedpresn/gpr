package dataAccess.OldStuff;

import java.util.List;

import businessLogic.OldStuff.Magazine;
import excepciones.DAOExcepcion;

public interface IMagazineDAO {
	public Magazine findMagazineByName(String ID) throws DAOExcepcion;

	public void createMagazine(Magazine a) throws DAOExcepcion;

	public List<Magazine> findAllMagazines() throws DAOExcepcion;
}