package dataAccess.OldStuff;

import java.sql.Timestamp;
import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;

import businessLogic.OldStuff.Area;
import businessLogic.OldStuff.Article;
import businessLogic.OldStuff.Evaluation;
import businessLogic.OldStuff.Magazine;
import businessLogic.OldStuff.Person;
import businessLogic.OldStuff.RegisteredUser;
import excepciones.DAOExcepcion;

public class DAL {
	private static DAL dal;
	private IRegisteredUserDAO ruDAO;
	private IAreaDAO aDAO;
	private IMagazineDAO mDAO;
	private IArticleDAO artDAO;
	private IEvaluationDAO eDAO;

	private DAL() throws DAOExcepcion {
		ruDAO = new RegisteredUserDAOImp();
		aDAO = new AreaDAOImp();
		mDAO = new MagazineDAOImp();
		artDAO = new ArticleDAOImp();
		eDAO = new EvaluationDAOImp();
	}

	public static DAL getSingletonDAL() throws DAOExcepcion {
		if (dal == null)
			dal = new DAL();
		return dal;
	}

	public void createArea(Area a, String magazineName) throws DAOExcepcion {
		aDAO.createArea(a, magazineName);
	}
	
	public void createArticle(Article art) throws DAOExcepcion {
		artDAO.createArticle(art);
	}

	public Area findAreaByName(String ID) throws DAOExcepcion {
		return aDAO.findAreaByName(ID);
	}

	public void createMagazine(Magazine a) throws DAOExcepcion {
		mDAO.createMagazine(a);
	}

	public Magazine findMagazineByName(String ID) throws DAOExcepcion {
		return mDAO.findMagazineByName(ID);
	}

	public void createRegisteredUser(RegisteredUser p) throws DAOExcepcion {
		ruDAO.createRegisteredUser(p);
	}

	public List<RegisteredUser> findAllRegisteredUsers() throws DAOExcepcion {
		return ruDAO.findAllRegisteredUsers();
	}

	public RegisteredUser findRegisteredUserbyLogin(String login) throws DAOExcepcion {
		return ruDAO.findRegisteredUserbyLogin(login);
	}

	public void createPerson(Person p) throws DAOExcepcion {
		ruDAO.createPerson(p);
	}

	public Person findPersonbyNif(String ID) throws DAOExcepcion {
		return ruDAO.findPersonbyNif(ID);
	}

	public List<RegisteredUser> findAllRegisteredUsersNotEditors() throws DAOExcepcion {
		return ruDAO.findAllRegisteredUsersNotEditors();
	}
	
	public List<Article> findAllPendingEvaluationArticlesNotRejectedForEditorByLogin(String areaEditorLogin) throws DAOExcepcion {
		RegisteredUser areaEditor = this.findRegisteredUserbyLogin(areaEditorLogin);
		Area a = aDAO.findAreaByEditor(areaEditor);
		return artDAO.findAllPendingEvaluationArticlesNotRejectedInArea(a);
	}
	
	public List<Article> findAllPendingPublicationArticlesForEditorByLogin(String areaEditorLogin) throws DAOExcepcion {
		RegisteredUser areaEditor = this.findRegisteredUserbyLogin(areaEditorLogin);
		Area a = aDAO.findAreaByEditor(areaEditor);
		return artDAO.findAllPendingPublicationArticlesInArea(a);
	}

	public List<Magazine> findAllMagazines() throws DAOExcepcion {
		return mDAO.findAllMagazines();
	}

	public List<Area> findAllAreas() throws DAOExcepcion {
		return aDAO.findAllAreas();
	}
	
	public Article findArticleById(String articleId) throws DAOExcepcion {
		return artDAO.findArticleById(articleId);
	}
	
	public void acceptArticle(Article art, String comment) throws DAOExcepcion {
		artDAO.changeStateToPendingPublication(art);
		Evaluation ev = new Evaluation(art.getId(), new Timestamp(System.currentTimeMillis()), "ACCEPTED", comment);
		eDAO.createEvaluation(ev);
	}
	
	public void rejectArticle(Article art, String comment) throws DAOExcepcion {
		Evaluation ev = new Evaluation(art.getId(), new Timestamp(System.currentTimeMillis()), "REJECTED", comment);
		eDAO.createEvaluation(ev);
	}
	
	public void publicateArticle(Article art) throws DAOExcepcion {
		artDAO.changeStateToProposedForPublication(art);
	}
}