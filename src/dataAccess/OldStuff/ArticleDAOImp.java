package dataAccess.OldStuff;

import java.sql.ResultSet;
import java.sql.SQLException;

import businessLogic.OldStuff.Area;
import businessLogic.OldStuff.Article;
import businessLogic.OldStuff.Evaluation;
import businessLogic.OldStuff.RegisteredUser;
import excepciones.DAOExcepcion;
import java.util.List;
import java.util.ArrayList;

public class ArticleDAOImp implements IArticleDAO {
	protected ConnectionManager connManager;
	private IAreaDAO aDAO;
	private IRegisteredUserDAO ruDAO;

	public ArticleDAOImp() throws DAOExcepcion {
		super();
		try {
			connManager = new ConnectionManager("ISW");
			aDAO = new AreaDAOImp();
			ruDAO = new RegisteredUserDAOImp();
		} catch (ClassNotFoundException e) {
			throw new DAOExcepcion("DB_CONNECT_ERROR");
		}
	}

	public void createArticle(Article art) throws DAOExcepcion {
		try {
			if (this.findArticleById(art.getId()) == null) {
				Evaluation ev = art.getEvaluation();
				if (ev != null) {
					connManager.connect();
					connManager.updateDB("insert into EVALUATION (ARTICLE, DATE, RESULT, COMMENTS) values ('" + art.getId() + "', '" + ev.getDate().toString() + "', '" + ev.getResult() + "', '" + ev.getComments() + "')");
					connManager.close();
				}
				connManager.connect();

				connManager.updateDB("insert into ARTICLE (ID, TITLE, SUBMISSION_DATE, FILE_PATH, FILE_NAME, SUBMISSION_RESPONSIBLE, HAS_AREA, STATE) values ('" + art.getId() + "', '"
						+ art.getTitle() + "', '" + art.getSubmissionDate().toString() + "', '" + art.getFilePath() + "', '" + art.getFileName() + "', '" + art.getSubmissionResponsible().getLogin()
						+ "', '" + art.getHasArea().getName() + "', '" + art.getState() + "')");

				connManager.close();
			} else
				throw new DAOExcepcion("ARTICLE_EXISTE");
		} catch (DAOExcepcion e) {
			throw e;
		}
	}

	public Article findArticleById(String id) throws DAOExcepcion {
		try {

			connManager.connect();
			ResultSet rs = connManager.queryDB("select ID, TITLE, SUBMISSION_DATE, FILE_PATH, FILE_NAME, SUBMISSION_RESPONSIBLE, HAS_AREA, STATE from ARTICLE where ID= '" + id + "'");
			connManager.close();
			try {
				if (rs.next()) {
					Area hasArea = aDAO.findAreaByName(rs.getString("HAS_AREA"));
					RegisteredUser submissionResponsible = ruDAO.findRegisteredUserbyLogin(rs.getString("SUBMISSION_RESPONSIBLE"));
					Article art = new Article(rs.getString("ID"), rs.getString("TITLE"), rs.getTimestamp("SUBMISSION_DATE"), rs.getString("FILE_PATH"), rs.getString("FILE_NAME"), hasArea,
							submissionResponsible, rs.getString("STATE"));
					return art;
				} else
					return null;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}

	public List<Article> findAllArticles() throws DAOExcepcion {
		try {
			Article art;
			connManager.connect();
			ResultSet rs = connManager.queryDB("select * from ARTICLE");
			connManager.close();
			List<Article> listArticles = new ArrayList<Article>();
			try {
				while (rs.next()) {
					Area hasArea = aDAO.findAreaByName(rs.getString("HAS_AREA"));
					RegisteredUser submissionResponsible = ruDAO.findRegisteredUserbyLogin(rs.getString("SUBMISSION_RESPONSIBLE"));
					art = new Article(rs.getString("ID"), rs.getString("TITLE"), rs.getTimestamp("SUBMISSION_DATE"), rs.getString("FILE_PATH"), rs.getString("FILE_NAME"), hasArea, submissionResponsible,
							rs.getString("STATE"));
					listArticles.add(art);
				}
				return listArticles;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}

	public List<Article> findAllPendingEvaluationArticlesNotRejectedInArea(Area a) throws DAOExcepcion {
		try {
			Article art;
			connManager.connect();
			ResultSet rs = connManager.queryDB("select distinct ART.ID, ART.TITLE, ART.SUBMISSION_DATE, ART.FILE_PATH, ART.FILE_NAME, ART.SUBMISSION_RESPONSIBLE, ART.HAS_AREA, ART.STATE " +
			"from ARTICLE ART " +
			"where ART.HAS_AREA = '" + a.getName() + "' and ART.STATE='PENDING_EVALUATION' and " +
			"not exists ( " +
				"select * " +
				"from EVALUATION E " +
				"where E.ARTICLE = ART.ID and E.RESULT = 'REJECTED' "+
			");");
			connManager.close();
			List<Article> listArticles = new ArrayList<Article>();
			try {
				while (rs.next()) {
					Area hasArea = aDAO.findAreaByName(rs.getString("HAS_AREA"));
					RegisteredUser submissionResponsible = ruDAO.findRegisteredUserbyLogin(rs.getString("SUBMISSION_RESPONSIBLE"));
					art = new Article(rs.getString("ID"), rs.getString("TITLE"), rs.getTimestamp("SUBMISSION_DATE"), rs.getString("FILE_PATH"), rs.getString("FILE_NAME"), hasArea, submissionResponsible,
							rs.getString("STATE"));
					listArticles.add(art);
				}
				if (listArticles.size() == 0)
					throw new DAOExcepcion("NO_ART_PENDIENTE_NOT_REJECTED_FOUND");
				else
					return listArticles;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
	
	public List<Article> findAllPendingPublicationArticlesInArea(Area a) throws DAOExcepcion { // TODO
		try {
			Article art;
			connManager.connect();
			ResultSet rs = connManager.queryDB("select distinct ART.ID, ART.TITLE, ART.SUBMISSION_DATE, ART.FILE_PATH, ART.FILE_NAME, ART.SUBMISSION_RESPONSIBLE, ART.HAS_AREA, ART.STATE " +
			"from ARTICLE ART " +
			"where ART.HAS_AREA = '" + a.getName() + "' and ART.STATE='PENDING_PUBLICATION'");
			connManager.close();
			List<Article> listArticles = new ArrayList<Article>();
			try {
				while (rs.next()) {
					Area hasArea = aDAO.findAreaByName(rs.getString("HAS_AREA"));
					RegisteredUser submissionResponsible = ruDAO.findRegisteredUserbyLogin(rs.getString("SUBMISSION_RESPONSIBLE"));
					art = new Article(rs.getString("ID"), rs.getString("TITLE"), rs.getTimestamp("SUBMISSION_DATE"), rs.getString("FILE_PATH"), rs.getString("FILE_NAME"), hasArea, submissionResponsible,
							rs.getString("STATE"));
					listArticles.add(art);
				}
				if (listArticles.size() == 0)
					throw new DAOExcepcion("NO_ART_PENDIENTE_PUBLICACION_FOUND");
				else
					return listArticles;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
	
	public void changeStateToPendingPublication(Article art) throws DAOExcepcion {
		connManager.connect();
		ResultSet rs = connManager.queryDB("update ARTICLE " +
		"set STATE='PENDING_PUBLICATION' " +
		"where ID = '" + art.getId() + "';");
		connManager.close();
	}
	
	public void changeStateToProposedForPublication(Article art) throws DAOExcepcion {
		connManager.connect();
		ResultSet rs = connManager.queryDB("update ARTICLE " +
		"set STATE='PROPOSED_FOR_PUBLICATION' " +
		"where ID = '" + art.getId() + "';");
		connManager.close();
	}
}
