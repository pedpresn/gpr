package dataAccess.OldStuff;

import java.util.List;

import businessLogic.OldStuff.Area;
import businessLogic.OldStuff.Article;
import excepciones.DAOExcepcion;

public interface IArticleDAO {
	public Article findArticleById(String id) throws DAOExcepcion;

	public void createArticle(Article art) throws DAOExcepcion;

	public List<Article> findAllArticles() throws DAOExcepcion;

	public List<Article> findAllPendingEvaluationArticlesNotRejectedInArea(Area a) throws DAOExcepcion;
	
	public List<Article> findAllPendingPublicationArticlesInArea(Area a) throws DAOExcepcion;
	
	public void changeStateToPendingPublication(Article art) throws DAOExcepcion;
	
	public void changeStateToProposedForPublication(Article art) throws DAOExcepcion;
}
