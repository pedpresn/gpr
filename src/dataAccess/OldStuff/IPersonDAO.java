package dataAccess.OldStuff;

import excepciones.*;
import businessLogic.OldStuff.Person;

public interface IPersonDAO {
	public Person findPersonbyNif(String ID) throws DAOExcepcion;

	public void createPerson(Person p) throws DAOExcepcion;
}
