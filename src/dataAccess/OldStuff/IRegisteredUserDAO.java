package dataAccess.OldStuff;

import java.util.List;

import businessLogic.OldStuff.RegisteredUser;
import excepciones.DAOExcepcion;

public interface IRegisteredUserDAO extends IPersonDAO {
	public RegisteredUser findRegisteredUserbyLogin(String login) throws DAOExcepcion;

	public void createRegisteredUser(RegisteredUser p) throws DAOExcepcion;

	public List<RegisteredUser> findAllRegisteredUsers() throws DAOExcepcion;

	public List<RegisteredUser> findAllRegisteredUsersNotEditors() throws DAOExcepcion;
}
