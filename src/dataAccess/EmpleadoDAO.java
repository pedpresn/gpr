package dataAccess;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import dataAccess.ConnectionManager;
import excepciones.DAOExcepcion;
import businessLogic.Empleado;
import businessLogic.Jefe;
import businessLogic.Maestro;
import businessLogic.Maquina;
import businessLogic.Operario;

public class EmpleadoDAO {
	/* Atributos */
	private static EmpleadoDAO empDAO;
	private static ConnectionManager connManager;
	private static final String[] tipoEmpleados =
			{ "OPERARIO", "MAESTRO", "JEFE" };

	/* Constructores */
	public EmpleadoDAO() throws DAOExcepcion {
		super();
		try {
			connManager = new ConnectionManager("GPR");
		} catch (ClassNotFoundException e) {
			throw new DAOExcepcion("DB_CONNECT_ERROR");
		}
	}

	/* M�todos */
	public static EmpleadoDAO getSingleton() throws DAOExcepcion {
		if (empDAO == null)
			empDAO = new EmpleadoDAO();
		return empDAO;
	}
	
	public List<Empleado> getAllOperariosByArea(int codArea) throws DAOExcepcion{
		try {
			List<Empleado> operarios = new ArrayList<Empleado>();
			connManager.connect();
			ResultSet rs =
					connManager.queryDB("select distinct * from EMPLEADO e, OPERARIO o where e.dni=o.dni AND area = " + codArea + ";");
			connManager.close();
			try {
				while (rs.next()) {
					operarios.add(new Operario(rs.getString("dni"), rs
							.getString("apellidos"), rs
							.getString("nombre"), rs
							.getDate("fechaNacimiento"), rs
							.getString("poblacion"), rs
							.getString("provincia"), rs
							.getString("cp"), rs
							.getString("telefono"), rs
							.getString("nombreUsuario"), rs
							.getString("contrasena"), rs
							.getInt("area")));
				}
				return operarios;
			} catch (SQLException e) {
				e.printStackTrace();
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}

	public Empleado getEmpleadoByNombreUsuarioAndContrasena(
			String nombreUsuario, String contrasena) throws DAOExcepcion {
		Empleado emp = null;
		try {
			connManager.connect();
			for (String tipoEmp : tipoEmpleados) {
				ResultSet rs =
						connManager
								.queryDB("SELECT E.dni, E.apellidos, E.nombre, "
										+ "E.fechaNacimiento, E.poblacion, E.provincia, "
										+ "E.cp, E.telefono, E.nombreUsuario, E.contrasena"
										+ ((tipoEmp.equals("MAESTRO"))
												? ", X.area " : ((tipoEmp.equals("OPERARIO"))? ", X.area " : " "))
										+ "FROM EMPLEADO E, "
										+ tipoEmp
										+ " X WHERE E.dni = X.dni AND E.nombreUsuario = '"
										+ nombreUsuario
										+ "' AND E.contrasena = '"
										+ contrasena
										+ "';");
				if (rs.next()) {
					if (tipoEmp.equals("OPERARIO")) {
						emp =
								new Operario(rs.getString("dni"), rs
										.getString("apellidos"), rs
										.getString("nombre"), rs
										.getDate("fechaNacimiento"), rs
										.getString("poblacion"), rs
										.getString("provincia"), rs
										.getString("cp"), rs
										.getString("telefono"), rs
										.getString("nombreUsuario"), rs
										.getString("contrasena"), rs
										.getInt("area"));
					} else if (tipoEmp.equals("MAESTRO")) {
						emp =
								new Maestro(rs.getString("dni"), rs
										.getString("apellidos"), rs
										.getString("nombre"), rs
										.getDate("fechaNacimiento"), rs
										.getString("poblacion"), rs
										.getString("provincia"), rs
										.getString("cp"), rs
										.getString("telefono"), rs
										.getString("nombreUsuario"), rs
										.getString("contrasena"), rs
										.getInt("area"));
					} else if (tipoEmp.equals("JEFE")) {
						emp =
								new Jefe(rs.getString("dni"), rs
										.getString("apellidos"), rs
										.getString("nombre"), rs
										.getDate("fechaNacimiento"), rs
										.getString("poblacion"), rs
										.getString("provincia"), rs
										.getString("cp"), rs
										.getString("telefono"), rs
										.getString("nombreUsuario"), rs
										.getString("contrasena"));
					}
					break;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOExcepcion("DB_READ_ERROR");
		} finally {
			connManager.close();
		}

		if (emp == null) {
			throw new DAOExcepcion("CONTRASENA_INCORRECTA");
		}
		return emp;
	}
}
