package dataAccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import businessLogic.PeticionMaterial;
import businessLogic.Material;
import excepciones.DAOExcepcion;

public class PeticionMaterialDAO {
	protected ConnectionManager connManager;
	private static PeticionMaterialDAO pmDAO;

	private PeticionMaterialDAO() throws DAOExcepcion {
		super();
		try {
			connManager = new ConnectionManager("GPR");
		} catch (ClassNotFoundException e) {
			throw new DAOExcepcion("DB_CONNECT_ERROR");
		}
	}

	public static PeticionMaterialDAO getSingleton() throws DAOExcepcion {
		if (pmDAO == null)
			pmDAO = new PeticionMaterialDAO();
		return pmDAO;
	}
	
	/* lm is a list of arrays of objects. The position 0 of the array 
	 * is a PeticionMaterial, the position 1 is the amount of that material. */
	public void crearPeticionMaterial(PeticionMaterial pm, List<Object[]> lm) throws DAOExcepcion {
		try {
			connManager.connect();
			connManager
					.updateDB("insert into PETICION_MATERIAL (fechaPedido, fechaRecepcion, estado) values ('"
							+ pm.getFechaPedido()
							+ "', '"
							+ pm.getFechaRecepcion()
							+ "', '"
							+ pm.getEstado() + "')");
			for (Object o[] : lm) {
				Material m = (Material) o[0];
				connManager
					.updateDB("insert into TIENE(codPed, codMaterial, cantidad) values ('"
						+ pm.getCodPed()
						+ "', '"
						+ m.getCodMaterial()
						+ "', '"
						+ (Integer) o[1] + "')");
			}
			connManager.close();
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
}
