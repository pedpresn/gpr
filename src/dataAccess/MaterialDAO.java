package dataAccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import businessLogic.Material;
import businessLogic.OrdenTrabajo;
import excepciones.DAOExcepcion;

public class MaterialDAO {
	protected ConnectionManager connManager;
	private static MaterialDAO mDAO;

	private MaterialDAO() throws DAOExcepcion {
		super();
		try {
			connManager = new ConnectionManager("GPR");
		} catch (ClassNotFoundException e) {
			throw new DAOExcepcion("DB_CONNECT_ERROR");
		}
	}

	public static MaterialDAO getSingleton() throws DAOExcepcion {
		if (mDAO == null)
			mDAO = new MaterialDAO();
		return mDAO;
	}

	public List<Material> getAllMateriales() throws DAOExcepcion {
		try {
			List<Material> materiales = new ArrayList<Material>();
			connManager.connect();
			ResultSet rs =
					connManager.queryDB("select distinct * from MATERIAL");
			connManager.close();
			try {
				while (rs.next()) {
					Material m =
							new Material(rs.getInt("codMaterial"), rs.getString("proveedor"),
									rs.getString("descripcion"), rs.getDouble("precio"), 
									rs.getInt("stock"), rs.getInt("stockMin"), rs.getInt("stockMax"));
					materiales.add(m);
				}
				return materiales;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
	
	public void updateMaterial(Material m) throws DAOExcepcion {
		try {
			connManager.connect();
			connManager
					.updateDB("update MATERIAL set "
							+ " proveedor=" + m.getProveedor()
							+ " descripcion=" + m.getDescripcion()
							+ " precio=" + m.getPrecio()
							+ " stock=" + m.getStock()
							+ " stockMin=" + m.getStockMin()
							+ " stockMax=" + m.getStockMax()
							+ " WHERE codigo=" + m.getCodMaterial());
			connManager.close();
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
}
