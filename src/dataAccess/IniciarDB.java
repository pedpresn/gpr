package dataAccess;


public class IniciarDB {
	public static void main(String[] args) {
		try{
			RestartDB rdb = new RestartDB();
			rdb.cleanDB();
			rdb.initDB();
			System.out.println("Done!");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
