package dataAccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import businessLogic.AvisoIncidencia;
import businessLogic.OrdenTrabajo;
import businessLogic.ParteIncidencia;
import excepciones.DAOExcepcion;

public class OrdenTrabajoDAO {
	protected ConnectionManager connManager;
	private static OrdenTrabajoDAO otDAO;

	private OrdenTrabajoDAO() throws DAOExcepcion {
		super();
		try {
			connManager = new ConnectionManager("GPR");
		} catch (ClassNotFoundException e) {
			throw new DAOExcepcion("DB_CONNECT_ERROR");
		}
	}

	public static OrdenTrabajoDAO getSingleton() throws DAOExcepcion {
		if (otDAO == null)
			otDAO = new OrdenTrabajoDAO();
		return otDAO;
	}

	public void crearOrdenTrabajo(OrdenTrabajo ot) throws DAOExcepcion {
		int pedido = ot.getPedido();
		Timestamp fechaRep = ot.getFechaReparacion();	
		try {
			connManager.connect();
			connManager
					.updateDB("insert into ORDEN_TRABAJO (codigo, dni, fechaReparacion, pedido) values ("
							+ ot.getCodigo()
							+ ", '"
							+ ot.getDni()
							+ "', "
							+ (ot.getFechaReparacion() == null? "NULL" : "'" + ot.getFechaReparacion() + "'" )
							+ ", "
							+ (ot.getPedido() == -1? "NULL" : "'" + Integer.toString(pedido) + "'") + ")");
			connManager.close();
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
	
	public void finalizarOrdenTrabajo(OrdenTrabajo ot) throws DAOExcepcion {
		try {
			connManager.connect();
			connManager.updateDB("update PARTE_INCIDENCIA set"
							+ " estado=" + OrdenTrabajo.TERMINADA
							+ " WHERE codigo=" + ot.getCodigo());
			connManager.updateDB("update ORDEN_TRABAJO set"
					+ " fechaReparacion='" + ot.getFechaReparacion()
					+ "' WHERE codigo=" + ot.getCodigo());
			connManager.close();
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
	
	public void cerrarOrdenTrabajo(OrdenTrabajo ot) throws DAOExcepcion {
		try {
			connManager.connect();
			connManager
					.updateDB("update PARTE_INCIDENCIA set "
							+ " estado=" + OrdenTrabajo.REGISTRADA
							+ " WHERE codigo=" + ot.getCodigo());
			connManager.close();
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
	
	public List<OrdenTrabajo> getAllOrdenesTrabajoByDni(String dni) throws DAOExcepcion {
		try {
			List<OrdenTrabajo> ordenes = new ArrayList<OrdenTrabajo>();
			connManager.connect();
			ResultSet rs =
					connManager.queryDB("select distinct * from ORDEN_TRABAJO where dni=" + dni);
			connManager.close();
			try{
				while(rs.next()){
					OrdenTrabajo ot = new OrdenTrabajo(rs.getInt("codigo"),rs.getString("dni"),rs.getTimestamp("fechaReparacion"),
							rs.getInt("pedido"));
					ordenes.add(ot);
				}
				if (ordenes.size() == 0)
					throw new DAOExcepcion("NO_INCIDENCIAS_FOUND");
				else
					return ordenes;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
	
	public Object[] getAllIncidenciasAndAvisosOfOrdenTrabajoByDni(String dni) throws DAOExcepcion {
		try {
			int maquinaAux;
			Object[] result = new Object[2];
			List<ParteIncidencia> partes = new ArrayList<ParteIncidencia>();
			List<AvisoIncidencia> avisos = new ArrayList<AvisoIncidencia>();
			connManager.connect();
			ResultSet rs =
					connManager
							.queryDB("select distinct P.codigo, P.prioridad, P.estado, P.costeDiario, P.area, A.descripcion," +
									" A.fechaPeticion, A.maquina from PARTE_INCIDENCIA P, AVISO_INCIDENCIA A, ORDEN_TRABAJO O where O.dni = " + dni + " and O.codigo = P.codigo and P.codigo = A.codigo " +
											"and (P.estado = " + Integer.toString(OrdenTrabajo.PENDIENTE) + " or P.estado = " + Integer.toString(OrdenTrabajo.EN_CURSO) + ");");
			connManager.close();
			try {
				while (rs.next()) {
					maquinaAux = rs.getInt("maquina");
					if(rs.wasNull()) maquinaAux = -1;
					partes.add(new ParteIncidencia(rs.getInt("codigo"), rs
							.getInt("prioridad"), rs.getInt("estado"), rs
							.getDouble("costeDiario"), rs.getInt("area")));
					avisos.add(new AvisoIncidencia(rs.getInt("codigo"), rs
							.getString("descripcion"), rs
							.getTimestamp("fechaPeticion"), maquinaAux));
				}
				if(partes.isEmpty() || avisos.isEmpty()){
					throw new DAOExcepcion("NO_ORDENES_FOUND");
				}
				else{
					result[0] = partes;
					result[1] = avisos;
					return result;
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
	
	public Object[][] getAllOrdenesTerminadasByArea(int area) throws DAOExcepcion {
		try {
			connManager.connect();
			ResultSet rs1 = connManager.queryDB("SELECT COUNT(*) AS num FROM parte_incidencia P, aviso_incidencia A, orden_trabajo O WHERE" +
					" O.codigo = P.codigo and P.codigo = A.codigo and (P.estado = " + Integer.toString(OrdenTrabajo.TERMINADA) + ") AND P.area = '"+ area +"'");
			int num = rs1.getInt("num");
			Object[][] result = new Object[num][8];
			ResultSet rs2 = connManager.queryDB("SELECT P.codigo, A.descripcion, O.fechaReparacion, E.nombre, E.apellidos FROM parte_incidencia P, aviso_incidencia A, orden_trabajo O, empleado E WHERE" +
									" O.codigo = P.codigo AND P.codigo = A.codigo AND (P.estado = " + Integer.toString(OrdenTrabajo.TERMINADA) + " " +
									") AND O.dni = E.dni AND P.area ='"+ area +"'");
			connManager.close();
				
			int pos = 0;
			while (rs2.next() & pos < num) {
				result[pos][0] = rs2.getInt("codigo");
				result[pos][1] = rs2.getTimestamp("fechaReparacion");
				result[pos][2] = rs2.getString("descripcion");
				result[pos][3] = rs2.getString("nombre");
				result[pos][4] = rs2.getString("apellidos");
			}
			return result;
		} catch (DAOExcepcion e) {
			throw e;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOExcepcion("DB_READ_ERROR");
		}
	}
	
	public void eliminarOrdenTrabajoById(int codigo) throws DAOExcepcion {
		try {
			connManager.connect();
			connManager.updateDB("DELETE FROM orden_trabajo WHERE codigo = '"+ codigo +"'");
			connManager.updateDB("DELETE FROM parte_incidencia WHERE codigo = '"+ codigo +"'");
			connManager.updateDB("DELETE FROM aviso_incidencia WHERE codigo = '"+ codigo +"'");
			connManager.close();
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
}