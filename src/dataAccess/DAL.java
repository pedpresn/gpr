package dataAccess;

import java.sql.Timestamp;
import java.util.List;

import businessLogic.Area;
import businessLogic.AvisoIncidencia;
import businessLogic.Empleado;
import businessLogic.Maquina;
import businessLogic.OrdenTrabajo;
import businessLogic.ParteIncidencia;
import dataAccess.EmpleadoDAO;
import excepciones.DAOExcepcion;

public class DAL {
	/* Atributos */
	private static DAL dal;
	private static EmpleadoDAO empDAO;
	private static AvisoIncidenciaDAO avDAO;
	private static MaquinaDAO maqDAO;
	private static AreaDAO areaDAO;
	private static ParteIncidenciaDAO parInDAO;
	private static OrdenTrabajoDAO ordDAO;

	/* Constructores */
	private DAL() throws DAOExcepcion {
		empDAO = EmpleadoDAO.getSingleton();
		avDAO = AvisoIncidenciaDAO.getSingleton();
		maqDAO = MaquinaDAO.getSingleton();
		areaDAO = AreaDAO.getSingleton();
		parInDAO = ParteIncidenciaDAO.getSingleton();
		ordDAO = OrdenTrabajoDAO.getSingleton();
	}

	/* M�todos */
	public static DAL getSingletonDAL() throws DAOExcepcion {
		if (dal == null)
			dal = new DAL();
		return dal;
	}

	public Empleado getEmpleadoByNombreUsuarioAndContrasena(
			String nombreUsuario, String contrasena) throws DAOExcepcion {
		return empDAO.getEmpleadoByNombreUsuarioAndContrasena(nombreUsuario,
				contrasena);
	}

	public void crearAvisoIncidencia(String descripcion, Timestamp timestamp,
			int maquinaId) throws DAOExcepcion {
		avDAO.crearAvisoIncidencia(new AvisoIncidencia(0, descripcion,
				timestamp, maquinaId));
	}
	
	public void crearParteIncidencia(ParteIncidencia pi) throws DAOExcepcion {
		parInDAO.crearParteIncidencia(pi);
	}

	public List<Maquina> getAllMaquinas() throws DAOExcepcion {
		return maqDAO.getAllMaquinas();
	}
	
	public Object[] getAllMaquinasWithAreas() throws DAOExcepcion {
		return maqDAO.getAllMaquinasWithAreas();
	}

	public AvisoIncidencia getAvisoIncidenciaByCod(int codigo) throws DAOExcepcion {
		return avDAO.getAvisoIncidenciaByCod(codigo);
	}
	
	public List<AvisoIncidencia> getAllAvisosIncidencia() throws DAOExcepcion {
		return avDAO.getAllAvisosIncidencia();
	}

	public Maquina getMaquinaById(int id) throws DAOExcepcion {
		return maqDAO.getMaquinaById(id);
	}

	public Area getAreaByCodArea (int codArea) throws DAOExcepcion {
		return areaDAO.getAreaByCodArea(codArea);
	}
	
	public List<Area> getAllAreas() throws DAOExcepcion {
		return areaDAO.getAllAreas();
	}

	public Object[] getAllIncidenciasByCodArea(int area) throws DAOExcepcion {
		return parInDAO.getAllIncidenciasAndAvisosByCodArea(area);
	}
	
	public List<Empleado> getAllOperariosByArea(int codArea) throws DAOExcepcion{
		return empDAO.getAllOperariosByArea(codArea);
	}

	public void crearOrdenTrabajo(OrdenTrabajo orden) throws DAOExcepcion {
		ordDAO.crearOrdenTrabajo(orden);
	}
	
	public Object[] getAllIncidenciasAndAvisosOfOrdenTrabajoByDni(String dni) throws DAOExcepcion {
		return ordDAO.getAllIncidenciasAndAvisosOfOrdenTrabajoByDni(dni);
	}

	public Object[][] getAllOrdenesTerminadasByArea(int area) throws DAOExcepcion {
		return ordDAO.getAllOrdenesTerminadasByArea(area);
	}
	
	public void finalizarOrdenTrabajo(OrdenTrabajo ordenActual) throws DAOExcepcion{
		ordDAO.finalizarOrdenTrabajo(ordenActual);
	}
	
	public void eliminarAvisoIncidenciaById(int codigo) throws DAOExcepcion {
		avDAO.eliminarAvisoIncidenciaById(codigo);
	}
	
	public void eliminarParteIncidenciaById(int codigo) throws DAOExcepcion {
		parInDAO.eliminarParteIncidenciaById(codigo);
	}
	
	public void eliminarOrdenTrabajoById(int codigo) throws DAOExcepcion {
		ordDAO.eliminarOrdenTrabajoById(codigo);
	}
}
