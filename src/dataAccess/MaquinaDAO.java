package dataAccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import businessLogic.Area;
import businessLogic.Maquina;
import excepciones.DAOExcepcion;

public class MaquinaDAO {
	protected ConnectionManager connManager;
	private static MaquinaDAO mDAO;

	private MaquinaDAO() throws DAOExcepcion {
		super();
		try {
			connManager = new ConnectionManager("GPR");
		} catch (ClassNotFoundException e) {
			throw new DAOExcepcion("DB_CONNECT_ERROR");
		}
	}

	public static MaquinaDAO getSingleton() throws DAOExcepcion {
		if (mDAO == null)
			mDAO = new MaquinaDAO();
		return mDAO;
	}

	public List<Maquina> getAllMaquinas() throws DAOExcepcion {
		try {
			List<Maquina> maquinas = new ArrayList<Maquina>();
			connManager.connect();
			ResultSet rs =
					connManager.queryDB("select distinct * from MAQUINA");
			connManager.close();
			try {
				while (rs.next()) {
					Maquina m =
							new Maquina(rs.getInt("codMaquina"), rs.getString("marca"),
									rs.getString("modelo"), rs.getInt("codArea"), 
									rs.getString("descripcion"), rs.getTimestamp("fechaUltimaRev"));
					maquinas.add(m);
				}
				return maquinas;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}

	public Object[] getAllMaquinasWithAreas() throws DAOExcepcion {
		try {
			Object[] result = new Object[2];
			List<Area> areas = new ArrayList<Area>();
			List<Maquina> maquinas = new ArrayList<Maquina>();
			connManager.connect();
			ResultSet rs =
					connManager
							.queryDB("select distinct M.codMaquina, M.marca, M.modelo, M.area, M.descripcion, M.fechaUltimaRev, A.nombre from MAQUINA M, AREA A where A.codArea = M.area");
			connManager.close();
			try {
				while (rs.next()) {
					maquinas.add(new Maquina(rs.getInt("codMaquina"), rs
							.getString("marca"), rs.getString("modelo"), rs
							.getInt("area"), rs.getString("descripcion"), rs
							.getTimestamp("fechaUltimaRev")));
					areas.add(new Area(rs.getInt("area"), rs
							.getString("nombre")));
				}
				result[0] = maquinas;
				result[1] = areas;
				return result;
			} catch (SQLException e) {
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}

	public Maquina getMaquinaById(int id) throws DAOExcepcion {
		try {
			connManager.connect();
			ResultSet rs =
					connManager
							.queryDB("select * from MAQUINA where codMaquina = "
									+ id);
			connManager.close();
			try {
				if (!rs.next()) throw new DAOExcepcion("NO_MAQUINA_FOUND");
				Maquina m =
						new Maquina(rs.getInt("codMaquina"), rs
								.getString("marca"), rs.getString("modelo"), rs
								.getInt("area"),
								rs.getString("descripcion"), rs
										.getTimestamp("fechaUltimaRev"));
				if (m == null)
					throw new DAOExcepcion("NO_MAQUINA_FOUND");
				else
					return m;
			} catch (SQLException e) {
				e.printStackTrace();
				throw new DAOExcepcion("DB_READ_ERROR");
			}
		} catch (DAOExcepcion e) {
			throw e;
		}
	}
}
